<div class="modal fade" id="zoomModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="margin-top:40px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Listing Preview</h4>
      </div>
      <div class="modal-body" style="height:500px;overflow:scroll;">
	

<%def name="my_images(id)">
<div class="item">
<img src="${request.route_url('serve',id=id)}" class="img-responsive">
</div>
</%def>
<%def name="my_thumbs(id)">
<div data-target="#carousel-kaniru" data-slide-to="%s"%(id) class="item"><img src="${request.route_url('serve',id=id)}" class="img-responsive">
</div>
</%def>

<div id="zoomcarousel" class="carousel slide" data-ride="carouse">
  
  <!-- Wrapper for slides -->
<div class="carousel-inner" >
    		<div class="item active">
		    <img src="${request.route_url('serve',id=ids[0])}" class="img-responsive">
			</div>
	%for id in ids[1:] :
    ${self.my_images(id)}
	%endfor
	</div>

<div class="col-md-offset-5">
	<a class="left carousel-controls" href="#zoomcarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-controls" href="#zoomcarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>
</div>






	<!-- Modal-->
 </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>


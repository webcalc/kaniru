<%inherit file="kaniru:templates/base/base.mako" />
<div class="row" id="browsebg" style="padding:10px;">
	<div class="col-md-2" id="searchbrowse">

	
<form class="form-horizontal" action="${request.route_url('prop_search')}" role="form" method="GET" >
<p style="font-family:cursive">Refine your search:</p>
<div class="form-group has-success  has-feedback">
	<select class="form-control control" name='type_id' id="type_id">
<option value="">Any Property Type</option>
<option value="apartment">Apartment</option>
<option value="house">House</option>
<option value="commercial">Commercial</option>
<option value="development">Development</option>
<option value="land">Land</option>
</select>
</div>
<div class="form-group">
	<input type="text" class="form-control control" placeholder="Max Price" name="price">
</div>
<div class="form-group has-success has-feedback">
	<select class="form-control control" name="bed">
<option value="">Any Bed</option>
<option value="2">2 +</option>
<option value="3">3 +</option>
<option value="4">4 +</option>
<option value="5">5 +</option>
<option value="6">6 +</option>
<option value="7">7 +</option>
</select>
</div>
<div class="form-group">
	<input type="text" class="form-control control" placeholder="Keyword: e.g flats " name="fulltext">
</div>

	<button style="border-radius:0; width:80px;"class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-search"></span></button>

</form>

	

			</div>
	<div class='col-md-6'>

%if paginator:

<div class="row" >
<div class="col-sm-12 col-md-12 bg-primary" style="margin-bottom:8px;">
<heading>
<h4 style="color:#fff;"><strong>Featured Listings</strong></h4></heading>
</div>

<div class="col-sm-12 col-md-12">

<p class="pull-right">${paginator.pager(
'$link_previous ~3~ $link_next',
	link_attr={"class":"btn btn-sm btn-primary"},
	curpage_attr={"class":"btn btn-sm btn-default disabled"},
	symbol_next="Next",
	symbol_previous="Previous",
	show_if_single_page=True,
)}
</p>
</div>
</div>
%for listing in paginator.items:
	<div class="row" id="browsedetails" style="background:#fff;">
	
	<div class="col-sm-4 col-md-4" >
		<div class="thumbnail" id="detailthumb">
	<img src="${request.storage.url(listing.photos[0].filename)}" class="img-responsive"/>
			<div class="captions">
			
			</div>
			</div>
	</div>
	<div class="col-sm-8 col-md-8" style="padding:5px;">

	<h4 style="color:#526170;"><strong>${listing.browse_title}</strong> 
%if request.user !=None: 
 		%if listing not in request.user.favourites:
	<p class="pull-right"><a href="${request.route_url('add_to_favourites',listing_id=listing.id)}" style="color:#eeab2a">Save</a></p> 
		%else:
		<p class="pull-right">Saved</p>
		%endif
		%endif
</h4>
		<h4 style="color:#000;"><strong>${listing.naira_price}</strong></h4>
 <span class="pull-right glyphicon glyphicon-map-marker">${listing.state.name}
</span>
		
		<ul class="list-inline">
		%if listing.category_id==2:
		<li><h5><strong>Apartment:</strong></h5></li>
		<li><strong>${listing.bedroom}</strong> Bedrooms</li>
		<li><strong>${listing.bathroom}</strong> Bathroom</li>
		
		
  %elif listing.category_id==3:
			<li><h5><strong>Land:</strong></h5></li>
	<li> <strong>${listing.plot_size}</strong> Plot size</li>
	%elif listing.category_id==4:
	<li><h5><strong>Commercial:</strong></h5></li>
<li> <strong>${listing.total_room}</strong> Total Rooms</li>
<li> <strong>${listing.building_size}</strong> Building Size</li>
%elif listing.category_id==5:
			<li><h5><strong>Development:</strong></h5></li>
	 <li> <strong>${listing.bedroom}</strong> Bedroom</li>
			<li><strong> ${listing.bathroom}</strong> Baths</li>
			
		
%else:
			<li><h5><strong>House:</strong></h5></li>
	 <li> <strong>${listing.bedroom}</strong> Bedroom</li>
			<li><strong> ${listing.bathroom}</strong> Baths</li>
			
		
		 
%endif
</ul>

	<a class="btn btn-success" href="${request.route_url('property_view',listing_id=listing.id,slug=listing.slug)}" style="border-radius:0px;">Read more</a>
%if h.has_permission(request.user,request,'admin','editor'):
<a class="btn btn-danger pull-right" href="${request.route_url('delete_listing',listing_id=listing.id)}" style="border-radius:0px;">Delete</a>
%endif
		</div>

	</div>
%endfor
<div class="col-sm-12 col-md-12">

<p class="pull-right">${paginator.pager(
'$link_previous ~3~ $link_next',
	link_attr={"class":"btn btn-sm btn-primary"},
	curpage_attr={"class":"btn btn-sm btn-default disabled"},
	symbol_next="Next",
	symbol_previous="Previous",
	show_if_single_page=True,
)}
</p>
</div>

%else:
	<p>No property uploaded</p>
%endif
</div>
	<div class="col-md-4">
	%if first:
	%if first.filename:
		<a href="${first.link}" target="_blank"><img src="${request.storage.url(first.filename)}" class="img-responsive" id="first"></a>
	%else:
		${first.link|n}
	%endif
	%else:
		<a href=""><img src="/static/images/konga.png" id="first"></a>
	%endif

	%if second:
	%if second.filename:
		<a href="${second.link}" target="_blank"><img src="${request.storage.url(second.filename)}" class="img-responsive" id="first"></a>
	%else:
		${second.url|}
	%endif
	%else:
		<a href=""><img src="/static/images/ad2.png" id="first"></a>
	%endif

	%if third:
	%if third.filename:
		<a href="${third.link}" target="_blank"><img src="${request.storage.url(third.filename)}" class="img-responsive" id="first"></a>
	%else:
		${third.url|n}
	%endif
	%else:
		<a href=""><img src="/static/images/konga.png" id="first"></a>
	%endif

			</div>
</div>

<%def name="breadcrumb()">
<ol class="breadcrumb">
	<li><a href="/">Home</a></li>
	<li class="active">All</li>
</ol>
</%def>

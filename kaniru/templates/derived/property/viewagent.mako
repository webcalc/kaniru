<%inherit file="kaniru:templates/base/base.mako"/>
<div class="row row-inset">
	<div class="col-sm-12 col-md-12">
<div class="panel panel-primary">
  
  	<div class="panel-body">
<div class="row">
	<div class="col-sm-3 col-md-3">
%if user.agency_logo:
	<div>
	<img src="${request.storage.url(user.agency_logo)}" class="img-responsive" width="200px" height="auto">
	</div>
%endif
		</div>
	<div class="col-sm-6 col-md-6">
%if user.agency_name:
	<h2>${user.agency_name}</h2>
	<div class="h3">${user.agency_address}</div>
	<p>${user.agency_note}</p>
%endif
	%if user.agency_website:
		<a href="${user.agency_website}" target="_blank">
		<span class="glyphicon glyphicon-link"></span>
		Website<span class="glyphicon glyphicon-new-window"></span></a>
	%endif

		</div>
	<div class="col-sm-3 col-md-3">
		</div>

</div>

</div>
</div>
</div>
</div>
<%def name="breadcrumb()">
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li class="active">${user.fullname}</li>
</ol>
</%def>
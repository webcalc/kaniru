<%inherit file="kaniru:templates/base/base.mako" />
<%!
    from webhelpers.html import literal
%>
<%block name="script_tags">
	${parent.script_tags()}
<script>
	$('#agentForm').validate({
rules: {
    		email: {
      	required: true,
      	email: true
			}
		}


});
	$('#agentform').validate({
	rules: {
    		email: {
      	required: true,
      	email: true
			}
		}

});

</script>
</%block>
<%def name="my_images(id)">
<div class="item">
<img src="${request.storage.url(listing.photos[id+1].filename)}" class="img-responsive">
</div>
</%def>
<%def name="my_thumbs(id)">
<div data-target="#carousel-kaniru" data-slide-to="${id+1}" class="item"><img src="${request.storage.url(listing.photos[id+1].filename)}" class="img-responsive">
</div>
</%def>
<div class="row" id="listing-view">
		<div class="col-sm-8 col-md-8 shadow" style="padding-bottom:20px;">
<div class="row">
	<div class="col-sm-12 col-md-12">
<h4><strong>${listing.title.title()}</strong></h4>
<p><span class="glyphicon glyphicon-map-marker"></span><strong>Address:</strong>${listing.address.title()}</p>

	</div>
</div>
	<div class="row" id="viewdetail">
		<div class="col-sm-8 col-md-8" id="main-content">
		<div id="carousel-kaniru" class="carousel slide">
  
	<div class="big-image">
  	<!-- Wrapper for slides -->
  	<div class="carousel-inner" >
    		<div class="item active">
		    <img src="${request.storage.url( listing.photos[0].filename)}" class="img-responsive">
			</div>
	%for id in range(len(listing.photos[1:])) :
    ${self.my_images(id)}
	%endfor
	</div>
<div class="text-center">
	<a class="btn btn-sm btn-default left controls" href="#carousel-kaniru" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="btn btn-sm btn-default right controls" href="#carousel-kaniru" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>
</div><!-- /.big-image -->

<nav class="thumbnail-controls">
<a class="btn btn-sm btn-default left" href="#carousel-kaniru" data-slide="prev" style="color:#24A95C;
">
<span class="glyphicon glyphicon-chevron-left"></span>
  </a>
<div class="thumbnail-slider">
	<div class="slider-bar" >
<a href="#carousel-kaniru" data-to="0" data-slide-to="0" class="active">
	<img src="${request.storage.url( listing.photos[0].filename)}" style="display:inline;">

	%for i in range(len(listing.photos)-1):
	<a href="#carousel-kaniru" data-to="${i+1}" data-slide-to="${i+1}">
	<img src="${request.storage.url( listing.photos[i+1].filename)}" style="display:inline;">
	</a>
	%endfor
	</div>
</div>
<a class="btn btn-sm btn-default right" href="#carousel-kaniru" data-slide="next" style="color:#24A95C;
">
<span class="glyphicon glyphicon-chevron-right"></span>
  </a>

</nav>
</div>
</div>

		<div class="col-sm-4 col-md-4">
	<p class="pull-left"style="font-size:14px">Price</p>
			<p class="pull-right" style="font-size:16px; color:#29721;"><b> ${listing.naira_price}</b></p>
			<table class="table table-striped">
		<thead>
			<tr>
				<td></td>
				<td></td>
			<tr>
		<thead>
		<tbody>
%if listing.category_id==2:

				<tr>
				<td><strong>Bedrooms</strong></td>
				<td>${listing.bedroom}</td>
			 </tr>
				<tr>
					<td><strong>Bathrooms</strong></td>
					<td>${listing.bathroom}</td>
				<tr>
				%if listing.total_room:
				<tr>
					<td><strong>Total Rooms</strong></td>
					<td>${listing.total_room}</td>
				</tr>
				%endif
				<tr>
					<td><strong>Living Area Size</strong></td>
					<td>${listing.living_area}</td>
				</tr>
				<tr>
					<td><strong>Floor</strong></td>
					<td>${listing.floor}</td>
				</tr>
				<tr>
					<td><strong>Total Floor</strong></td>
					<td>${listing.total_floor}</td>
				</tr>
		%if listing.date_available:
				<tr>
					<td><strong>Available from</strong></td>
					<td>${listing.date_available}</td>
				</tr>
		%endif
				
%elif listing.category_id==3:
				<tr>
					<td><strong>Plot size</strong></td>
					<td>${listing.plot_size}</td>
				</tr>
				%if listing.date_available:
				<tr>
					<td><strong>Available from</strong></td>
					<td>${listing.date_available}</td>
				</tr>
		%endif


%elif listing.category_id==4:
				<tr>
					<td><strong>Total Rooms</strong></td>
					<td>${listing.total_room}</td>
				</tr>
				<tr>
				<td><strong>Building Size</strong></td>
				<td>${listing.building_size}</td>
				</tr>
				%if listing.date_available:
				<tr>
					<td><strong>Available from</strong></td>
					<td>${listing.date_available}</td>
				</tr>
		%endif

%else:
				<tr>
				<td><strong>Bedrooms</strong></td>
				<td>${listing.bedroom}</td>
			 </tr>
				<tr>
					<td><strong>Bathrooms</strong></td>
					<td>${listing.bathroom}</td>
				</tr>
				%if listing.total_room:
				<tr>
					<td><strong>Total Rooms</strong></td>
					<td>${listing.total_room}</td>
				</tr>
				%endif
				<tr>
					<td><strong>Living Area Size</strong></td>
					<td>${listing.living_area}</td>
				</tr>
				<tr>
					<td><strong>Land Size</strong></td>
					<td>${listing.land_size}</td>
				</tr>
				%if listing.date_available:
				<tr>
					<td><strong>Available from</strong></td>
					<td>${listing.date_available}</td>
				</tr>
				%endif


%endif
				
		<tbody>
		</table>
		</table>
<p><strong>Listing ID:</strong> ${listing.serial}</p>
%if h.has_permission(request.user,request,'admin','editor'):
<a class="btn btn-danger pull-right" href="${request.route_url('delete_listing',listing_id=listing.id)}" style="border-radius:0px;">Delete</a>
%endif

		</div>


	</div><!--/.detail -->
	<div class="col-sm-12 col-md-12" 
style="top:10px; padding:10px;">
	<div class="h4"><strong>Description</strong></div>
<hr style="margin-top:-5px;">
		<p>${listing.description}</p>
<div class="h4"><strong>More Details</strong></div>
<hr style="margin-top:-5px;">
<table class="table table-striped">
	<tbody>
<tr>
%if listing.year_built:
	<td>Year Built: ${listing.year_built}</td>
%endif
%if listing.car_spaces:
	<td>Car Spaces: ${listing.car_spaces}</td>
%endif
</tr>
<tr>
%if listing.price_condition:
	<td>Price Conditions: ${listing.price_condition}</td>
%endif
%if listing.deposit:
	<td>Deposit: ${listing.naira_deposit}</td>
%endif
</tr>
<tr>
%if listing.agent_commission:
	<td>Agent Commission: ${listing.agent_commission}</td>
%endif
</tr>
</tbody>
</table>
<div class="h4"><strong>Listing Courtesy Of</strong></div>
<hr style="margin-top:-5px;">
<div class="row">
	<div class="col-sm-6 col-md-6">
%if listing.users.agency_logo:
	<div>
	<img src="${request.storage.url(listing.users.agency_logo)}" class="img-responsive" width="200px" height="auto">
	</div>
%endif
<br>
%if listing.users.is_agent:
<address>
  <strong>${listing.users.agency_name.title()}</strong><br>
  ${listing.users.agency_address.title()}<br>
  ${listing.users.state.name.title()}<br>
</address>
%if listing.users.agency_name:
<a href="${request.route_url('view_agent', user_id=listing.users.id,
slug=listing.users.agency_name)}" class="btn btn-warning" style="border-radius:0">View Agent Details</a>
%endif
<br>
%endif
<p><span class="glyphicon glyphicon-user"></span>

<strong>${listing.display_fullname.title()}</strong></p>
  <strong> Mobile Phone <span class="glyphicon glyphicon-phone"></span>: </strong>  ${listing.mobile}<br>
%if listing.home_phone:
  <strong> Home Phone <span class="glyphicon glyphicon-phone-alt"></span>:</strong> ${listing.home_phone}<br>
%endif
%if listing.office_phone:
 <strong>Office Phone <span class="glyphicon glyphicon-phone-alt"></span>: </strong> ${listing.office_phone}
%endif
<div class="h6" style="font-family:cursive"><strong>About this property</strong></div>

<address>
<strong>Title:</strong> ${listing.title.title()}<br>
<strong>Listing ID: </strong>${listing.serial}<br>
<strong>Address:</strong> ${listing.address.title()}<br>
<strong>State: </strong> ${listing.state.name}<br>
%if listing.city:
<strong>City: </strong> ${listing.city}<br>
%endif
%if listing.area:
<strong>Area: </strong> ${listing.area}<br>
%endif
</address>
	</div>
	<div class="col-sm-6 col-md-6">
${form.begin(url=request.route_url('emailagent', agentemail=listing.users.email),id="agentform", method="post", role="form")}
${form.csrf_token()}
    ${form.hidden("camefrom", value=listing.id)}
    ${form.hidden("mail",value=listing.users.email)}
  <div class="form-group">
    ${form.text("fullname", class_="form-control required", placeholder="Full Name *")}
  </div>
<div class="form-group">
    ${form.text("mobile", class_="form-control", id="phone", placeholder="Mobile")}
  </div>
  
  <div class="form-group">
    ${form.text("email", class_="form-control", id="email", placeholder="Your Email *")}
  </div>
  <div class="form-group">
${form.textarea("body",content="Hello, I am Interested in your listing at Kaniru. Please I will like to know more about "+listing.title+", "+listing.address+"."+" Thank you.",class_="form-control required", rows="6")}
  </div>
<div class="form-group">
${form.submit("submit",class_="btn btn-warning btn-block", style="border-radius:0",value="Email Agent")}
</div>
${form.end()}
	</div>
</div>
	</div>

		</div>
	<div class="col-sm-4 col-md-4 ">
<div class="col-sm-12 col-md-12 shadow">
	<div style="padding:10px;">
%if listing.users.agency_logo:
	<div>
	<img src="${request.storage.url(listing.users.agency_logo)}" class="img-responsive" width="200px" height="auto">
	</div>
%endif

<br>
%if listing.users.agency_name:
<a href="${request.route_url('view_agent', user_id=listing.users.id,
slug=listing.users.agency_name)}" class="btn btn-warning" style="border-radius:0">View Agent Details</a>
%endif
<br>
%if listing.users.is_agent:
<address>
  <strong>${listing.users.agency_name.title()}</strong><br>
  ${listing.users.agency_address.title()}<br>
  ${listing.users.state.name.title()}<br>
</address>
%endif
<p><span class="glyphicon glyphicon-user"></span>

<strong>${listing.display_fullname.title()}</strong></p>
  <strong> Mobile Phone <span class="glyphicon glyphicon-phone"></span>: </strong>  ${listing.mobile}<br>
%if listing.home_phone:
  <strong> Home Phone <span class="glyphicon glyphicon-phone-alt"></span>:</strong> ${listing.home_phone}<br>
%endif
%if listing.office_phone:
 <strong>Office Phone <span class="glyphicon glyphicon-phone-alt"></span>: </strong> ${listing.office_phone}
%endif

	</div>
</div>
<div class="col-sm-12 col-md-12 shadow " style="top:10px;">
<div style="padding:10px;">
<h3>Email Agent</h3>
${form.begin(url=request.route_url('emailagent', agentemail=listing.users.email),id="agentForm", method="post", role="form")}
${form.csrf_token()}
    ${form.hidden("camefrom", value=listing.id)}
    ${form.hidden("mail",value=listing.users.email)}
  <div class="form-group">
    ${form.text("fullname", class_="form-control required", placeholder="Full Name *")}
  </div>
<div class="form-group">
    ${form.text("mobile", class_="form-control", id="phone", placeholder="Mobile")}
  </div>
  
  <div class="form-group">
    ${form.text("email", class_="form-control required", id="email", placeholder="Your Email *")}
  </div>
  <div class="form-group">
${form.textarea("body",content="Hello, I am Interested in your listing at Kaniru. Please I will like to know more about "+listing.title+", "+listing.address+"."+" Thank you.",class_="form-control required", rows="6")}
  </div>
<div class="form-group">
${form.submit("submit",class_="btn btn-warning btn-block", style="border-radius:0",value="Email Agent")}
</div>
${form.end()}
</div>
</div><!-- /.div -->
</div>
</div>


<%def name="breadcrumb()">
	<ol class="breadcrumb">
		<li><a href="/">Home</a></li>
<li><a href="${request.route_url('browse_location',location_name=listing.state.name)}">State: ${listing.state.name}</a></li>
%if listing.city:
		<li><a href="${request.route_url('browse_city',location_name=listing.city)}">City: ${listing.city}</a></li>
%endif
%if listing.area:
		<li><a href="${request.route_url('browse_area',location_name=listing.area)}">Area: ${listing.area}</a></li>
%endif
		<li><a href="${request.route_url('browse_category',category_name=listing.category.name)}">${listing.category.name}</a></li>
		<li class="active">${listing.subcategory.name}</li>
	</ol>
</%def>
<%inherit file="kaniru:templates/base/base.mako" />
<%namespace file="kaniru:templates/base/uiHelpers.mako" import="validate_errors"/>


<div class="row row-inset">
	<div class="col-sm-12 col-md-12 col-lg-12">
	<div class="panel panel-primary">
  		<div class="panel-heading">
   	 <h1 class="panel-title""> <strong>Tell us more about yourself</strong></h1>
  		</div>
  		<div class="panel-body">
	${form.begin(url=action_url,id="reg", method="POST", class_="form-horizontal", role="form")}
	${form.csrf_token()}
	<div class="form-group ">
	${form.label("Email*", class_="col-sm-3 col-md-3 control-label")}
		<div class="col-sm-4 col-md-4">
		${form.text("email",value=request.session.get('email',''), class_="form-control required")}
		${validate_errors('email')}
		</div>
	</div>
	<div class="form-group ">
		${form.label("First name*",class_="col-sm-3 col-md-3 control-label ")}
			<div class=" col-sm-4 col-md-4">

		${form.text("first_name", class_="form-control required")}
		${validate_errors('first_name')}

		</div>
	</div>
	<div class="form-group ">
		${form.label("Last name*",class_="col-sm-3 col-md-3 control-label")}
		<div class=" col-sm-4 col-md-4">
		${form.text("last_name", class_="form-control required")}
		${validate_errors('last_name')}

		</div>
	</div>
	<div class="form-group ">
		
		${form.label("Password*",class_="col-sm-3 col-md-3 control-label")}
		<div class=" col-sm-4 col-md-4">
		${form.password("password", class_="form-control required")}
		${validate_errors('password')}
		</div>
	</div>
	<div class="form-group ">
		
		${form.label("Confirm Password*",class_="col-sm-3 col-md-3 control-label")}
		<div class=" col-sm-4 col-md-4">
		${form.password("password_confirm", class_="form-control required")}
		${validate_errors('password_confirm')}
		</div>
	</div>
	<div class="form-group ">
		<div class="col-sm-offset-3 col-sm-4 col-md-offset-3 col-md-4">
		${h.checkbox('agent',class_="agentBox",label=" I am an agent")}

	<a href="#" data-toggle="tooltip" data-placement="top" title="Check this if you are an agent"><span class="badge" >?</span>
	</a>
		</div>
	</div>
		<div id="IsAgent" style="display:none">
		<div class="form-group">
		${form.label("Agency Name*",class_="col-sm-3 col-md-3 control-label ")}
		<div class=" col-sm-4 col-md-4">

		${form.text("agency_name", class_="form-control required")}
		</div>
		</div>
		<div class="form-group">
		
		${form.label("Agency State*",class_="col-sm-3 col-md-3 control-label")}
		<div class=" col-sm-4 col-md-4">
		${form.select("state_id",options=states, class_="form-control required")}
		</div>
		</div>
		<div class="form-group">
		
		${form.label("Agency address*",class_="col-sm-3 col-md-3 control-label")}
		<div class=" col-sm-4 col-md-4">

		${form.text("agency_address", class_="form-control required")}
		</div>
		</div>

		<div class="form-group">
		
		${form.label("Agency Website",class_="col-sm-3 col-md-3 control-label")}
		<div class=" col-sm-4 col-md-4">

		${form.text("agency_website", class_="form-control")}
		</div>
		</div>
		


	</div><!--/.IsAgent -->
<div class="form-group ">
		<div class="col-sm-offset-3 col-sm-4 col-md-offset-3 col-md-4">
		${h.checkbox('newsletter',label=" Weekly newsletter",checked=True)}

	
		</div>
	</div>

<div class="form-group">
	<div class="col-sm-offset-3 col-md-offset-3 col-md-4 col-sm-4">
          <button type="submit" class="btn btn-warning btn-block" name="form_submitted" style="border-radius:0px;">Create My Account</button>
		</div>
</div>
	
	${form.end()}
		</div>
	</div>
	</div>

	
</div>

<%def name="breadcrumb()">
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="${request.route_url('login')}">Login</a></li>
    <li class="active">Register</li>

</ol>
</%def>
<%block name="script_tags">
${parent.script_tags()}
<script type="text/javascript">
$(document).ready(function() {
	$("#reg").validate({
	rules:{agency_website:{url:true}}
});
});
</script>
</%block>


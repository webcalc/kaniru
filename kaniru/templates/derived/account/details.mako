<%inherit file="layout.mako"/>
<%namespace file="kaniru:templates/base/uiHelpers.mako" import="validate_errors"/>
<%def name="details()">
	active
</%def>
<div class="panel panel-primary">
  <div class="panel-heading">
    <h2 class="panel-title">Account Details</h2>
  	</div>
  	<div class="panel-body">
	${form1.begin(url=action_url,id="userDetails", method="post", class_="form-horizontal")}
${form1.csrf_token()}
<div class="form-group">
	${form1.label("First name*", class_="col-sm-3 col-md-3 col-lg-3 control-label")}
	<div class="col-sm-6 col-md-6 col-lg-6">
          ${form1.text( name="first_name", value=request.user.first_name,class_="form-control required")}
		${validate_errors("first_name")}
		</div>

</div>
<div class="form-group ">
	${form1.label("Last name*", class_="col-sm-3 col-md-3 col-lg-3 control-label")}
	<div class="col-sm-6 col-md-6 col-lg-6">
          ${form1.text( name="last_name",value=request.user.last_name, class_="form-control required")}
		${validate_errors("last_name")}
		</div>
</div>

 <div class="form-group ">
	${form1.label("Email*", class_="col-sm-3 col-md-3 col-lg-3 control-label")}
	<div class="col-sm-6 col-md-6 col-lg-6">
          ${form1.text( name="email", value=request.user.email,class_="form-control required")}

		${validate_errors("email")}
		</div>

</div>
<div class="form-group ">
${form1.label("user type", class_="col-sm-3 col-md-3 col-lg-3 control-label")}
		<div class="col-sm-4 col-md-4">
		%if request.user.is_agent:
	<label>
    <input type="radio" id="agneg" name="agent" class="not_agent" value="not_agent">
    	I am a private user
  </label>
	
	<label>
    <input type="radio" name="agent" class="profagent" value="agent-info" checked="checked">
    	I am a professional
  </label>
		%else:
			<label>
    <input type="radio" name="agent" class="not_agent" value="not_agent" checked="checked">
    	I am a private user
  </label>
	
	<label>
    <input type="radio" name="agent" class="profagent" value="agent-info" >
    	I am a professional
  </label>
		%endif
	${form1.hidden('isagent', class_="isagent", value=int(request.user.is_agent))}
	<a href="#" data-toggle="tooltip" data-placement="top" title="Check this if you are an agent">
<span class="badge" >?</span>
	</a>
		</div>
	</div>

<div class="form-group">
	${form1.label("Mobile", class_="col-sm-3 col-md-3 col-lg-3 control-label")}
	<div class="col-sm-6 col-md-6 col-lg-6">
          ${form1.text( name="mobile",value=request.user.mobile, class_="form-control")}
		
		</div>

</div>
<div class="form-group">
	${form1.label("Home phone", class_="col-sm-3 col-md-3 col-lg-3 control-label")}
	<div class="col-sm-6 col-md-6 col-lg-6">
          ${form1.text( name="home_phone",value=request.user.home_phone, class_="form-control")}
		</div>

</div>
<div class="form-group">
	${form1.label("office phone", class_="col-sm-3 col-md-3 col-lg-3 control-label")}
	<div class="col-sm-6 col-md-6 col-lg-6">
          ${form1.text( name="office_phone",value=request.user.office_phone, class_="form-control")}
		
		</div>

</div>



	

</div>	
</div>
<button type="submit" name="cancel" onclick="" class="btn btn-default pull-left" style="border-radius:0px;">Cancel</button>

<button type="submit" name="userdetails" class="btn btn-warning pull-right" style="border-radius:0px;">Save</button>
${form1.end()}
<div id="agent-info" style="margin-top:70px;">
<div class="panel panel-primary" >
  <div class="panel-heading">
    <h2 class="panel-title">Agency Details</h2>
  	</div>
  	<div class="panel-body">
	${form2.begin(url=action_url,id="agencyInfo", method="post", class_="form-horizontal", role="form")}
	${form2.csrf_token()}
	<div class="form-group">
	${form2.label("Agency name*", class_="col-sm-3 col-md-3 col-lg-3 control-label")}
	<div class="col-sm-6 col-md-6 col-lg-6">
          ${form2.text( name="agency_name",value=request.user.agency_name, class_="form-control required")}
		</div>

	</div>
	<div class="form-group">
	${form2.label("Agency address*", class_="col-sm-3 col-md-3 col-lg-3 control-label")}
	<div class="col-sm-6 col-md-6 col-lg-6">
          ${form2.text( name="agency_address",value=request.user.agency_address, class_="form-control required")}
		</div>

	</div>
	<div class="form-group">
	${form2.label("Agency Website", class_="col-sm-3 col-md-3 col-lg-3 control-label")}
	<div class="col-sm-6 col-md-6 col-lg-6">
          ${form2.text( name="agency_website",value=request.user.agency_website, class_="form-control")}
<span class="help-block">full url e.g http://www.example.com</span>
		</div>

	</div>

	<div class="form-group">
	${form2.label("Agency Location*", class_="col-sm-3 col-md-3 col-lg-3 control-label")}
	<div class="col-sm-6 col-md-6 col-lg-6">
          ${form2.select("state_id",options=states,selected_value=request.user.state_id, class_="form-control required")}
		</div>

	</div>
	<div class="form-group">
	${form2.label("Agency description*", class_="col-sm-3 col-md-3 col-lg-3 control-label")}
	<div class="col-sm-6 col-md-6 col-lg-6">
          ${form2.textarea( name="agency_note",content=request.user.agency_note,rows="10", class_="form-control required")}
		</div>

	</div>
	
	
	

</div>
</div>
<button type="submit" name="cancel" onclick="" class="btn btn-default pull-left" style="border-radius:0px;">Cancel</button>

<button type="submit" name="prodetails" class="btn btn-warning pull-right" style="border-radius:0px;">Save</button>
${form2.end()}


</div>
<%def name="breadcrumb()">
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="">My account</a></li>
  <li class="active">details</li>
</ol>
</%def>
<%block name="script_tags">
	${parent.script_tags()}
<script type="text/javascript">
	$('#userDetails').validate();
	$('#agencyInfo').validate({
	rules:{
		agency_website:{url:true}
}
});
</script>
</%block>
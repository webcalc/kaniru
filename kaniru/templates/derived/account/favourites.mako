<%inherit file="layout.mako" />
<%def name="favourites()">
	active
</%def>

<div class="panel panel-primary">
  <div class="panel-heading">
    <h2 class="panel-title">Favourite Properties</h2>
  	</div>
  	<div class="panel-body">
		%if paginator:
<p class="pull-right">${paginator.pager(
'$link_previous ~3~ $link_next',
	link_attr={"class":"btn btn-sm btn-primary"},
	curpage_attr={"class":"btn btn-sm btn-default disabled"},
	symbol_next="Next",
	symbol_previous="Previous",
	show_if_single_page=True,
)}
</p>

	<table class="table table-striped">
		<thead>
			<th>Listing</th>
			<th>Type</th>
			<th>Location</th>
			<th>Actions</th>
		</thead>
		<tbody>
	%for listing in paginator.items:
		<tr>

			<td><div class="thumbnail" style="width:200px;height:auto;">
	<a href="${request.route_url('property_view',listing_id=listing.id,slug=listing.slug)}"><img src="${request.storage.url(listing.photos[0].filename)}" class="img-responsive"/></a>
			<div class="captions">
			
			</div>
			</div>
</td>
			<td><p>Category:${listing.category.name}</p>
			<p>Subcategory:${listing.subcategory.name}</p></td>
			<td><p>State: ${listing.state.name}</p>
				<p>City: ${listing.city}</p>
				<p>Area: ${listing.area}</p>
</td>
			<td></td>
	
		</tr>
	%endfor
	</table>
%else:
	<i>You have not saved any property</i>
	%endif
		
	</div>
</div>

<%def name="breadcrumb()">
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="">My account</a></li>
  <li class="active">Favourites</li>
</ol>
</%def>

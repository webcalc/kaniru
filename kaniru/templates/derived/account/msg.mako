<%inherit file="layout.mako" />
<%def name="message()">
	active
</%def>

<div class="panel panel-primary">
  <div class="panel-heading">
    <h2 class="panel-title">Messages</h2>
  	</div>
  	<div class="panel-body">
		
<p>No messages found</p>
	
		
	</div>
</div>

<%def name="breadcrumb()">
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="">My account</a></li>
  <li class="active">Messages</li>
</ol>
</%def>

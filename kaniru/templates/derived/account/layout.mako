<%inherit file="kaniru:templates/base/base.mako" />
<div class="row row-inset" id="accountProfile">
	<div class="col-sm-3 col-md-3 col-lg-3">
		<div class="panel panel-primary">
  		<div class="panel-heading">
   		 <h3 class="panel-title">My Account</h3>
  		</div>
  		<div class="panel-body" id="accountDetails">
<ul class="nav">
		<li class="${self.overview()}"><a href="${request.route_url('profile')}">OVERVIEW</a></li>
  		<li class="${self.details()}"><a href="${request.route_url('details')}">DETAILS</a></li>
		<li class="${self.favourites()}"><a href="${request.route_url('favourites')}">FAVOURITES</a></li>
		<li class="${self.all_listing()}"><a href="${request.route_url('user_listings')}">ALL LISTINGS</a></li>
	<li class="${self.message()}"><a href="${request.route_url("messages")}">MESSAGES</a></li>

     </ul>
		</div><!--/.panel-body -->
		</div><!-- /.p success-->
	</div><!-- /.col-md-3 -->
		
	<div class="col-sm-9 col-md-9 col-lg-9">
		${next.body()}
</div>
</div><!-- /.row -->


<%def name="details()" />
<%def name="overview()" />
<%def name="all_listing()" />
<%def name="favourites()" />
<%def name="message()" />

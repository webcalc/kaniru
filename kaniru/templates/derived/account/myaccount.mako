<%inherit file="layout.mako"/>
<%def name="overview()">
	active
</%def>

<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">My Account <p class="pull-right">${request.user.first_name} ${request.user.last_name}</p></h3>
  	</div>
  	<div class="panel-body">
<div class="col-md-4">
<p>Please click here to manage your account informations</p>
		<a href="${request.route_url('details')}" class="btn btn-success btn-block green" style="border-radius:0">Account Details</a>
		</div>
<div class="col-md-4">
<p>Click here to manage your list of favourite properties</p>
		<a href="${request.route_url('favourites')}" class="btn btn-success btn-block green" style="border-radius:0">Favourites</a>

		</div>
<div class="col-md-4">
<p>Click here to manage your messages</p><br>
		<a href="${request.route_url("messages")}" class="btn btn-success btn-block green" style="border-radius:0">Messages</a>

		
	</div>

<div class="col-sm-offset-3 col-md-offset-3 col-md-6 col-sm-6" style="margin-top:10px;margin-bottom:10px;">
	<a href="${request.route_url('create_listing')}" class="btn btn-warning btn-block" style="border-radius:0">Create a new listing</a>

</div>

%if request.user.agency_logo:

 <div class="thmbnail">
	<img src="${request.storage.url(request.user.agency_logo)}" class="img-responsive" width="200px" height="auto">
	</div><br>
${form.begin(url=action_url,method="post",id="agupload" ,multipart=True)}
${form.csrf_token()}
${form.file('pics',class_="required")}
<span class="help-block">To change your logo, simply choose file and click "Change" </span>
${form.submit('submit',class_="btn btn-warning",value="Change", style="border-radius:0")}
${form.end()}
</div>
%else:

</div>
<div style="padding:10px;">
	${form.begin(url=action_url,id="agupload" ,method="post",multipart=True)}
${form.csrf_token()}
${form.file('pics',class_="required")}
<span class="help-block">Oops! Your company logo is missing. To set logo, simply choose file and click Upload</span>
${form.submit('submit',class_="btn btn-warning",value="Upload", style="border-radius:0")}
${form.end()}
</div>
%endif
</div>

	<%def name="breadcrumb()">
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li class="active">My Account</li>
</ol>
</%def>
<%block name="script_tags">
	${parent.script_tags()}
<script>
	$("#agupload").validate();
</script>
</%block>

<%inherit file="kaniru:templates/base/base.mako" />
<div class="row" id="homesearch">
	<div class="col-md-8" id="searchform">
<fieldset>
<form id="fpform" class="form-inline" action="${request.route_url('prop_search')}" role="form" method="GET" >
<br>
<strong>Search Properties</strong><br><br>
<div class="form-group has-success  has-feedback">
<select class="form-control control" name='type_id' id="type_id">
<option value="">Any Property Type</option>
<option value="apartment">Apartment</option>
<option value="house">House</option>
<option value="commercial">Commercial</option>
<option value="development">Development</option>
<option value="land">Land</option>
</select>
  <span class="glyphicon glyphicon-chevron-down form-control-feedback"></span>
</div>
<div class="form-group">
	<input type="text" class="form-control control" placeholder="Max Price" name="price">
</div>
<div class="form-group has-success has-feedback">
	<select class="form-control control" name="bed">
<option value="">Any Bed</option>
<option value="2">2 +</option>
<option value="3">3 +</option>
<option value="4">4 +</option>
<option value="5">5 +</option>
<option value="6">6 +</option>
<option value="7">7 +</option>

</select>

  <span class="glyphicon glyphicon-chevron-down form-control-feedback"></span>
</div>
	<button style="border-radius:0;"class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-search"></span></button>
<br><br>
<div class="form-group has-success" id="fulltext">
	<input type="text" placeholder="Enter keywords to search e.g flats,etc" name="fulltext" class="form-control">
</div>
<div id="movup" class="col-md-offset-4 fulltext bg-primary text-center ">fulltext search</div>
</form>

	</fieldset>
<table class="table subscribe">
<tbody>
	<tr>
	<td><a class="pull-left" href="#">
    <img class="media-object" src="/static/subscribelogo.png" alt="">
  </a></td>
	<td><h4 class="media-heading" style="color:#fff;">Subscribe to newsletter</h4>
	<p style="font-size:10px;">Subscribe to our newsletter to get access to our news and<br> resources. Your email will be safe.Unsubscribe at any time</p></td>
	<td style="width:250px;vertical-align:bottom;">
 <form class="form-inline", action="${request.route_url('subscribemail')}", role="form" id="homepageform">
			<div class="input-group">
      			<input name="email",id="email" type="email" class="form-control" data-msg-required=" " placeholder="Your Email" style="border-radius:0">
      			<span class="input-group-btn">
        	<button class="btn btn-success green" type="submit" style="border-radius:0" id="subemail">Sign up</button>
      </span>
    		</div>
			
		</form>
</td>
	</tr>
</tbody>
</table>



	</div>
	<div class="col-md-4" id="konga" >
	%if ad:
	%if ad.filename:
		<a href="${ad.link}" target="_blank"><img src="${request.storage.url(ad.filename)}"></a>
	%elif ad.url:
		${ad.url|n}
	%endif
	%else:
		<a href=""><img src="/static/images/konga.png"></a>
	%endif
		</div>
</div>
		<div class="row">
		<div class="col-md-12" id="topproducts">
		${topproducts|n}

		</div>
		</div>
		<div class="row" style="margin-bottom:10px;">
	<div class="col-sm-8 col-md-8"  style="border:2px solid #ccc;padding:10px;">
<div class="row">
	<div class="col-sm-6 col-md-6">
	<h3  style="color:#287eb6"><strong>Welcome to Kaniru</strong></h3>
	<p> 
Kaniru provides a platform for advertising and finding property in Nigeria. The listed property include flats, houses, commercial property and land to rent and for sale listed by organisations and Nigerian private property owners.

 </p>
</div>
	<div class="col-sm-6 col-md-6">
		<h3 style="color:#287eb6"><strong>Join Us</strong></h3>
		<strong>Connect to your account</strong>
		<p>Login and start to buy, bid, sell and manage your account</p>
		<a href="${request.route_url('login')}" class="btn btn-primary blue" style="border-radius:0">Login</a>
	<h4>Not registered yet?</h4>
	<a href="${request.route_url('registration')}" class="btn btn-primary blue" style="border-radius:0">Register</a>
		</div>
</div>
	</div>
	<div class="col-sm-4 col-md-4">

<iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fkaniru.com.ng&amp;width=293&amp;layout=box_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=65" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:293px; height:65px;" allowTransparency="true"></iframe>
<iframe src="//www.facebook.com/plugins/facepile.php?app_id&amp;href=https%3A%2F%2Fwww.facebook.com%2Fkaniru.com.ng&amp;action&amp;width=293&amp;height&amp;max_rows=2&amp;colorscheme=light&amp;size=medium&amp;show_count=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:293px;" allowTransparency="true"></iframe>
	</div>
</div>
<div class="row">
	<h4><strong>Featured Listings</strong></h4>
	<div class="col-md-12" id="featuredproducts">
	
		${featuredlistings|n}
	</div>
</div>


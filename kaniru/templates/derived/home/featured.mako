<div id="featuredproducts-carousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
		
%for listing in listings[0:5]:
		<a href="${request.route_url('property_view',listing_id=listing.id,slug=listing.slug)}">
		<img src="${request.storage.url(listing.photos[0].filename)}" style="display:inline-block"/>
	<div>
	<h4>${listing.featured_title.title()}</h4>
		<p class="price">
				${listing.naira_price}
		</p>
		<p class="listingdetails">
		<span class="glyphicon glyphicon-map-marker"></span>
			${listing.state.name}
		</p>
	</div>
			</a>
			
%endfor
		
    </div>
%if len(listings)>5:

    <div class="item">
%for listing in listings[5:11]:
<a href="${request.route_url('property_view',listing_id=listing.id, slug=listing.slug)}">
<img src="${request.storage.url(listing.photos[0].filename)}" style="display:inline-block">
<div>
	<h4>${listing.featured_title.title()}</h4>
		<p class="price">
				${listing.naira_price}
		</p>
		<p class="listingdetails">
		<span class="glyphicon glyphicon-map-marker"></span>
			${listing.state.name}
		</p>
	</div>

	</a>
%endfor
	</div>
	%endif

  </div>

  <!-- Controls -->
 <a class="left controls btn btn-success" href="#featuredproducts-carousel" data-slide="prev" style="color:#fff;top:-130px; left:-20px; border-radius:0">
   <strong> < </strong>
  </a>
  <a class="right controls btn btn-success pull-right" href="#featuredproducts-carousel" data-slide="next" style="color:#fff;top:-130px; right:-20px; border-radius:0">
   <strong> ></strong>
  </a>
  
</div>
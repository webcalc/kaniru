<div id="topproducts-carousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
		
%for listing in listings[0:3]:
	
		<a href="${request.route_url('property_view',listing_id=listing.id,slug=listing.slug)}">
		<img src="${request.storage.url(listing.photos[0].filename)}" class="pull-left" style="display:inline-block"/>
	<div>
	<h4>${listing.featured_title.title()}</h4>
		<p class="price">
				${listing.naira_price}
		</p>
		<p class="listingdetails">
		<span class="glyphicon glyphicon-map-marker"></span>
			${listing.state.name}
		</p>
</div>	

			</a>
			
%endfor
		
    </div>
%if len(listings)>3:

<div class="item">

    
%for listing in listings[3:7]:
<a href="${request.route_url('property_view',listing_id=listing.id, slug=listing.slug)}">
<img src="${request.storage.url(listing.photos[0].filename)}" class="pull-left" style="display:inline-block"/>
	<div>
	<h4>${listing.featured_title.title()}</h4>
		<p class="price">
				${listing.naira_price}
		</p>
		<p class="listingdetails">
		<span class="glyphicon glyphicon-map-marker"></span>
			${listing.state.name}
		</p>
</div>	

	</a>
%endfor
	</div>

	%endif
  </div>

  <!-- Controls -->
 <a class="left controls btn btn-success" href="#topproducts-carousel" data-slide="prev" style="color:#fff;top:-90px;left:-20px; border-radius:0">
   <strong> < </strong>
  </a>
  <a class="right controls btn btn-success pull-right" href="#topproducts-carousel" data-slide="next" style="color:#fff;top:-90px; right:-20px; border-radius:0">
   <strong> ></strong>
  </a>
  
</div>
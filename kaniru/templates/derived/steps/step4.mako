<%inherit file="kaniru:templates/base/baselist.mako" />
<%def name="step4()">
	active
</%def>

##step1
<div class="row row-inset">
	<div class="col-md-12">
${form.begin(url = action_url, method="post",class_="form-horizontal")}
${form.csrf_token()}
	<div class="panel panel-primary">
	<div class="panel-heading">
    <h3 class="panel-title">Price</h3>
  </div>
  	<div class="panel-body">
    	

	<div class=" col-md-8">
		
		<div class="form-group">
		${form.label("Fullname", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
			<div class="col-sm-8 col-md-8 col-lg-8">
		${form.text("display_fullname", value=request.user.first_name+' '+request.user.last_name ,class_ = "form-control")}
		${form.errorlist("display_fullname")}
			</div>
		</div>
		<div class="form-group">
		${form.label("E-mail", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
		<div class="col-sm-8 col-md-8 col-lg-8">
		${form.text("display_email", value = request.user.email,class_ = "form-control")}
		${form.errorlist("email")}
			</div>
		</div>
		<div class="form-group">
		${form.label("Mobile phone", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
			<div class="col-sm-8 col-md-8 col-lg-8">
		${form.text("mobile",value=request.user.mobile, class_ = "form-control")}
		${form.errorlist("mobile")}
			</div>
		</div>
	<div class="form-group">
		${form.label("home phone", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
			<div class="col-sm-8 col-md-8 col-lg-8">
		${form.text("home_phone",value=request.user.home_phone, class_ = "form-control")}
		${form.errorlist("home_phone")}
			</div>
		</div>
		<div class="form-group">
		${form.label("Office phone", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
			<div class="col-sm-8 col-md-8 col-lg-8">
		${form.text("office_phone",value=request.user.office_phone, class_ = "form-control")}
		${form.errorlist("office_phone")}
			</div>
		</div>
		</div>
	<div class=" col-md-4">

		</div>
  </div>
</div>
<button type="submit" name="step_back" class="btn btn-success green pull-left" style="border-radius:0">Back</button>
<button type="submit" name="step_to_5" class="btn btn-success green pull-right" style="border-radius:0">Continue to the next step</button>

${form.end()}

	</div><!-- /.col-md-12 -->
</div><!-- /.row -->
	<%def name="breadcrumb()">
	<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="${request.route_url("create_listing")}">Steps</a></li>
  <li><a href="${request.route_url("step1")}">Start</a></li>
  <li><a href="${request.route_url("step2")}">Details</a></li>
	  <li><a href="${request.route_url("step3")}">Uploads</a></li>
  <li class="active">Contact Setting</li>
</ol>

		</%def>

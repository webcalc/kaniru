<%inherit file="kaniru:templates/base/base.mako"/>
<div class="row row-inset ">
	<div class="col-md-12">
		<div class="panel panel-primary">
  			<div class="panel-heading">
    			<h1 class="panel-title">List Your Property On Kaniru</h1>
  			</div>
  			<div class="panel-body">
			<h5><strong>Choose how you want to list your property</strong></h5>
			<p>List your property on kaniru. It's easy and fast. Reach thousands of potential customer within minutes.</p>
${form.begin(url = action_url, method="post",class_="form-horizontal")}
${form.csrf_token()}
			<div class="row ">
			<div class="col-md-5">
			<button class="btn btn-success btn-lg" type="submit" name="for_sale" style="border-radius:0"><span class="glyphicon glyphicon-home"></span>
   LIST YOUR PROPERTY FOR SALE</button>
				</div>
			<div class="col-md-2 text-center">
			<h4>OR</h4>
			</div>
			<div class="col-md-5 ">
			<button class="btn btn-primary btn-lg pull-right" type="submit" name="for_rent"  style="border-radius:0"><span class="glyphicon glyphicon-home"></span>
   LIST YOUR PROPERTY FOR RENT</button>
			</div>
			</div><!-- /.row -->
${form.end()}
		</div>
</div>

</div>
</div>



<%def name="breadcrumb()">
	<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li class="active">Create new listing</li>
</ol>

		</%def>


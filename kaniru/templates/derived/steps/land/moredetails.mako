<div class="panel panel-primary">
<div class="panel-heading">
    <h3 class="panel-title">More Property Details</h3>
  </div>
  	<div class="panel-body">
	<div class=" col-md-8">
		
		<div class="form-group">
		${form.label("Year Build", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
			<div class="col-sm-8 col-md-8 col-lg-8">
		${form.text("year_built", class_ = "form-control")}
		${form.errorlist("year_built")}
			</div>
		</div>
		<div class="form-group">
		${form.label("Car Spaces", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
			<div class="col-sm-8 col-md-8 col-lg-8">
		${form.text("car_spaces", class_ = "form-control")}
		${form.errorlist("car_spaces")}
			</div>
		</div>
		
		
		</div>
	<div class=" col-md-4">

		</div>


</div>
</div>
  


		<div class="row" id="detail" style="border:1px solid #ccc;margin-left:8px;margin-right:8px;">
				<div class="col-md-12">
<h4><strong>${obj1.title.title()}</strong></h4>
		<p><span class="glyphicon glyphicon-map-marker"></span><strong>Address:</strong>${obj1.address.title()}</p>
	

				</div>
<div class="row">
			<div class="col-md-8" id="main-content">
		
<div id="carousel-kaniru" class="carousel slide" data-ride="carousel">
  
<div class="big-image">
  <!-- Wrapper for slides -->
  <div class="carousel-inner" >
    <div class="item active">
      <img src="${request.storage.url(files[0])}" class="img-responsive">
      
	</div>
%for file in range(len(files[1:])) :
    ${self.my_images(file)}
%endfor
    
  </div>
<div class="col-md-offset-5">
	<a class="btn btn-default left controls" href="#carousel-kaniru" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="btn btn-default right controls" href="#carousel-kaniru" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>
</div><!-- /.big-image -->
</div>
	</div><!-- /. col-md-8-->
	<div class="col-md-4">
		<div class="col-md-12">
			<p class="pull-left"style="font-size:14px">Price</p>
			<p class="pull-right" style="font-size:16px; color:#29721;"><b> ${obj2.naira_price}</b></p>
		
		
		<table class="table table-striped">
		<tbody>
%if obj1.category_id==2:

				<tr>
				<td><strong>Bedrooms</strong></td>
				<td>${obj2.bedroom}</td>
			 </tr>
				<tr>
					<td><strong>Bathrooms</strong></td>
					<td>${obj2.bathroom}</td>
				</tr>
				<tr>
					<td><strong>Total Rooms</strong></td>
					<td>${obj2.total_room}</td>
				</tr>
				<tr>
					<td><strong>Living Area Size</strong></td>
					<td>${obj2.living_area}</td>
				</tr>
				<tr>
					<td><strong>Floor</strong></td>
					<td>${obj2.floor}</td>
				</tr>
				<tr>
					<td><strong>Total Floor</strong></td>
					<td>${obj2.total_floor}</td>
				</tr>
				<tr>
					<td><strong>Available from</strong></td>
					<td>${obj2.date_available}</td>
				</tr>

				
%elif obj1.category_id==3:
				<tr>
					<td><strong>Plot size</strong></td>
					<td>${obj2.plot_size}</td>
				</tr>
				<tr>
					<td><strong>Available from</strong></td>
					<td>${obj2.date_available}</td>
				</tr>

%elif obj1.category_id==4:
				<tr>
					<td><strong>Total Rooms</strong></td>
					<td>${obj2.total_room}</td>
				</tr>
				<tr>
				<td><strong>Building Size</strong></td>
				<td>${obj2.building_size}</td>
				</tr>
				<tr>
					<td><strong>Available from</strong></td>
					<td>${obj2.date_available}</td>
				</tr>
%else:
				<tr>
				<td><strong>Bedrooms</strong></td>
				<td>${obj2.bedroom}</td>
			 </tr>
				<tr>
					<td><strong>Bathrooms</strong></td>
					<td>${obj2.bathroom}</td>
				</tr>
				<tr>
					<td><strong>Total Rooms</strong></td>
					<td>${obj2.total_room}</td>
				</tr>
				<tr>
					<td><strong>Living Area Size</strong></td>
					<td>${obj2.living_area}</td>
				</tr>
				<tr>
					<td><strong>Land Size</strong></td>
					<td>${obj2.land_size}</td>
				</tr>
				<tr>
					<td><strong>Available from</strong></td>
					<td>${obj2.date_available}</td>
				</tr>

%endif
				
		<tbody>
		</table>
		</table>
		
		</div>
		</div>
</div>
<div class="col-sm-12 col-md-12" 
style="top:10px; padding:10px;">
	<div class="h4"><strong>Description</strong></div>
<hr style="margin-top:-5px;">
		<p>${obj2.description}</p>
<div class="h4"><strong>More Details</strong></div>
<hr style="margin-top:-5px;">
<table class="table table-striped">
	<tbody>
<tr>
%if obj2.year_built:
	<td>Year Built: ${obj2.year_built}</td>
%endif
%if obj2.car_spaces:
	<td>Car Spaces: ${obj2.car_spaces}</td>
%endif
</tr>
<tr>
%if obj2.price_condition:
	<td>Price Conditions: ${obj2.price_condition}</td>
%endif
%if obj2.deposit:
	<td>Deposit: ${obj2.naira_deposit}</td>
%endif
</tr>
<tr>
%if obj2.agent_commission:
	<td>Agent Commission: ${obj2.agent_commission}</td>
%endif
</tr>
</tbody>
</table>
</div>
</div>
<%def name="my_images(id)">
<div class="item">
<img src="${request.storage.url(files[id+1])}" class="img-responsive">
</div>
</%def>
<%def name="my_thumbs(id)">

</%def>


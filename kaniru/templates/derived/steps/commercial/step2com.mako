<%inherit file="kaniru:templates/base/baselist.mako" />
<%def name="step2()">
	active
</%def>

##step1
<div class="row row-inset">
	<div class="col-md-12">
${form.begin(url = action_url, method="post",class_="form-horizontal")}
${form.csrf_token()}
	<div class="panel panel-primary">
	<div class="panel-heading">
    <h3 class="panel-title">Price</h3>
  </div>
  	<div class="panel-body">
    	

	<div class=" col-md-8">
		
		<div class="form-group">
		${form.label("price*", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
			<div class="col-sm-8 col-md-8 col-lg-8">
		${form.text("price", class_ = "form-control")}
		${form.errorlist("price")}
			</div>
		</div>
		<div class="form-group">
		${form.label("currency*", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
		<div class="col-sm-8 col-md-8 col-lg-8">
		${form.select("currency",options=[('Naira','Naira')],
			 class_="form-control")}
		${form.errorlist("currency")}
			</div>
		</div>

		<div class="form-group">
		${form.label("price conditions", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
			<div class="col-sm-8 col-md-8 col-lg-8">
		${form.text("price_condition", class_ = "form-control")}
		${form.errorlist("price_condition")}
			</div>
		</div>
	<div class="form-group">
		${form.label("Deposit", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
			<div class="col-sm-8 col-md-8 col-lg-8">
		${form.text("deposit", class_ = "form-control")}
		${form.errorlist("deposit")}
			</div>
		</div>
		<div class="form-group">
		${form.label("Agent Commission(if any)", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
			<div class="col-sm-8 col-md-8 col-lg-8">
		${form.text("agent_commission", class_ = "form-control")}
		${form.errorlist("agent_commission")}
			</div>
		</div>
		</div>
	<div class=" col-md-4">

		</div>
  </div>
</div>
<%include file="comdetails.mako"/>
<%include file="description.mako" />
<%include file="moredetails.mako" />
<button type="submit" name="step_back" class="btn btn-success green pull-left " style="border-radius:0"> Back to basic</button>
<button type="submit" name="step_to_3" class="btn btn-success green pull-right" style="border-radius:0">Continue to the next step</button>

${form.end()}

	</div><!-- /.col-md-12 -->
</div><!-- /.row -->
	<%def name="breadcrumb()">
	<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="${request.route_url("create_listing")}">Steps</a></li>
  <li><a href="${request.route_url("step1")}">Start</a></li>
  <li class="active">details</li>
</ol>

		</%def>

<div class="panel panel-primary">
<div class="panel-heading">
    <h3 class="panel-title">Property Details</h3>
  </div>
  	<div class="panel-body">
	<div class=" col-md-8">
		
		<div class="form-group">
		${form.label("bedrooms*", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
			<div class="col-sm-8 col-md-8 col-lg-8">
		${form.text("bedroom", class_ = "form-control")}
		${form.errorlist("bedroom")}
			</div>
		</div>
		<div class="form-group">
		${form.label("bathrooms*", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
			<div class="col-sm-8 col-md-8 col-lg-8">
		${form.text("bathroom", class_ = "form-control")}
		${form.errorlist("bathroom")}
			</div>
		</div>

		<div class="form-group">
		${form.label("Rooms(total)", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
			<div class="col-sm-8 col-md-8 col-lg-8">
		${form.text("total_room", class_ = "form-control")}
		${form.errorlist("total_room")}
			</div>
		</div>
		<div class="form-group">
		${form.label("living area*", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
		<div class="col-sm-8 col-md-8 col-lg-8">
		${form.text("living_area", class_="form-control")}
		<span class="help-block">e.g 100 by 100 or 534m2</span>
		${form.errorlist("living_area")}
			</div>
		</div>
		<div class="form-group">
		${form.label("Land Size*", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
			<div class="col-sm-8 col-md-8 col-lg-8">
		${form.text("land_size", class_ = "form-control")}
		<span class="help-block">e.g 100 by 100 or 534m2</span>
		${form.errorlist("land_size")}
			</div>
		</div>
		<div class="form-group">
		${form.label("Available from", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
			<div class="col-sm-8 col-md-8 col-lg-8">
		${form.text("date_available", class_ = "form-control")}
		${form.errorlist("date_available")}
			</div>
		</div>
		</div>
	<div class=" col-md-4">

		</div>


</div>
</div>
  
<div class="modal fade" id="previewModal" tabindex="-1" role="dialog" aria-labelledby="previewModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="margin-top:40px;width:700px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="previewModalLabel">Listing Preview</h4>
      </div>
      <div class="modal-body" style="height:500px;overflow:scroll;">
	
<%include file="carousel.mako"/>
	<!-- Modal-->
 </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>


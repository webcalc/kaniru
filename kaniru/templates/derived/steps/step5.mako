<%inherit file="kaniru:templates/base/baselist.mako" />
<%def name="step5()">
	active
</%def>


<div class="row row-inset">
	<div class="col-md-12">
${form.begin(url = action_url, method="post",class_="form-horizontal")}
${form.csrf_token()}
	<div class="panel panel-primary">
	<div class="panel-heading">
    <h3 class="panel-title">Overview</h3>
  </div>
  	<div class="panel-body">
<article>
	<heading>
	<h4>${obj1.title.title()}
	<span class="pull-right"> ${obj2.naira_price}</span></h4>
	</heading>
<div class="detailwrap">
	
	<a href="#previewModal" data-toggle="modal"><img src="${request.storage.url(files[0])}" class="img-responsive pull-left thumbnail" height="120" width="120"/></a>

		
	<ul class="list-inline">
	
%if obj1.category_id==2:
	<li><h5><strong>Apartment:</strong></h5></li>
	<li> <strong>${obj2.bedroom}</strong> Bedroom</li>
			<li><strong> ${obj2.bathroom}</strong> Baths</li>
			<li> <strong>${obj2.total_room}</strong> Rooms(total)</li>
<li> <strong>${obj2.living_area}</strong> Living area</li>

<li> <strong>${obj2.floor}</strong> Floor</li>
<li> <strong>${obj2.total_floor}</strong> Total Floor</li>

%elif obj1.category_id==3:
	<li><h5><strong>Land:</strong></h5></li>
	<li> <strong>${obj2.plot_size}</strong> Plot size</li>
%elif obj1.category_id==4:
	<li><h5><strong>Commercial:</strong></h5></li>
<li> <strong>${obj2.total_room}</strong> Total Rooms</li>
<li> <strong>${obj2.building_size}</strong> Building Size</li>

		
%elif obj1.category_id==5:
			<li><h5><strong>Development:</strong></h5></li>
	 <li> <strong>${obj2.bedroom}</strong> Bedroom</li>
			<li><strong> ${obj2.bathroom}</strong> Baths</li>
			<li> <strong>${obj2.total_room}</strong> Rooms(total)</li>
		<li> <strong>${obj2.living_area}</strong> Living Area</li> 
		<li> <strong>${obj2.land_size}</strong> Land Size</li> 
else:
			<li><h5><strong>House:</strong></h5></li>
	 <li> <strong>${obj2.bedroom}</strong> Bedroom</li>
			<li><strong> ${obj2.bathroom}</strong> Baths</li>
			<li> <strong>${obj2.total_room}</strong> Rooms(total)</li>
		<li> <strong>${obj2.living_area}</strong> Living Area</li> 
		<li> <strong>${obj2.land_size}</strong> Land Size</li> 
%endif
		</ul>
     
	<a href="#previewModal" data-toggle="modal"><button type="submit" class="btn btn-success green pull-right" style="border-radius:0" >Details</button></a>
	
<%include file="previewmodal.mako" />
</div><!--/detailwrap -->
</article>
  </div>
</div>
<button type="submit" name="step_back" class="btn btn-success green pull-left " style="border-radius:0">Back</button>
<button type="submit" name="publish" class="btn btn-success green pull-right" style="border-radius:0">Publish</button>

${form.end()}

	</div><!-- /.col-md-12 -->
</div><!-- /.row -->
<%def name="breadcrumb()">
	<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="${request.route_url("create_listing")}">Steps</a></li>
  <li><a href="${request.route_url("step1")}">Start</a></li>
  <li><a href="${request.route_url("step2")}">Details</a></li>
	  <li><a href="${request.route_url("step3")}">Uploads</a></li>
	  <li><a href="${request.route_url("step4")}">Contact setting</a></li>
  <li class="active">Review and Publish</li>
</ol>

		</%def>



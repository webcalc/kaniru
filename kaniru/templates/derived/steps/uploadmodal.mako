<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel" aria-hidden="true">
  <div class="modal-dialog" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="uploadModalLabel">Upload Your file</h4>
      </div>
      <div class="modal-body">

	${form.begin(url=request.route_url('upload'), method="POST",multipart=True, role="form")}
	${form.csrf_token()}
	${form.file("pics", class_="form-control")}<br>
	${form.submit(name="submit",
class_="btn btn-success",value="Upload")}
	${form.end()}

 </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>


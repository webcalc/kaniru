<div class="panel panel-primary">
<div class="panel-heading">
    <h3 class="panel-title">Choose A Location</h3>
  </div>
  	<div class="panel-body">
    	

	<div class=" col-md-8">
		
		<div class="form-group">
		${form.label("State*", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
			<div class="col-sm-8 col-md-8 col-lg-8">
		${form.select("state_id",options=states,prompt="Please Select A State",
		 class_="save form-control")}
		${form.errorlist("state_id")}
			</div>
		</div>
		<div class="form-group" id="city">
		${form.label("city", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
		<div class="col-sm-8 col-md-8 col-lg-8">
		${form.text("city", class_ = "form-control")}
		
			</div>
		</div>
		<div class="form-group" id="area">
		${form.label("area", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
		<div class="col-sm-8 col-md-8 col-lg-8">
		${form.text("area", class_ = "form-control")}
		
			</div>
		</div>
		</div>
	<div class=" col-md-4">

		</div>
<div class="row">
	<div class="col-md-12">
<h3 class="panel-title">Address</h3>
	<hr>


<%include file = "addressmap.mako"/>
</div>
</div>
  </div>


</div>

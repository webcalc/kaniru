<%inherit file="kaniru:templates/base/baselist.mako" />
<%def name="step3()">
	active
</%def>

##step3
<div class="row row-inset">
	<div class="col-md-12">
	<div class="panel panel-primary">
	<div class="panel-heading">
    <h3 class="panel-title">Uploads</h3>
  </div>
  	<div class="panel-body">
		
<div class="row">
		<div class="col-md-12">
${form.begin(url = request.route_url('upload'),id="filestep",multipart=True, method="post",class_="form-horizontal")}
${form.csrf_token()}
				${form.file("pics",class_="required", accept="image/jpg, image/png,image/jpeg")}
			${form.errorlist("pics")}
<br>
			${form.submit("submit","Upload",class_="btn btn-warning")}
			
${form.end()}
<br>
${up|n}

			</div>
	</div>
		</div>
		</div>

	${self.my_form()}
	</div><!-- /.col-md-12 -->

</div><!-- /.row -->
	<%def name="breadcrumb()">
	<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="${request.route_url("create_listing")}">Steps</a></li>
  <li><a href="${request.route_url("step1")}">Start</a></li>
  <li><a href="${request.route_url("step2")}">details</a></li>
  <li class="active">Uploads</li>
</ol>

		</%def>
<%def name="my_form()">
${form.begin(url = action_url,multipart=True, method="post",class_="form-horizontal")}
${form.csrf_token()}
<button type="submit" name="step_back" class="btn btn-success green pull-left " style="border-radius:0">Back</button>
<button type="submit" name="step_to_4" class="btn btn-success green pull-right " style="border-radius:0">Continue to the next step</button>
${form.end()}
</%def>


<%block name="script_tags">
${parent.script_tags()}
<script type="text/javascript">
$(document).ready(function() {
	$("#filestep").validate();
});
</script>
</%block>


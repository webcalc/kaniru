<%inherit file="kaniru:templates/base/baselist.mako" />

##step1
<div class="row row-inset">
	<div class="col-md-12">
${form.begin(url = action_url,id="step1", method="post",class_="form-horizontal")}
${form.csrf_token()}
	<div class="panel panel-primary">
	<div class="panel-heading">
    <h3 class="panel-title">Basic Info</h3>
  </div>
  	<div class="panel-body">
    	

	<div class=" col-md-8" id="CategorySelector">
		
		<div class="form-group">
		${form.label("title*", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
			<div class="col-sm-8 col-md-8 col-lg-8">
		${form.text("title", class_ = "form-control required")}
		<span class="help-block" style="color:#ccc">Please enter a nice title for your property</span>
		${form.errorlist("title")}
			</div>
		</div>
		<div class="form-group">
		${form.label("Property Type*", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
		<div class="col-sm-8 col-md-8 col-lg-8" >
		${form.select("category_id",id="category_id",options=categories,prompt="Select Property Type",
		 class_="form-control required")}
		${form.errorlist("category_id")}
			</div>
		</div>
		<div class="form-group" id="subcategory" >
		${form.label("Category*", class_="col-sm-4 col-md-4 col-lg-4 control-label")}
		<div class="col-sm-8 col-md-8 col-lg-8">
		${form.select("subcategory_id",options=[],
			 class_="form-control required")}
		${form.errorlist("subcategory_id")}
			</div>
		</div>

		</div>
	<div class=" col-md-4" >

		</div>
  </div>
</div>
<%include file="location.mako"/>
<button type="submit" class="btn btn-default pull-left disabled" style="border-radius:0">Back</button>
<button type="submit" name="step_to_2" class="btn btn-success green pull-right " style="border-radius:0">Continue to the next step</button>

${form.end()}

	</div><!-- /.col-md-12 -->
</div><!-- /.row -->
	<%def name="breadcrumb()">
	<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="${request.route_url("create_listing")}">Steps</a></li>
  <li class="active">start</li>
</ol>

		</%def>
<%block name="script_tags">
${parent.script_tags()}
<script src="${request.static_url('kaniru:static/listing-creation.js')}"></script>

<script type="text/javascript">
$(document).ready(function() {
	$("#step1").validate();
});
</script>
</%block>
<%def name="step1()">
	active
</%def>

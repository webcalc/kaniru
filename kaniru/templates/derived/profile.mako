<%inherit file="kaniru:templates/base/base.mako" />

<div class="row row-inset" id="accountProfile">
	<div class="col-sm-3 col-md-3 col-lg-3">
		<div class="panel panel-success">
  		<div class="panel-heading">
   		 <h3 class="panel-title">My Profile</h3>
  		</div>
  		<div class="panel-body">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#profile" data-toggle="tab">Profile View</a></li>
  		<li><a href="#details" data-toggle="tab">My Details</a></li>
		<li><a href="#favourites" data-toggle="tab">My Favourites</a></li>
		<li><a href="#listings" data-toggle="tab">My Listings</a></li>
	<li><a href="#messages" data-toggle="tab">Messages</a></li>

     </ul>
		</div>
</div>
	</div>
	<div class="col-sm-9 col-md-9 col-lg-9">


<div class="tab-content">
  

<div class="tab-pane active" id="profile">
	<div class="panel panel-success">
  		<div class="panel-heading">
   		 <h3 class="panel-title">Overview <p class="pull-right"> ${request.user.first_name} ${request.user.last_name}</p></h3>
  		</div>
  		<div class="panel-body">
<p>I am the profile</p>
		</div>
	</div>
</div><!-- /.ends profile -->

  <div class="tab-pane" id="details">
	<div class="panel panel-success">
  		<div class="panel-heading">
   		 <h3 class="panel-title">Details <p class="pull-right"> ${user.last_name} ${user.first_name}</p></h3>
  		</div>
  		<div class="panel-body">
	<p>I am ur details</p></div>
  </div>
</div><!-- /.ends details -->


  <div class="tab-pane" id="favourites">
	<div class="panel panel-success">
  		<div class="panel-heading">
   		 <h3 class="panel-title">Favourites <p class="pull-right"> ${user.last_name} ${user.first_name}</p></h3>
  		</div>
  		<div class="panel-body">
<p>I am ur favourites</p></div>
	</div>
</div><!-- /.ends favourites -->

  <div class="tab-pane" id="messages">
	<div class="panel panel-success">
  		<div class="panel-heading">
   		 <h3 class="panel-title">Messages <p class="pull-right"> ${user.last_name} ${user.first_name}</p></h3>
  		</div>
  		<div class="panel-body">
<p>I will contain ur messages</p></div>
</div>
</div>

<div class="tab-pane" id="listings">
	<div class="panel panel-success">
  		<div class="panel-heading">
   		 <h3 class="panel-title">Listings <p class="pull-right"> ${user.last_name} ${user.first_name}</p></h3>
  		</div>
  		<div class="panel-body">
<p>I will contain ur listings</p></div>
</div>
</div>

</div><!-- /.ends tab content -->

	</div>
</div>
<%def name="breadcrumb()">
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li class="active">My profile</li>
</ol>
</%def>

<%inherit file="kaniru:templates/base/base.mako" />
<%namespace file="kaniru:templates/base/uiHelpers.mako" import="validate_errors"/>

<div class="row row-inset">
	<div class="col-xs-12 col-md-12 col-lg-12 col-sm-12">

<div class="panel panel-primary">
  <div class="panel-heading">
    <h2 class="panel-title""> <strong>Reset Your Password</strong></h2>
  </div>
  <div class="panel-body">
    
<p>Choose new password</p>
${form.begin(url=action_url,id="reset", method="post", class_="form-horizontal", role="form")}
${form.csrf_token()}
<div class="form-group">
          <input type="hidden" class="form-control" name="user_id"/>
	</div>
 <div class="form-group">
	${form.label("password *",class_="col-sm-2 col-md-2 col-lg-2 control-label")}
	<div class="col-sm-6 col-md-6 col-lg-6">
          ${form.password(name="password",  class_="form-control required")}
		${validate_errors("password")}
	</div>
</div>
<div class="form-group">
	${form.label("Confirm Password*",class_="col-sm-2 col-md-2 col-lg-2 control-label req")}
	<div class="col-sm-6 col-md-6 col-lg-6">
          ${form.password(name="password_confirm" , class_="form-control required")}
		${validate_errors("password_confirm")}
	</div>
</div>

 <div class="form-group">
	<div class="col-sm-offset-2 col-sm-6">
          <button type="submit" class="btn btn-success green" name="form_submitted">Send</button>
		</div>
</div>
        </form>

			
  </div>
</div>

</div><!--/.col-md-12-->
</div><!-- /.row -->

<%def name="breadcrumb()">
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li class="active">Forgot Password</li>
</ol>
</%def>
<%block name="script_tags">
${parent.script_tags()}
<script>
$("#reset").validate();
</script>
</%block>


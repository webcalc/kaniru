<%inherit file="kaniru:templates/base/base.mako"/>
<div class="row row-inset">
	<div class="col-sm-12 col-md-12">
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">About us</h3>
  	</div>
  	<div class="panel-body">
<h2>About Kaniru</h2>

Kaniru is passionate about property and technology. We work tirelessly to apply the latest technology to the property industry in an innovative way. 

We make it easy for buyers and prospective tenants to find properties for sale and to rent, and to provide enough property information to make a buying or renting decision.

As well as private listings, selected estate agents will also add their properties for sale and to rent to give buyers and tenants an even wider range of properties to search for and to give estate agents access to a powerful advertising platform. 

<h2>Kaniru's Vision and Mission</h2>

<p><strong>Vision:</strong> To Create Nigeria's digital property Market Place.</p>

<p><strong>Mission:</strong> Using quality content to help people find property.</p>

</div>
</div>
</div>
</div>
<%def name="breadcrumb()">
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li class="active">About Us</li>
</ol>
</%def>

<%doc>
	pager template
</%doc> 
<%def name="pager(items)"> 
	<div class="pager">
		<% link_attr={"style":"border: 1px solid green" } %>
		<% curpage_attr={"class": "btn btn-primary disabled"} %>
		<% dotdot_attr={"class": "btn disabled"} %>
		
		${items.pager(format="$link_previous ~2~ $link_next", 
				
					  link_attr=link_attr,
					  curpage_attr=curpage_attr,
				      dotdot_attr=dotdot_attr,
				      onclick="$('.list-partial').load('%s'); return false;")}
	</div>
</%def>

<%doc>
	checks validate errors, if any validate errors, adds message as span
</%doc> 
<%def name="validate_errors(field)">
	% for message in form.errors_for(field):
		% if (message is not None):
			<span class="text-danger">${message}</span>	
		% endif
	% endfor
</%def>



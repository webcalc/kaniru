<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%block name="header_tags">

<title><%block name="page_title">kaniru - ${title}</%block></title>

<link href="${request.static_url('kaniru:static/bootstrap/css/bootstrap.css')}" rel="stylesheet">

<link href="${request.static_url('kaniru:static/main.css')}" rel="stylesheet">

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</%block>
</head>
<body>
<% user=request.user
from pyramid.security import has_permission 
%>

<header>
	<nav id="usernav">
			<div class="container">
        <a href="${request.route_url('about')}">About Us</a>
%if not user:
	        <a href="${request.route_url('login')}">Register/Login</a>
%else:
	<a href="${request.route_url('logout')}">Logout</a>
	

%endif
	<a href="${request.route_url('favourites')}">My Favourites</a>
%if has_permission('admin', request.context,request):
	<a href="${request.route_url('dashboard')}">Dashboard</a>
	
%endif
%if user:
	<a href="${request.route_url("profile")}"><span class="glyphicon glyphicon-user"></span> My Account</a>
%endif

				</div>

    </nav> 
<div class="container">

	<div class="row" id="topnav">
	<div class="col-md-4" style="margin-top:10px;bottom:5px;">
<a href="${request.route_url('home')}"><img src="/static/logo.png" class="img-responsive pull-left" ></a>
		</div>
		<div class="col-md-5">
			
      
	
		
</div>
<div class="col-md-3" id="list-property" >
</div>
</div><!-- /.row -->
<div style="margin-right:-15px; margin-left:-15px;">

${self.flash_messages()}
</div>
</div><!-- /.container -->
</header> 
<div class="container">
<div class="row row-inset" >
	<div class="col-md-12" style="border-radius:0">
${self.breadcrumb()}
	</div>
</div>
<div class="row row-inset">
	<div class="col-md-12" id="listing-start" >
      <ul class="nav nav-pills nav-justified" >
        <li class="${self.step1()}"><a href="${request.route_url('step1')}" style="border-radius:0">Basic </a>
</li>
        <li class="${self.step2()}"><a href="${request.route_url('step2')}" style="border-radius:0">Details</a>
</li>
        <li class="${self.step3()}"><a href="${request.route_url('step3')}" style="border-radius:0">File Uploads </a>
</li>
	<li class="${self.step4()}"><a href="${request.route_url('step4')}" style="border-radius:0">Contact Setting</a> 
</li>
	<li class="${self.step5()}"><a href="${request.route_url('step5')}" style="border-radius:0">Review and Publish</a> 
</li>
      </ul>
    
<br>

	</div>
</div>

${next.body()}

<div class="row" id="nextToFt">
	<div class="col-md-1-5" id="social">
	<dl>
		<dt><a href=""><img src="/static/images/twitter.png" class="img-responsive"></a></dt>
		<dt><a href="https://www.facebook.com/kaniru.com.ng"><img src="/static/images/facebook.png" class="img-responsive"></a></dt>
		<dt><a href=""><img src="/static/images/google.png" class="img-responsive"></a></dt>
	</dl>
	</div>
	<div class="col-md-1-5">
<ul class="list-unstyled ftlist" >	
			<li><h5><strong>Properties</strong></h5></li>
			<li><a href="${request.route_url('for_sale')}">Property for sale</a></li>
			<li><a href="${request.route_url('for_rent')}">Property for rent</a></li>
			<li><a href="${request.route_url('dev')}">Development property</a></li>
			
		</ul>
	</div>
	<div class="col-md-1-5">
		<ul class="list-unstyled ftlist">	
			<li><h5><strong> More Properties</strong></h5></li>
			<li><a href="${request.route_url('browse_location',location_name=u'lagos')}">Lagos</a></li>
			<li><a href="${request.route_url('browse_location',location_name=u'Abuja')}">Abuja</a></li>
			<li><a href="${request.route_url('browse_location',location_name=u'Rivers')}">Rivers</a></li>
			
		</ul>
	</div>
	<div class="col-md-1-5 ftlist">
		<ul class="list-unstyled">	
			<li><h5><strong>About Kaniru</strong></h5></li>
			
			<li><a href="${request.route_url('about')}">About Us</a></li>
			
			<li><a href="${request.route_url('terms')}">Terms and Conditions</a></li>
			
		</ul>
	</div>
	<div class="col-md-1-5" id="subscribeft">
		<h4 style="color:#fff">Subscribe to newsletter <span class="pull-right"><img src="/static/subscribelogo.png"/></span></h4>
	<p style="font-size:10px;">Subscribe to our newsletter to get access to our news and<br> resources. Your email will be safe.Unsubscribe at any time</p>
		 <form class="form-inline" action="${request.route_url('subscribemail')}" role="form" id="homepage_form">

			<div class="input-group">
      			<input name="email" id="email" type="email" class="form-control" data-msg-required=" " placeholder="Your Email">

      			<span class="input-group-btn">
        	<button class="btn btn-success green" type="submit"><strong>Sign up</strong></button>
      </span>
    		</div>
			
		</form>
	</div>

</div>
<footer id="ft">
	<div class="row" >
		<div class="col-xs-12 col-md-12 text-center">
		<p>&copy; 2014 kaniru Communication. All rights reserved. Material may not be published or reproduced in any form without prior written permission.</p><br>
		</div>
</div>
</footer>
</div>
<%block name="script_tags">
<script src="${request.static_url('kaniru:static/jquery.min.js')}"></script>
<script src="${request.static_url('kaniru:static/bootstrap/js/bootstrap.min.js')}"></script>
<script src="${request.static_url('kaniru:static/jquery-validation-1.12.0/dist/jquery.validate.min.js')}"></script>

</%block>

</body>
</html>



<%def name="breadcrumb()"></%def>

<%def name="flash_messages()">
		% if request.session.peek_flash():
	
		<% flash = request.session.pop_flash() %>
		% for message in flash:
		
<div class="alert alert-${message.split(';')[0]} alert-dismissable col-md-12" style="border-radius:0">
	 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			
				${message.split(";")[1]}<br>
			
		</div>
	
		% endfor
	
	% endif
</%def>

<%def name="step1()" />
<%def name="step2()" />
<%def name="step3()" />
<%def name="step4()" />
<%def name="step5()" />


<%inherit file="kaniru:templates/base/base.mako"/>
<div class="row row-inset">
	<div class="col-sm-12 col-md-12">
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Contact us</h3>
  	</div>
  	<div class="panel-body">
<address>
Kaniru Real Estate Ltd.<br>
(Alliance Complex) 46/47 Limca Road,<br>
Onitsha<br>

<span class="glyphicon glyphicon-phone-alt"></span>:07087071959
</address>
	</div>
</div>
</div>
</div>
<%def name="breadcrumb()">
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li class="active">Contact</li>
</ol>
</%def>
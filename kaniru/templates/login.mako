<%inherit file="kaniru:templates/base/base.mako" />
<%namespace file="kaniru:templates/base/uiHelpers.mako" import="validate_errors"/>

<div class="row row-inset">
	<div class="col-xs-12 col-md-12 col-lg-12 col-sm-12">

<div class="panel panel-primary">
  <div class="panel-heading">
    <h1 class="panel-title""> <strong>Sign In</strong></h1>
  </div>
  <div class="panel-body">
    <div class="row">
		<div class="col-md-12 col-sm-12">
<p style="color:red">${message}</p>
${form.begin(url=url,method="POST",id="login", class_="form-horizontal",role="form")}
${form.csrf_token()}
<div class="form-group">
${form.hidden("came_from",value=came_from, class_="form_control")}

	</div>
 <div class="form-group ${error_cls}">
${form.label("Email*", class_="col-sm-2 col-md-2 col-lg-2 control-label")}
	<div class="col-sm-6 col-md-6 col-lg-6">
          ${form.text("email", class_="form-control")}
		${validate_errors('email')}
		</div>
</div>
<div class="RadioGroup">
	<div class="form-group">
	<div class="col-sm-offset-2 col-md-offset-2 col-md-6 col-sm-6 radio">
	<label>
    <input type="radio" name="user" value="new-user">
    	I am new to Kaniru
  </label>
	</div>
	
	<div class="col-sm-offset-2 col-md-offset-2 col-md-6 col-sm-6 radio">
	<label>
    <input type="radio" name="user" value="old-user" checked="checked">
    I already have an account here
  </label>
	</div>
	</div>
	<div id="old-user" class="desc">
 <div class="form-group ${error_cls}">
${form.label("Password*", class_="col-sm-2 col-md-2 col-lg-2 control-label")}
	<div class="col-sm-6 col-md-6 col-lg-6">

          ${form.password("password", class_="form-control")}
		${validate_errors('password')}

		</div>
</div>
 <div class="form-group">
	<div class="col-sm-offset-2 col-md-offset-2 col-md-6 col-sm-6">
          <button type="submit" class="btn btn-success green" name="form_submitted" style="border-radius:0px;">Sign In</button>
		</div>
</div>
</div>
<div id="new-user" class="desc" style="display:none">
	<div class="form-group">
	<div class="col-sm-offset-2 col-md-offset-2 col-md-6 col-sm-6">
          <button type="submit" name="continue" class="btn btn-warning btn-block" style="border-radius:0px;">Continue</button>
		</div>
</div>
</div>
</div>
        </form>

<a  data-toggle="modal" href="#MyModal">Forgot password ?</a>


<div class="modal fade" id="MyModal" tabindex="-1" role="dialog" aria-labelledby="MyModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="MyModalLabel"><strong>Forgot Password</strong></h4>
      </div>
      <div class="modal-body">
<p>Please Enter the email you registered with so we can send you a password reset link</p>
${form.begin(url=request.route_url('forgot_password'), id="forgot",method="post", class_="form-horizontal", role="form")}
${form.csrf_token()}
 <div class="form-group">
	${form.label("Email", class_="control-label")}
          ${form.text(name="email",  class_="form-control required")}
		${validate_errors("email")}
</div>
 <div class="form-group">
          <button type="submit" class="btn btn-success green" name="form_submitted">Send</button>
		</div>
        </form>
	
</div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>


			</div><!-- /.col-md-12 inside panel -->
			

	</div><!-- /.row-->
  </div>
</div>

</div><!--/.col-md-12-->

</div><!-- /.row -->

<%def name="breadcrumb()">
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li class="active">Sign in</li>
</ol>
</%def>

<%block name="script_tags">
${parent.script_tags()}
<script>
$("#forgot").validate();
</script>
</%block>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<%block name="header_tags">

<title><%block name="page_title">${title}</%block></title>

<link href="${request.static_url('kaniru:static/bootstrap/css/bootstrap.css')}" rel="stylesheet">

<link href="${request.static_url('kaniru:static/main.css')}" rel="stylesheet">

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</%block>
</head>
<body>
<header>
	<nav id="usernav">
			<div class="container">
        <a href="">About Us</a>
%if not request.user:
	        <a href="${request.route_url('login')}">Register/Login</a>
%else:
	<a href="${request.route_url('logout')}">Logout</a>
	

%endif
	<a href="">My Favourites</a>
%if h.has_permission(request.user,request,'admin','editor'):
	<a href="${request.route_url('dashboard')}">Dashboard</a>
%endif
%if request.user:
	<a href="${request.route_url("profile")}"><span class="glyphicon glyphicon-user"></span> My Account</a>
%endif
				</div>

    </nav> 
<div class="container">
<div class="row row-inset">
	<div class="col-sm-12 col-md-12">
	<div style="margin-right:-15px; margin-left:-15px;">

${self.flash_messages()}
</div>

${next.body()}
	
	</div>
</div>
<footer class="text-center" style="height:50px;margin-top:10px;">
&copy; 2014
</footer>
</div>
<div id="draggable">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">Toolbox</h3>
		<a href="/"><img src="/static/logo.png" class="img-responsive pull-right" style="padding:6px;"></a>
	</div>
<div class="panel-body">
<ul class="list-unstyled">
  <li><a href="${request.route_url('send_news')}"> NewsLetters</a></li>
<li class="active"><a href="${request.route_url('ad_list')}">Adverts</a></li>
<li><a href="${request.route_url('list_users')}">Users</a></li>
<li><a href="${request.route_url('list_state')}">Locations</a></li>
<li><a href="${request.route_url('list_categories')}"> Categories</a></li>
##<li><a href="${request.route_url('list_featuretypes')}"> ##Features</a></li>
<li><a href="${request.route_url('admin_search_listing')}"> Listings</a></li>

</ul>
</div>
</div>
</div>

<%block name="script_tags">
<script src="${request.static_url('kaniru:static/jquery.min.js')}"></script>
<script src="${request.static_url('kaniru:static/bootstrap/js/bootstrap.min.js')}"></script>
<script src="${request.static_url('kaniru:static/jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js')}"></script>
<script src="${request.static_url('kaniru:static/jquery-validation-1.12.0/dist/jquery.validate.min.js')}"></script>
<script>
	$(function() {
		$( "#draggable" ).draggable();
	});
	</script>
</%block>
</body>
</html>
<%def name="flash_messages()">
		% if request.session.peek_flash():
	
		<% flash = request.session.pop_flash() %>
		% for message in flash:
		
<div class="alert alert-${message.split(';')[0]} alert-dismissable col-md-12" style="border-radius:0">
	 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			
				${message.split(";")[1]}<br>
			
		</div>
	
		% endfor
	
	% endif
</%def>


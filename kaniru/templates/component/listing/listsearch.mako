<%inherit file="kaniru:templates/component/layout.mako" />
	<h3>Search Listings</h3>
<form method="GET" action="${request.route_url('admin_search_listing')}">
				<input name="search" type="text" value="" placeholder="Enter Listing ID to search" class="form-control">
			</form>
<br>
%if listing:
	<img src="${request.storage.url(listing.photos[0].filename)}" width="120" height="100" class="img-responsive pull-left thumbnail" >
	<p>${listing.serial}</p>
	<p>${listing.users.fullname}</p>
<a class="btn btn-warning pull-right" href="${request.route_url('pushfront',listing_id=listing.id)}" style="border-radius:0px;">Push Front</a>

<a class="btn btn-danger pull-right" href="${request.route_url('delete_listing',listing_id=listing.id)}" style="border-radius:0px;">Delete</a>
%endif

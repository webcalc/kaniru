<%inherit file="kaniru:templates/component/layout.mako" />

	<h3><strong>State:</strong> ${state.name}</h3>
	<hr>
	<h4>Cities:</h4>
	%if paginator:
<p class="pull-right">${paginator.pager(
'$link_previous ~3~ $link_next',
	link_attr={"class":"btn btn-sm btn-primary"},
	curpage_attr={"class":"btn btn-sm btn-default disabled"},
	symbol_next="Next",
	symbol_previous="Previous",
	show_if_single_page=True,
)}
</p>

		<table class="table table-striped table-bordered table-condensed">
	<thead>
	<tr>
		<th>City Name</th>
		<th>Actions</th>
	</tr>
	<tbody>
		%for c in paginator.items:
	<tr><td>${c.name}</td>
		<td><a class="btn btn-success" href="${request.route_url('edit_city',city_id=c.id,state_id=state.id)}" style="border-radius:0">Edit</a>
<a class="btn btn-success" href="${request.route_url('view_city',city_id=c.id)}" style="border-radius:0">View Areas</a>
		<a class="btn btn-danger" href="${request.route_url('delete_city',city_id=c.id, state_id=state.id)}" style="border-radius:0">Delete</a>
		</td>
	</tr>
		%endfor

	<tbody>
</table>

%endif
<a class="btn btn-primary pull-left" href="${request.route_url('list_state')}" style="border-radius:0px;">Go back to state list</a>

<a class="btn btn-primary pull-right" href="${request.route_url('add_city',state_id=state.id)}" style="border-radius:0px;">Add city</a>



<%inherit file="kaniru:templates/component/layout.mako" />
	<h3>State list <small> total is ${total}</small></h3>
	

	<hr>
%if paginator:
<p class="pull-right">${paginator.pager(
'$link_previous ~3~ $link_next',
	link_attr={"class":"btn btn-sm btn-primary"},
	curpage_attr={"class":"btn btn-sm btn-default disabled"},
	symbol_next="Next",
	symbol_previous="Previous",
	show_if_single_page=True,
)}
</p>

<table class="table table-striped table-bordered table-condensed">
	<thead>
	<tr>
		<th>State Name</th>
		<th>Actions</th>
	</tr>
	<tbody>

	%for state in paginator.items:
	<tr>
<td><a href="${request.route_url('view_state',state_id=state.id)}" >${state.name}</a></td>
<td><a class="btn btn-success" href="${request.route_url('edit_state',state_id=state.id)}" style="border-radius:0">Edit</a>
##<a class="btn btn-primary" href="${request.route_url('view_state', ##state_id=state.id)}" style="border-radius:0">View Cities</a>
##<a class="btn btn-danger" ##href="${request.route_url('delete_state',state_id=state.id)}" ##style="border-radius:0">Delete</a></td>
</tr>
	%endfor
	</tbody>
</table>
%endif
<a class="btn btn-primary pull-left" href="${request.route_url('dashboard')}" style="border-radius:0px;">Go Back to dashboard</a>

<a class="btn btn-primary pull-right" href="${request.route_url('add_state')}" style="border-radius:0">Add more States</a>
	

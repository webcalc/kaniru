<%inherit file="kaniru:templates/component/layout.mako" />

<%namespace file="kaniru:templates/base/uiHelpers.mako" import="validate_errors"/>
<h3>Add  Areas to ${city.name}</h3>
<hr>
${form.begin(url=action_url, method="post")}

${form.csrf_token()}

<div class="form-group">
	${form.label("Name")}
		${form.text("name", class_="form-control")}
		<p class="help-block">Please enter a comma separated list of areas to add e.g awada, nkpor etc.</p>
		${validate_errors("name")}
</div>
<input type="submit" name="form_submitted" value="Save" class="btn btn-success pull-left">
			<input type="reset" name="form_reset" value="Cancel" class="btn btn-default pull-right" 
				onclick="location.href='${request.route_url('view_city', city_id=city.id)}'">
	    
	${form.end()}


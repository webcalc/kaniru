<%inherit file="kaniru:templates/component/layout.mako" />

	<h3><strong>City:</strong> ${city.name}</h3>
	<hr>
	<h4>Areas:</h4>
	%if city.area:
		<table class="table table-striped table-bordered table-condensed">
	<thead>
	<tr>
		<th>City Name</th>
		<th>Actions</th>
	</tr>
	<tbody>

		%for c in city.area:
	<tr><td>${c.name}</td>
		<td><a class="btn btn-success" href="${request.route_url('edit_area',area_id=c.id,city_id=city.id)}" style="border-radius:0">Edit</a>
		<a class="btn btn-danger" href="${request.route_url('delete_area',area_id=c.id, city_id=city.id)}" style="border-radius:0">Delete</a>
		</td>
	</tr>
		%endfor
	<tbody>
</table>
%endif
<a class="btn btn-primary pull-left" href="${request.route_url('view_state',state_id = city.state.id)}" style="border-radius:0px;">Go back to ${city.state.name}</a>

<a class="btn btn-primary pull-right" href="${request.route_url('add_area',city_id=city.id)}" style="border-radius:0px;">Add area</a>


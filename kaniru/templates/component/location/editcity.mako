<%inherit file="kaniru:templates/component/layout.mako" />
<%namespace file="kaniru:templates/base/uiHelpers.mako" import="validate_errors"/>
<h3>Edit City</h3>
<hr>
${form.begin(url=action_url, method="post")}

${form.csrf_token()}

<div class="form-group">
	${form.label("Name")}
		${form.text("name",value=city.name, class_="form-control")}
		
		${validate_errors("name")}
</div>
<input type="submit" name="form_submitted" value="Save" class="btn btn-success pull-left">
			<input type="reset" name="form_reset" value="Cancel" class="btn btn-default pull-right" 
				onclick="location.href='${request.route_url('view_state',state_id=state.id)}'">
	    
	${form.end()}


<%inherit file="kaniru:templates/component/layout.mako" />

	<h3>${category.name}</h3>
	<hr>
	<h4>Sub-categories:</h4>
	%if category.subcategory:
		<table class="table table-striped table-bordered table-condensed">
	<thead>
	<tr>
		<th>SubCategory Name</th>
		<th>Actions</th>
	</tr>
	<tbody>

		%for sub in category.subcategory:
	<tr><td>${sub.name}</td>
		<td><a class="btn btn-success" href="${request.route_url('edit_subcategory',sub_id=sub.id,category_id=category.id)}">Edit</a>
		<a class="btn btn-danger" href="${request.route_url('delete_subcategory',sub_id=sub.id, category_id=category.id)}">Delete</a>
		</td>
	</tr>
		%endfor
	<tbody>
</table>
%endif
<a class="btn btn-primary pull-left" href="${request.route_url('list_categories')}" style="border-radius:0px;">Go back to category list</a>

<a class="btn btn-primary pull-right" href="${request.route_url('add_subcategory',category_id=category.id)}" style="border-radius:0px;">Add subcategories</a>


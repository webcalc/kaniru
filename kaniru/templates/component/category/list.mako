<%inherit file="kaniru:templates/component/layout.mako" />
	<h3>Category list</h3>
	

	<hr>
<table class="table table-striped table-bordered table-condensed">
	<thead>
	<tr>
		<th>Category Name</th>
		<th>Actions</th>
	</tr>
	<tbody>
	%for c in category:
	<tr>
<td><a href="${request.route_url('view_category',category_id=c.id)}" >${c.name}</a></td>
<td><a class="btn btn-success" href="${request.route_url('edit_category',category_id=c.id)}" style="border-radius:0">Edit</a>
<a class="btn btn-primary" href="${request.route_url('view_category', category_id=c.id)}" style="border-radius:0">View Sub Categories</a>
##<a class="btn btn-danger" ##href="${request.route_url('delete_category',category_id=c.id)}" ##style="border-radius:0">Delete</a></td>
</tr>
	%endfor
	</tbody>
</table>
<a class="btn btn-primary pull-left" href="${request.route_url('dashboard')}" style="border-radius:0px;">Go Back to dashboard</a>

##<a class="btn btn-primary pull-right" ##href="${request.route_url('add_category')}"
## style="border-radius:0">Add more Categories</a>

<%inherit file="kaniru:templates/component/layout.mako" />
	<h4>Users</h4>
<form method="GET" action="${request.route_url('list_users')}">
				<input name="search" type="text" value="" placeholder="Enter names to search" class="form-control">
			</form>
<br>
%if paginator:
<table class="table table-striped">
	<thead>
	<th>Fullname</th>
	<th>Email</th>
	<th>Groups</th>
	<th>Joined Date</th>
	</thead>
<tbody>
	%for item in paginator.items:
	<tr>
		<td>${item.fullname}</td>
		<td>${item.email}</td>
		<td>${[g.groupname for g in item.mygroups]}</td>
		<td>${item.created.date()}</td>
	</tr>
	%endfor
</tbody>
</table>
%endif
	
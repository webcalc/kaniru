<%inherit file="kaniru:templates/component/layout.mako" />
	<h3>Feature Type List</h3>
	<hr>
<table class="table table-striped table-bordered table-condensed">
	<thead>
	<tr>
		<th>Feature Type Name</th>
		<th>Actions</th>
	</tr>
	<tbody>
	%for f in feature_type:
	<tr>
<td><a href="${request.route_url('view_featuretypes',featuretype_id=f.id)}" >${f.name}</a></td>
<td><a class="btn btn-success" href="${request.route_url('edit_featuretype',featuretype_id=f.id)}" style="border-radius:0">Edit</a>
<a class="btn btn-primary" href="${request.route_url('view_featuretypes', featuretype_id=f.id)}" style="border-radius:0">View Features</a>
<a class="btn btn-danger" href="${request.route_url('delete_featuretypes',featuretype_id=f.id)}" style="border-radius:0">Delete</a></td>
</tr>
	%endfor
	</tbody>
</table>
<a class="btn btn-primary pull-left" href="${request.route_url('dashboard')}" style="border-radius:0px;">Go Back to dashboard</a>

<a class="btn btn-primary pull-right" href="${request.route_url('feature_type')}" style="border-radius:0">Add More Feature type</a>
	

<%inherit file="kaniru:templates/component/layout.mako" />

<%namespace file="kaniru:templates/base/uiHelpers.mako" import="validate_errors"/>
<h3>Add Feature Types</h3>
<hr>
${form.begin(url=action_url, method="post")}

${form.csrf_token()}

<div class="form-group">
	${form.label("Name")}
		${form.text("name", class_="form-control")}
		<p class="help-block">Please enter a comma separated list of feature types to add e.g Outdoor, Indoor etc.</p>
		${validate_errors("name")}
</div>
<input type="submit" name="form_submitted" value="Save" class="btn btn-success pull-left">
			<input type="reset" name="form_reset" value="Cancel" class="btn btn-default pull-right" 
				onclick="location.href='${request.route_url('list_featuretypes')}'">
	    
	${form.end()}


<%inherit file="kaniru:templates/component/layout.mako" />

	<h3>${feature_type.name}</h3>
	<hr>
	<h4>Features:</h4>
	%if feature_type.features:
		<table class="table table-striped table-bordered table-condensed">
	<thead>
	<tr>
		<th>Feature Name</th>
		<th>Actions</th>
	</tr>
	<tbody>

		%for f in feature_type.features:
	<tr><td>${f.name}</td>
		<td><a class="btn btn-success" href="${request.route_url('edit_feature',feature_id=f.id,featuretype_id=feature_type.id)}" style="border-radius:0px;">Edit</a>
		<a class="btn btn-danger" href="${request.route_url('delete_feature',feature_id=f.id, featuretype_id=feature_type.id)}" style="border-radius:0px;">Delete</a>
		</td>
	</tr>
		%endfor
	<tbody>
</table>
%endif
<a class="btn btn-primary pull-left" href="${request.route_url('list_featuretypes')}" style="border-radius:0px;">Go back to feature type list</a>

<a class="btn btn-primary pull-right" href="${request.route_url('add_features',featuretype_id=feature_type.id)}" style="border-radius:0px;">Add More Features</a>


<%inherit file="kaniru:templates/component/layout.mako" />
	<h3>Ad list</h3>
	
%if ads:
<table class="table table-striped">
	<thead>
		<th>Image</th>
		<th>Advert Type</th>
		<th>Actions</th>
	<thead>
	<tbody>
	
%for ad in ads:
	<tr>
	<td>
%if ad.filename:

<img src="${request.storage.url(ad.filename)}" height="200" width="200" class="img-responsive pull-left">
%else:
${ad.url|n}
%endif
	</td>
	<td>
	%if ad.filename:
		uploaded advert
	%else:
		Google code. If the ad doesn't show here, that means your adsense account hasn't yet been approved
	%endif
	</td>
	<td>
	<a href="${request.route_url('front_ad', ad_id=ad.id)}" class="btn btn-primary" style="border-radius:0">Make Front Ad</a>
	<a href="${request.route_url('first_on_second', ad_id=ad.id)}" class="btn btn-success" style="border-radius:0">Make First</a>
	<a href="${request.route_url('second_on_second', ad_id=ad.id)}" class="btn btn-warning" style="border-radius:0">Make Second</a>
	<a href="${request.route_url('third_on_second', ad_id=ad.id)}" class="btn btn-info" style="border-radius:0">Make third</a>
<a href="${request.route_url('delete_ad', ad_id=ad.id)}" class="btn btn-danger" style="border-radius:0">Delete</a>

		</td>
	</tr>
%endfor
	</tbody>
</table><br>
%endif
<a href="${request.route_url('create_ads')}", class="btn btn-primary">Create Ad</a>


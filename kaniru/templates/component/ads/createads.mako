<%inherit file="kaniru:templates/component/layout.mako" />
<%block name="script_tags">
	${parent.script_tags()}
	<script type="text/javascript">
	
	$("#adform").validate({
	rules: {
    		link: {
      	required: true,
      	url: true
    }
  }

});
	$("#adgoogle").validate();
	</script>
</%block>
	<h3> Create Ad</h3>
	
${form.begin(url=request.route_url('post_ad'),multipart=True,id="adform",method="POST",class_="form-horizontal")}
${form.csrf_token()}
<fieldset><legend>Adverts upload</legend>
	${form.label('Upload')}
	${form.file(name='pics',class_="required",accept="image/*")}<br>
	<br>
	${form.label('Link', class_="control-label")}
	${form.text('link', class_="form-control")}
<span class="help-block">This field accepts a valid url e.g http://www.kaniru.com.ng, type it in full as in the example</span>

	<button type="submit" name="form_submitted" class="btn btn-primary">Submit Ad</button>
</fieldset>
${form.end()}
${form.begin(url=action_url,id="adgoogle",method="POST",class_="form-horizontal")}
${form.csrf_token()}

<fieldset><legend>Google Ads Upload</legend>
	${form.label("Google code")}
	${form.textarea('url',rows="5", class_="form-control required")}
	<span class="help-block">Find in the google code the width and height of ad and set it to width=293, height=251</span>
	<button type="submit" name="url_submitted" class="btn btn-primary">Submit Code</button>

</fieldset>
${form.end()}

	
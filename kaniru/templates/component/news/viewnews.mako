<%inherit file="kaniru:templates/component/layout.mako" />
<%!
    from webhelpers.html import literal
%>

	<h3>${news.subject}</h3>
	<hr>
	<h4>Content</h4>
${literal(news.body)}
	<h4>Sent to </h4>
	<p>${news.sent_to}</p>
<a class="btn btn-primary pull-left" href="${request.route_url('newslist')}" style="border-radius:0px;">Go back to newslist</a>

<a class="btn btn-primary pull-right" href="${request.route_url('send_news')}" style="border-radius:0px;">Compose News</a>


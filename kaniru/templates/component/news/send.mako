<%inherit file="kaniru:templates/component/layout.mako" />

<%namespace file="kaniru:templates/base/uiHelpers.mako" import="validate_errors"/>
<h3>SEND NEWSLETTER</h3>
<hr>
${form.begin(url=action_url, method="post", id="sendnews")}

${form.csrf_token()}

<div class="form-group">
	${form.label("subject")}
		${form.text("subject", class_="form-control required",)}
		
		${validate_errors("subject")}
</div>
<div class="form-group">
	${form.label("Body")}
		${form.textarea("body",class_="form-control required", rows="10")}
		${validate_errors('body')}
</div>
<div class="form-group">
	<input type="reset" name="form_reset" value="Cancel" class="btn btn-default pull-left" 
				onclick="location.href='${request.route_url('dashboard')}'">
</div>
	<div class="form-group">
<input type="submit" name="form_submitted" value="Send" class="btn btn-success pull-right">
			</div>
	    
	${form.end()}

<%block name="script_tags">
${parent.script_tags()}
	<script src="${request.static_url('kaniru:static/tinymce/js/tinymce/tinymce.min.js')}"></script>
<script>
	tinymce.init({
selector:'textarea',
theme : 'modern',
plugins: [
      "advlist autolink lists link image charmap print preview anchor",
      "searchreplace visualblocks code fullscreen",
      "insertdatetime media table contextmenu paste"
  ],
browser_spellcheck : true,
content_css :"http://127.0.0.1:6543/static/bootstrap/css/bootstrap.css,http://127.0.0.1:6543/static/main.css"

});
$("#sendnews").validate();
</script>
</%block>

<%inherit file="kaniru:templates/component/layout.mako" />
	<h3>Sent newsletters</h3>
	<hr>
%if paginator:
<table class="table table-striped table-bordered table-condensed">
	<thead>
	<tr>
		<th>Subject</th>
		<th>Sent Date</th>
		<th>Actions</th>
	</tr>
	<tbody>
	%for news in paginator.items:
	<tr>
<td><a href="${request.route_url('view_newsletter',news_id=news.id)}" >${news.subject}</a></td>
<td>${news.created}</td>
<td>
<a class="btn btn-danger" href="${request.route_url('delete_news',news_id=news.id)}" style="border-radius:0">Delete</a></td>
</tr>
	%endfor
	</tbody>
</table>
%else:
	<p class="text-center"><i>No sent newsletter</i></p>
%endif
<a class="btn btn-primary pull-left" href="${request.route_url('dashboard')}" style="border-radius:0px;">Go Back to dashboard</a>

<a class="btn btn-primary pull-right" href="${request.route_url('send_news')}" style="border-radius:0">Compose Newsletter</a>
	
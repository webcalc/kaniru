from pyramid.config import Configurator
from sqlalchemy import engine_from_config
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from kaniru.utils.subscribers import add_renderer_globals, csrf_validation

from pyramid.events import BeforeRender, NewRequest

from pyramid_beaker import (session_factory_from_settings,
                            set_cache_regions_from_settings)
from kaniru.models import Base,DBSession,RootFactory, groupfinder,get_user

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    session_factory = session_factory_from_settings(settings)
    set_cache_regions_from_settings(settings)
    authn_policy = AuthTktAuthenticationPolicy('*&sghw!yuowant',
                                               callback=groupfinder)
    authz_policy = ACLAuthorizationPolicy()
    
    config = Configurator(settings=settings,
                          authentication_policy=authn_policy,
                          authorization_policy=authz_policy,
                          session_factory=session_factory)
    cache = RootFactory.__acl__
    config.set_root_factory(RootFactory)
    config.add_request_method(get_user, 'user', reify=True)
    config.include('pyramid_mailer')
    config.add_subscriber(add_renderer_globals, BeforeRender)
    config.add_subscriber(csrf_validation, NewRequest)
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_static_view('images','kaniru:images')
    config.add_route('home','/')
    config.add_route('subscribemail', '/subscribe')
    config.add_route('terms','/terms')
    config.add_route('contact','/contact')
    config.add_route('about','/about')
    #add.py
    config.add_route('get_categories','/listings/category/getcategories')
    config.add_route('step1','/create/start.html')
    config.add_route("serve",'/public/{id}/')
    config.add_route('get_cities','/listings/city/getcities')
    config.add_route('get_areas','/listings/area/getareas')
    
    config.add_route('get_subcategories','/listings/category/getsubcategories')
    config.add_route('step2','/create/details.html')
    config.add_route('step3','/create/uploads.html')
    config.add_route('upload','/process/uploadpics')
    config.add_route('delete_upload','/upload{filename}/delete')
    config.add_route('step4','/contact/setting.html')
    config.add_route('step5','/create/publish.html')
    config.add_route('create_listing','/create/listing.html')
    
    #dashboard.py
    config.add_route('dashboard','/dashboard')
    config.add_route('newslist','/dashboard/allnews.html')
    config.add_route('send_news','/dashboard/sendnews.html')
    config.add_route('view_newsletter','/dashboard/{news_id}/newsletter.html')
    config.add_route('delete_news','/dashboard/{news_id}/deletenews.html')
    config.add_route('ad_list','/dashboard/listads.html')
    config.add_route('create_ads','/dashboard/create/ads.html')
    config.add_route('post_ad','/dashboard/post/ads.html')
    config.add_route('front_ad','/dashboard/front/{ad_id}/advert.html')
    config.add_route('first_on_second','/dashboard/{ad_id}/firstonsecond/advert.html')
    config.add_route('second_on_second','/dashboard/{ad_id}/secondonsecond/advert.html')
    config.add_route('third_on_second','/dashboard/{ad_id}/thirdonsecond/advert.html')
    config.add_route('ad_on_viewpage','/dashboard/{ad_id}/viewpagead/advert.html')
    config.add_route('delete_ad','/dashboard/{ad_id}/deletead.html')
    config.add_route('admin_search_listing','/dashboard/searchlisting.html')
    config.add_route('list_users','/dashboard/users/all.html')
    config.add_route('add_to_groups','/dashboard/{user_id}/users/togroup.html')
    
    
    #city
    config.add_route('add_city','/dashboard/city/{state_id}/add.html')
    config.add_route('view_city','/dashboard/{city_id}/city/view.html')
    config.add_route('delete_city','/dashboard/{city_id}/{state_id}/city/delete.html')
    config.add_route('edit_city','/dashboard/{city_id}/{state_id}/city/edit.html')
    
    #area
    config.add_route('add_area','/dashboard/{city_id}/area/add.html')
    config.add_route('edit_area','/dashboard/{area_id}/{city_id}/area/edit.html')
    config.add_route('delete_area','/dashboard/{area_id}/{city_id}/area/delete.html')
    #state
    config.add_route('view_state','/dashboard/{state_id}/state/view.html')
    config.add_route('add_state','/dashboard/state/add.html')
    config.add_route('edit_state','/dashboard/{state_id}/state/edit.html')
    config.add_route('delete_state','/dashboard/{state_id}/state/delete.html')
    config.add_route('list_state','/dashboard/state/list.html')
    
    #category
    config.add_route('add_category','/dashboard/category/add.html')
    config.add_route('view_category','/dashboard/{category_id}/category/view.html')
    config.add_route('list_categories','/dashboard/category/list.html')
    config.add_route('delete_category','/dashboard/{category_id}/category/delete.html')
    config.add_route('edit_category','/dashboard/{category_id}/category/edit.html')
    
    #subcategory
    config.add_route('add_subcategory','/dashboard/{category_id}/subcategory/add.html')
    config.add_route('delete_subcategory','/dashboard/{sub_id}/{category_id}/subcategory/delete.html')
    config.add_route('edit_subcategory','/dashboard/{sub_id}/{category_id}/subcategory/edit.html')
    
    #ftype
    config.add_route('feature_type','/dashboard/ftype/add.html')
    config.add_route('edit_featuretype','/dashboard/{featuretype_id}/ftype/edit.html')
    config.add_route('delete_featuretypes','/dashboard/ftype/{featuretype_id}/delete.html')
    config.add_route('list_featuretypes','/dashboard/ftype/list.html')
    config.add_route('view_featuretypes','/dashboard/ftype/{featuretype_id}/view.html')
    config.add_route('add_features','/dashboard/{featuretype_id}/features/add.html')
    config.add_route('edit_feature','/dashboard/{featuretype_id}/features/{feature_id}/edit.html')
    config.add_route('delete_feature','/dashboard/{featuretype_id}/features/{feature_id}/delete.html')
    
    #prop
    config.add_route('delete_listing','/listing/{listing_id}/delete.html')
    config.add_route('pushfront','/listing/{listing_id}/pushfront')
    config.add_route('prop_search','/search_properties')
    config.add_route('property_view','/{slug}-{listing_id:\d+}.html')
    config.add_route('browse_property','/all')
    config.add_route('browse_location','/listings/{location_name}/view/all')
    config.add_route('browse_city','/listings/{location_name}/city.html')
    config.add_route('browse_area','/listings/{location_name}/area.html')
    config.add_route('browse_category','/listings/{category_name}/category.html')
    config.add_route('add_to_favourites','/listing/{listing_id}/makefavourites')
    config.add_route('for_sale','/listings/forsale/all.html')
    config.add_route('for_rent','/listings/forrent/all.html')
    config.add_route('dev','/listings/development/all.html')
    config.add_route('emailagent','/agent/sendsmail')
    config.add_route('view_agent','/view{user_id:\d}/{slug}')
    config.add_route('registration','/register')
    config.add_route('change_password','/change/password')
    config.add_route('forgot_password','/passforgot')
    config.add_route('reset_password','/reset/:user_id/:hmac')
    config.add_route('logout','/logout')
    config.add_route('login','/login')
    
    config.add_route('profile','/user/profile')
    config.add_route('details','/user/details')
    config.add_route('userupdate','/user/update')
    config.add_route('email_alert','/user/email/alerts')
    config.add_route('messages','user/messages')
    config.add_route('favourites','/user/favourites')
    config.add_route('user_listings','/user/my_listings')
    config.add_route('agupload','/user/logoupload')
    
   
    
    config.scan()
    return config.make_wsgi_app()


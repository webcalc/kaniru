$(document).ready(function(){

	$hasValue=$('#category_id option:selected').val();
	if($hasValue.length==0){
		$('#subcategory').hide();
		}
	else {
		loadSubcategories($hasValue);
		$('#subcategory').show('fast');
		}
    $('#category_id').on('change',function(){
		$selected=$('#category_id option:selected').val();
		loadSubcategories($selected);
		if($selected.length==0){
			$('#subcategory').hide('slow');
		}
		else {
		$('#subcategory').show('fast');
		}
	});

	
});

var loadSubcategories = function(value){
	$.ajax( {
			url:"/listings/category/getsubcategories", 
			data:JSON.stringify({id:value}),
			dataType:"json",
			contentType: 'application/json; charset=utf-8',
			success:function( resp ) {
				 $('#subcategory_id').html('');
				 $('<option value=""'+'>' + "Choose category" + '</option>').appendTo('#subcategory_id');
				 //log each key in the response data
				$.map( resp, function( item, index ) {
					var option =$('<option value=' + item.value + '>' + item.label + '</option>');
				$('#subcategory_id').append(option);
				});
			},
		});
}


$(function() {
    $('a').tooltip();
});	
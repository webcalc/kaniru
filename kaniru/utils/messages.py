from pyramid_mailer import get_mailer
from pyramid_mailer.message import Message

from pyramid.url import route_url


class EmailMessageText(object):
    """ Default email message text class
    """

    def forgot(self):
        """
In the message body, %_url_% is replaced with:

::

    route_url('apex_reset', request, user_id=user_id, hmac=hmac))
        """
        return {
                'subject': 'Password reset request received',
                'body': """
A request to reset your password has been received. Please go to
the following URL to change your password:

%_url_%

If you did not make this request, you can safely ignore it.
"""
        }
    

def kaniru_email(request, recipients, subject, body, sender=None):
    """ Sends email message
    """
    mailer = get_mailer(request)
    if not sender:
        sender = 'info@kaniru.com.ng'
    message = Message(subject=subject,
                      sender=sender,
                      recipients=[recipients],
                      body=body)
    mailer.send(message)

    report_recipients = 'info@kaniru.com.ng'
    if not report_recipients:
        return

    report_recipients = [s.strip() for s in report_recipients.split(',')]

    # since the config options are interpreted (not raw)
    # the report_subject variable is not easily customizable.
    report_subject = "Registration activity for '%(recipients)s' : %(subject)s"

    report_prefix = 'Attention:'
    if report_prefix:
        report_subject = report_prefix + ' ' + report_subject

    d = { 'recipients': recipients, 'subject': subject }
    report_subject = report_subject % d

    body = "The following registration-related activity occurred: \r\n" + \
        "--------------------------------------------\r\n" + body

    message = Message(subject=report_subject,
                      sender=sender,
                      recipients=report_recipients,
                      body=body)
    mailer.send(message)

def kaniru_email_forgot(request, user_id, email, hmac):

    message_class = EmailMessageText()
    message_text = getattr(message_class, 'forgot')()

    message_body = message_text['body'].replace('%_url_%', \
        route_url('reset_password', request, user_id=user_id, hmac=hmac))

    kaniru_email(request, email, message_text['subject'], message_body)
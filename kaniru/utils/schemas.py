
from formencode import validators, Schema, All, Invalid
import formencode
from kaniru.models import Users, DBSession
from formencode.foreach import ForEach

class UniqueEmail(validators.FancyValidator):
    

    def __init__(self, fieldName, errorMessage):
        'Store fieldName and errorMessage'
        super(UniqueEmail, self).__init__()
        self.fieldName = fieldName
        self.errorMessage = errorMessage

    def _to_python(self, value, user):
        'Check whether the value is unique'
        # If the user is new or the value changed,
        if Users.get_by_email(value):
            raise Invalid(self.errorMessage, value, user)
        # Return
        return value


class Old_password(validators.FancyValidator):
    'Validator to ensure that the old password match during change of password'

    def _to_python(self, value, user):
        'Check whether the value is unique'
        if not Users.check_password(email=authenticated_userid(request),
                                    password=value):
            raise Invalid('Your old password doesn\'t match', value, user)
        # Return
        return value

class Email_Exist(validators.FancyValidator):
    'Validator to ensure that the email exist in our database'

    def _to_python(self, value, user):
        'Check whether the value is unique'
        if Users.get_by_email(value) is None:
            raise Invalid('Sorry that email doesn\'t exist.', value, user)
        # Return
        return value
class SecurePassword(validators.FancyValidator):
    'Validator to prevent weak passwords'

    def _to_python(self, value, user):
        'Check whether a password is strong enough'
        if len(set(value)) < 6:
            raise Invalid('That password needs more variety', value, user)
        return value



class ResetPasswordForm(Schema):
    allow_extra_fields = True
    filter_extra_fields = True
    
    password = All(
        validators.UnicodeString(
            min=6,
            not_empty=True),
        SecurePassword())
    
    password_confirm = validators.UnicodeString(not_empty=True)
    
    chained_validators = [validators.FieldsMatch(
        'password', 'password_confirm')]
    
    

class ForgotPasswordForm(Schema):
    allow_extra_fields = True
    filter_extra_fields = True
    
    
    email = All(
        validators.UnicodeString(
            not_empty=True,
            strip=True),
        validators.Email(),
        Email_Exist())
    
class ChangePasswordForm(Schema):
    allow_extra_fields = True
    filter_extra_fields = True
    
    user_id = validators.Int()
    old_password = password = All(
        validators.UnicodeString(
            min=6,
            not_empty=True),
        Old_password())
    password = validators.UnicodeString(not_empty=True)
    password_confirm = validators.UnicodeString(not_empty=True)
    chained_validators = [validators.FieldsMatch(
        'password', 'password_confirm')]
    
    
    
    
class UserAccountForm(Schema):
    'User account validator'

    allow_extra_fields = True
    filter_extra_fields = True

    first_name = validators.UnicodeString(not_empty=True,
                                          messages={'empty':u'Please enter your firstname'})
    
    last_name = validators.UnicodeString(not_empty=True,
                                         messages={'empty':u'Please enter your lastname'})
    
    
    email = All(
        validators.UnicodeString(
            not_empty=True,
            messages={'empty':u'Please enter your email address'},
            strip=True),
        validators.Email(),
        UniqueEmail('email', u'That email is reserved for another account'))
     
    password = All(
        validators.UnicodeString(
            min=6,
            max=15,
            messages={'empty':u'Please enter your password'},
            not_empty=True),
        SecurePassword())
    
    password_confirm = validators.UnicodeString(not_empty=True,
                                                messages={'empty':u'Please confirm your password'},)
    
    chained_validators = [validators.FieldsMatch(
        'password', 'password_confirm')]
    

class AgentForm(Schema):
        "User profile"
        allow_extra_fields = True
        filter_extra_fields = True
    
        first_name = validators.UnicodeString(not_empty=True,strip=True,min=2)
    
        last_name = validators.UnicodeString(not_empty=True,strip=True)
    
        email = All(
        validators.UnicodeString(
            not_empty=True,
            strip=True),
        validators.Email(),
        UniqueEmail('email', 'That email is reserved for another account'))
    
        user_type = validators.Int(not_empty=True,messages={'empty':"Please select your user type"})
        agency_name = validators.UnicodeString(not_empty=True)
        agency_website = validators.UnicodeString()
        agency_address = validators.UnicodeString()
    
        location = validators.Int(not_empty=True,
                              messages={'empty':"Please select your angency location"})
    
    

class UserProfile(Schema):
    "User profile"
    allow_extra_fields = True
    filter_extra_fields = True
    
    first_name = validators.UnicodeString(not_empty=True,strip=True,min=2)
    
    last_name = validators.UnicodeString(not_empty=True,strip=True)
    
    email = All(
        validators.UnicodeString(
            not_empty=True,
            strip=True),
        validators.Email())
    mobile = validators.UnicodeString()
    home_phone = validators.UnicodeString()
    office_phone = validators.UnicodeString()
    


    
class AgencyProfile(Schema):
    "Agency profile"
    allow_extra_fields = True
    filter_extra_fields = True
    
    agency_note = validators.UnicodeString()
    agency_name = validators.UnicodeString(not_empty=True)
    agency_website = validators.UnicodeString()
    agency_address = validators.UnicodeString(not_empty=True)
    
    state_id = validators.Int(not_empty=True,
                              messages={'empty':"Please select your agency location"})
    
class MailAgent(Schema):
    "Mail agent"
    allow_extra_fields = True
    filter_extra_fields = True
    
    fullname = validators.UnicodeString(not_empty=True)
    mobile = validators.UnicodeString(max=11)
    email = validators.Email(not_empty=True)
    body = validators.UnicodeString(not_empty=True)
    
class Step1(Schema):
    allow_extra_fields = True
    filter_extra_fields = True
    
    title = validators.UnicodeString(not_empty=True,messages={'empty':"This field is required"}, max=100)
    category_id = validators.Int(not_empty=True,
                                             messages={'empty':"This field is required"})
    subcategory_id = validators.Int(not_empty=True,
                                             messages={'empty':"This field is required"})
    state_id = validators.Int(not_empty=True,
                                             messages={'missing':"This field is required"})
    city = validators.UnicodeString()
    area = validators.UnicodeString()
    address = validators.UnicodeString(not_empty=True,
                                             messages={'empty':"This field is required"},max=100)

class Step2House(Schema):
    allow_extra_fields = True
    filter_extra_fields = True
    
    price = validators.Int(not_empty=True,
                                messages={'empty':"This field is required"})
    currency = validators.UnicodeString(not_empty=True,
                                             messages={'empty':"This field is required"})
    price_condition = validators.UnicodeString(max=100)
    deposit = validators.Int()
    agent_commission = validators.UnicodeString(max=100)
    bedroom = validators.Int(not_empty=True,
                                             messages={'empty':"This field is required"})
    bathroom = validators.Int(not_empty=True,
                                             messages={'empty':"This field is required"})
    total_room = validators.Int()
    living_area = validators.UnicodeString(not_empty=True,messages={'empty':"This field is required"})
    land_size=validators.UnicodeString(not_empty=True,
                                             messages={'empty':"This field is required"})
    date_available =validators.UnicodeString(max=50)
    description = validators.UnicodeString(not_empty=True,
                                             messages={'empty':"This field is required"})
    year_built = validators.Int()
    car_spaces = validators.Int()
    furnished = validators.OneOf(['Yes','No'])
    indoor = ForEach(validators.Int())
    outdoor = ForEach(validators.Int())
    eco = ForEach(validators.Int())


class Step2Apartment(Schema):
    allow_extra_fields = True
    filter_extra_fields = True
    
    price = validators.Int(not_empty=True,
                                messages={'empty':"This field is required"})
    currency = validators.UnicodeString(not_empty=True,
                                             messages={'empty':"This field is required"})
    price_condition = validators.UnicodeString(max=100)
    deposit = validators.Int()
    agent_commission = validators.UnicodeString(max=100)
    bedroom = validators.Int(not_empty=True,
                                             messages={'empty':"This field is required"})
    bathroom = validators.Int(not_empty=True,
                                             messages={'empty':"This field is required"})
    total_room = validators.Int()
    living_area = validators.UnicodeString(not_empty=True,messages={'empty':"This field is required"})
    floor=validators.Int(not_empty=True,
                                             messages={'empty':"This field is required"})
    total_floor=validators.Int(not_empty=True,
                                             messages={'empty':"This field is required"})
    date_available =validators.UnicodeString(max=50)
    description = validators.UnicodeString(not_empty=True,
                                             messages={'empty':"This field is required"})
    year_built = validators.Int()
    car_spaces = validators.Int()
    furnished = validators.OneOf(['Yes','No'])
    indoor = ForEach(validators.Int())
    outdoor = ForEach(validators.Int())
    eco = ForEach(validators.Int())


class Step2Commercial(Schema):
    allow_extra_fields = True
    filter_extra_fields = True
    
    price = validators.Int(not_empty=True,
                                messages={'empty':"This field is required"})
    currency = validators.UnicodeString(not_empty=True,
                                             messages={'empty':"This field is required"})
    price_condition = validators.UnicodeString(max=100)
    deposit = validators.Int()
    agent_commission = validators.UnicodeString(max=100)
   
    total_room = validators.Int()
    building_size = validators.UnicodeString(not_empty=True,messages={'empty':"This field is required"})
    
    date_available =validators.UnicodeString(max=50)
    description = validators.UnicodeString(not_empty=True,
                                             messages={'empty':"This field is required"})
    year_built = validators.Int()
    car_spaces = validators.Int()
    furnished = validators.OneOf(['Yes','No'])
    indoor = ForEach(validators.Int())
    outdoor = ForEach(validators.Int())
    eco = ForEach(validators.Int())
    

class Step2Land(Schema):
    allow_extra_fields = True
    filter_extra_fields = True
    
    price = validators.Int(not_empty=True,
                                messages={'empty':"This field is required"})
    currency = validators.UnicodeString(not_empty=True,
                                             messages={'empty':"This field is required"})
    price_condition = validators.UnicodeString(max=50)
    deposit = validators.Int()
    agent_commission = validators.UnicodeString(max=50)
   
    plot_size = validators.UnicodeString(not_empty=True,messages={'empty':"This field is required"})
    
    date_available =validators.UnicodeString(max=50)
    description = validators.UnicodeString(not_empty=True,
                                             messages={'empty':"This field is required"})
    year_built = validators.Int()
    car_spaces = validators.Int()
    


    
class Upload(Schema):
    filter_extra_fields = True
    allow_extra_fields = True
    pics = validators.String(not_empty=True)
    
class Step3(Schema):
    allow_extra_fields = True
    filter_extra_fields = True
    
    photo = validators.FieldStorageUploadConverter(not_empty=True, messages={'empty': 'You haven\'t selected any files'})
    
class Step4(Schema):
    "Contact Setting"
    
    allow_extra_fields = True
    filter_extra_fields = True
    
    display_fullname = validators.UnicodeString(not_empty=True,min=2)
    
    display_email = All(
        validators.UnicodeString(
            not_empty=True,
            strip=True),
        validators.Email(),
        )
    
    
    mobile = validators.UnicodeString(not_empty=True, max=11)
    home_phone = validators.UnicodeString(max=11)
    office_phone = validators.UnicodeString(max=11)
    
class NewsLetterSchema(Schema):
    allow_extra_fields = True
    filter_extra_fields = True
    
    subject = validators.UnicodeString(not_empty=True, max=150)
    body = validators.UnicodeString(not_empty=True)
    
class StateSchema(Schema):
    allow_extra_fields = True
    filter_extra_fields = True
    
    name = validators.UnicodeString(not_empty=True)
    
class LoginSchema(Schema):
    allow_extra_fields = True
    filter_extra_fields = True
    
    email = validators.Email(not_empty=True)
    password = validators.UnicodeString(not_empty=True)
    


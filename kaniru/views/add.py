from kaniru.utils.schemas import (
    Step1,
    Step2House,
    Step2Commercial,
    Step2Land,
    Step2Apartment,
    Step3,
    Step4,
    Upload
    )
import transaction
from pyramid_storage.exceptions import FileNotAllowed
#PIL
from PIL import Image
from kaniru.utils.water import watermark
import json

from cgi import FieldStorage
from pyramid.response import Response
from pyramid.view import view_config
from kaniru.models import DBSession,is_uploadable_mimetypes
from kaniru.models import (Photos,
    Users, State,City,Area,
    Commercials,House,Apartments,
    Lands,Listings,Category, 
    Subcategory)
from pyramid.httpexceptions import HTTPFound, HTTPNotFound, HTTPSeeOther
from pyramid.security import authenticated_userid

from pyramid.renderers import render_to_response, render
from pyramid_simpleform import Form
from pyramid_simpleform.renderers import FormRenderer

def get_category():
    query = DBSession.query(Category.id,Category.name).all()
    return query

def get_subcat():
    query = DBSession.query(Subcategory.id,Subcategory.name).filter_by(category_id=1).order_by(Subcategory.name).all()
    return query

def get_states():
    query = DBSession.query(State.id, State.name).all()
    return query

def get_cities():
    query = DBSession.query(City.id, City.name).all()
    return query

def get_areas():
    query = DBSession.query(Area.id, Area.name).all()
    return query


@view_config(route_name="create_listing",permission="post",
             renderer="kaniru:templates/derived/steps/creates.mako")
def step0(request):
    title = "Create New Listing"
    
    form = Form(request)
    session=request.session
    if 'for_sale' in request.POST:
        session['for_sale']='for_sale'
        if 'for_rent' in session:
            del session['for_rent']
        return HTTPFound(location=request.route_url('step1'))
    elif 'for_rent' in request.POST:
        session['for_rent']='for_rent'
        if 'for_sale' in session:
            del session['for_sale']
        return HTTPFound(location=request.route_url('step1'))
    action_url = request.route_url('create_listing')
    return {'title':title,'action_url':action_url,'form':FormRenderer(form)}



@view_config(route_name="get_cities",renderer="json")
def get_cities(request):
    cities = []
    for r in request.params:
        opts=r
    p=json.loads(opts)
    id=p['id']
    gcities=DBSession.query(City).filter_by(state_id=id).order_by(City.name).all()
    for c in gcities:
        d={"value":c.id,"label":c.name}
        cities.append(d)
    return cities

@view_config(route_name="get_areas",renderer="json")
def get_areas(request):
    areas = []
    for r in request.params:
        opts=r
    p=json.loads(opts)
    id=p['id']
    gareas=DBSession.query(Area).filter_by(city_id=id).order_by(Area.name).all()
    for a in gareas:
        d={"value":a.id,"label":a.name}
        areas.append(d)
    return areas

@view_config(route_name="get_categories",renderer="json")
def get_categories(request):
    c_dict=[]
    categories = DBSession.query(Category).order_by(Category.name).all()
    for c in categories:
        p={"value":c.id,"label":c.name}
        c_dict.append(p)
    return c_dict

@view_config(route_name="get_subcategories",renderer="json")
def get_subcategory(request):
    subs = []
    for r in request.params:
        opts=r
    p=json.loads(opts)
    id=p['id']
    subcategories=DBSession.query(Subcategory).filter_by(category_id=id).\
                 order_by(Subcategory.name).all()
    for sub in subcategories:
        d={"value":sub.id,"label":sub.name}
        subs.append(d)
    return subs


@view_config(route_name="step1",  permission="post",
             renderer="kaniru:templates/derived/steps/step1.mako")
def step01(request):
    title = 'Start'
    finished=''
    session = request.session
    states = get_states()
    category=get_category()
    subcategory = get_subcat()
    if 'obj1' in session:
        obj1 = session['obj1']
        form = Form(request, schema=Step1, obj=obj1)
        finished="bg-success"
    else:
        form = Form(request, schema=Step1)
    if 'step_to_2' in request.POST and form.validate():
        obj1 = form.bind(Listings())
        session['obj1'] = obj1
        if 'obj2' in session:
            del session['obj2']
        return HTTPFound(location=request.route_url("step2"))
    
    action_url = request.route_url("step1")
    return {'title':title,'form':FormRenderer(form),'action_url':action_url,
            'categories':category,'subcategories':subcategory,'finished':finished,
            'states':states}

@view_config(route_name="step2", permission="post")
def step2(request):
    title = 'Details'
    session = request.session

    if 'obj1' in session:
        obj1 = session['obj1']
    else:
        return HTTPFound(location=request.route_url('step1'))
    if obj1.category_id==2:
        #apartment
        if 'obj2' in session:
            obj2 = session['obj2']
            form = Form(request,schema = Step2Apartment, obj=obj2)
        else:
            form = Form(request,schema = Step2Apartment)
        
        if 'step_to_3' in request.POST and form.validate():
            obj2 = form.bind(Apartments())
            session['obj2'] = obj2
            return HTTPFound(location=request.route_url('step3'))
        if 'step_back' in request.POST:
            return HTTPFound(location=request.route_url('step1'))
        action_url = request.route_url('step2')
        return render_to_response("kaniru:templates/derived/steps/apartment/step2apartment.mako",
                                      dict(form=FormRenderer(form),title=title,
                                      action_url = action_url),request=request)
    elif obj1.category_id==3:
        if 'obj2' in session:
            obj2 = session['obj2']
            form = Form(request,schema = Step2Land, obj=obj2)
        else:
            form = Form(request,schema = Step2Land)
        
        if 'step_to_3' in request.POST and form.validate():
            obj2 = form.bind(Lands())
            session['obj2'] = obj2
            return HTTPFound(location=request.route_url('step3'))
        if 'step_back' in request.POST:
            return HTTPFound(location=request.route_url('step1'))
        action_url = request.route_url('step2')
        return render_to_response("kaniru:templates/derived/steps/land/step2land.mako",
                                      dict(form=FormRenderer(form),title=title,
                                      action_url = action_url),request=request)
    elif obj1.category_id==4:
        if 'obj2' in session:
            obj2 = session['obj2']
            form = Form(request,schema = Step2Commercial, obj=obj2)
        else:
            form = Form(request,schema = Step2Commercial)
        if 'step_to_3' in request.POST and form.validate():
            obj2 = form.bind(Commercials())
            session['obj2'] = obj2
            return HTTPFound(location=request.route_url('step3'))
        if 'step_back' in request.POST:
            return HTTPFound(location=request.route_url('step1'))
        action_url = request.route_url('step2')
        return render_to_response("kaniru:templates/derived/steps/commercial/step2com.mako",
                                      dict(form=FormRenderer(form),
                                        title=title,
                                      action_url = action_url),request=request)
    else:
        if 'obj2' in session:
            obj2 = session['obj2']
            form = Form(request,schema = Step2House, obj=obj2)
        else:
            form = Form(request,schema = Step2House)
        
        if 'step_to_3' in request.POST and form.validate():
            obj2 = form.bind(House())
            session['obj2'] = obj2
            return HTTPFound(location=request.route_url('step3'))
        if 'step_back' in request.POST:
            return HTTPFound(location=request.route_url('step1'))
        action_url = request.route_url('step2')
        return render_to_response("kaniru:templates/derived/steps/house/step2house.mako",
                                      dict(form=FormRenderer(form),
                                      title=title,
                                      action_url = action_url),request=request)
    
@view_config(route_name="step3", permission="post",renderer="kaniru:templates/derived/steps/step3.mako")
def step03(request):
    
    title = 'Uploads'
    form = Form(request)
    if 'step_to_4' in request.POST:
            return HTTPFound(location=request.route_url('step4'))
    
    elif 'step_back' in request.POST:
        return HTTPFound(location=request.route_url('step2'))
    action_url = request.route_url('step3')
    return dict(form=FormRenderer(form),
                title=title,
                up=up(request),
                action_url = action_url)

@view_config(route_name="upload", request_method="POST")
def uploader(request):
    files = []
    session = request.session
    form = Form(request,schema=Upload)
    if 'submit' in request.POST and form.validate():
        filename=request.storage.save(request.POST['pics'], randomize=True)
        if 'files' in session:
            session['files'].append(filename)
            session.changed()
        else:
            files.append(filename)
            session['files']=files
        return HTTPSeeOther(location=request.route_url('step3'))
    request.session.flash('warning;  An error occured')
    return HTTPFound(location=request.route_url('step3'))

@view_config(route_name="delete_upload", permission="post")
def deleteupload(request):
    session = request.session
    filename = request.matchdict['filename']
    request.storage.delete(filename)
    files=session['files']
    files.remove(filename)
    request.session.flash('success; deleted')
    return HTTPFound(location=request.route_url('step3'))

def up(request):
    files=[]
    session=request.session
    if 'files' in session:
        files=session['files']
    return render('kaniru:templates/derived/steps/uploadrender.mako',
                  dict(
                      files=files
                      ),
                  request=request)
#@view_config(route_name="serve")
#def serveimage(request):
    # retrieve uid from somewhere
    #id = request.matchdict['id']
    #im = DBSession.query(Image).get(id)
    #image = im.data

    #res = Response(
            #headerlist=[
                #('Content-Length', str(len(image))),
                #('Content-Type', str(im.mimetype)),
            #],
            #body=image,
           # )

    #return res


@view_config(route_name="step4",permission="post", renderer="kaniru:templates/derived/steps/step4.mako")
def step04(request):
    
    title = 'Contact Details'
    session = request.session
    if 'obj1' and 'obj2' in session:
        if 'obj4' in session:
            obj4 = session['obj4']
            form = Form(request, schema=Step4, obj=obj4)
        else:
            form = Form(request, schema=Step4)
        if 'step_to_5' in request.POST and form.validate():
            obj4 = form.bind(Listings())
            session['obj4'] = obj4
            return HTTPFound(location=request.route_url('step5'))
        elif 'step_back' in request.POST:
            return HTTPFound(location = request.route_url('step3'))
    else:
        return HTTPFound(location=request.route_url('step2'))
    action_url = request.route_url("step4")
    return {'title':title,'form':FormRenderer(form),'action_url':action_url}

@view_config(route_name ="step5",permission="post", renderer = "kaniru:templates/derived/steps/step5.mako")
def step5(request):
    title = 'Publish Listings'
    form = Form(request)
    session = request.session
    if 'obj1' and 'obj2' and 'obj4' in session:
        if 'files' in session:
            
            files = session['files']
        else:
            request.session.flash("danger; Please upload at least a picture")
            return HTTPFound(location=request.route_url('step3'))
        obj1 = session['obj1']
        obj2 = session['obj2']
        obj4 = session['obj4']
        if 'publish' in request.POST:
            if obj1.category_id==2:#Apartment Listing for sale
                category = Category.get_by_id(2)
                state=State.get_by_id(obj1.state_id)
                subcategory = Subcategory.get_by_id(obj1.subcategory_id)
                apartment = Apartments(
                    title = obj1.title,
                    user_id =request.user.id,
                    city = obj1.city,
                    area = obj1.area,
                    address = obj1.address,
                    price=obj2.price,
                    currency = obj2.currency,
                    price_available=obj2.price_available,
                    price_condition = obj2.price_condition,
                    deposit = obj2.deposit,
                    agent_commission = obj2.agent_commission,
                    car_spaces = obj2.car_spaces,
                    year_built = obj2.year_built,
                    description = obj2.description,
                    display_fullname = obj4.display_fullname,
                    display_email = obj4.display_email,
                    home_phone= obj4.home_phone,
                    mobile = obj4.mobile,
                    office_phone = obj4.office_phone,
                    date_available = obj2.date_available,
                    
                    furnished = obj2.furnished,
                    bathroom = obj2.bathroom,
                    bedroom = obj2.bedroom,
                    total_room = obj2.total_room,
                    floor = obj2.floor,
                    total_floor = obj2.total_floor,
                    living_area = obj2.living_area
                    )
                DBSession.add(apartment)
                if category:
                    category.listings.append(apartment)
                if subcategory:
                    subcategory.listings.append(apartment)
                if state:
                    state.listings.append(apartment)
                
                if 'for_rent' in session:
                    apartment.for_sale=False
                DBSession.flush()
                for filename in files:
                    pix = Photos(
                            filename = filename
                        )
                    DBSession.add(pix)
                    apartment.photos.append(pix)
                    DBSession.flush()
                del session['obj1']
                del session['obj2']
                del session['obj4']
                del session['files']
                request.session.flash("success; Success")
                return HTTPFound(location=request.route_url('property_view', listing_id=apartment.id, slug=apartment.slug))
            elif obj1.category_id==3:#Land for sale
                category = Category.get_by_id(3)
                state=State.get_by_id(obj1.state_id)
                subcategory = Subcategory.get_by_id(obj1.subcategory_id)
                
                land = Lands(
                    title = obj1.title,
                    user_id =request.user.id,
                    city = obj1.city,
                    area = obj1.area,
                    address = obj1.address,
                    price=obj2.price,
                    currency = obj2.currency,
                    price_available=obj2.price_available,
                    price_condition = obj2.price_condition,
                    deposit = obj2.deposit,
                    agent_commission = obj2.agent_commission,
                    car_spaces = obj2.car_spaces,
                    year_built = obj2.year_built,
                    description = obj2.description,
                    display_fullname = obj4.display_fullname,
                    display_email = obj4.display_email,
                    home_phone= obj4.home_phone,
                    mobile = obj4.mobile,
                    office_phone = obj4.office_phone,
                    plot_size = obj2.plot_size,
                    date_available = obj2.date_available
                    )
                DBSession.add(land)
                if category:
                    category.listings.append(land)
                if subcategory:
                    subcategory.listings.append(land)
                if state:
                    state.listings.append(land)
                
                if 'for_rent' in session:
                    land.for_sale=False
                DBSession.flush()
                for filename in files:
                    pix = Photos(
                            filename = filename
                        )
                    DBSession.add(pix)
                    land.photos.append(pix)
                    DBSession.flush()
                del session['obj1']
                del session['obj2']
                del session['obj4']
                del session['files']
                request.session.flash("success; Success")
                return HTTPFound(location=request.route_url('property_view', listing_id=land.id, slug=land.slug))
            elif obj1.category_id==4:
                category = Category.get_by_id(4)
                state=State.get_by_id(obj1.state_id)
                subcategory = Subcategory.get_by_id(obj1.subcategory_id)
                
                com = Commercials(
                    title = obj1.title,
                    user_id =request.user.id,
                    city = obj1.city,
                    area = obj1.area,
                    address = obj1.address,
                    price=obj2.price,
                    currency = obj2.currency,
                    price_available=obj2.price_available,
                    price_condition = obj2.price_condition,
                    deposit = obj2.deposit,
                    agent_commission = obj2.agent_commission,
                    car_spaces = obj2.car_spaces,
                    year_built = obj2.year_built,
                    description = obj2.description,
                    display_fullname = obj4.display_fullname,
                    display_email = obj4.display_email,
                    home_phone= obj4.home_phone,
                    mobile = obj4.mobile,
                    office_phone = obj4.office_phone,
                    
                    date_available = obj2.date_available,
                    building_size = obj2.building_size,
                    total_room = obj2.total_room
                    )
                DBSession.add(com)
                if category:
                    category.listings.append(com)
                if subcategory:
                    subcategory.listings.append(com)
                if state:
                    state.listings.append(com)
                
                if 'for_rent' in session:
                    com.for_sale=False
                DBSession.flush()
                for filename in files:
                    
                    pix = Photos(
                            filename =filename
                        )
                    DBSession.add(pix)
                    com.photos.append(pix)
                    DBSession.flush()
                del session['obj1']
                del session['obj2']
                del session['obj4']
                del session['files']
                request.session.flash("success; Success")
                return HTTPFound(location=request.route_url('property_view', listing_id=com.id, slug=com.slug))
            elif obj1.category_id ==1:
                category = Category.get_by_id(1)
                state=State.get_by_id(obj1.state_id)
                subcategory = Subcategory.get_by_id(obj1.subcategory_id)
                
                house = House(
                    title = obj1.title,
                    user_id =request.user.id,
                    city = obj1.city,
                    area = obj1.area,
                    address = obj1.address,
                    price=obj2.price,
                    currency = obj2.currency,
                    price_available=obj2.price_available,
                    price_condition = obj2.price_condition,
                    deposit = obj2.deposit,
                    agent_commission = obj2.agent_commission,
                    car_spaces = obj2.car_spaces,
                    year_built = obj2.year_built,
                    description = obj2.description,
                    display_fullname = obj4.display_fullname,
                    display_email = obj4.display_email,
                    home_phone= obj4.home_phone,
                    mobile = obj4.mobile,
                    office_phone = obj4.office_phone,
                    date_available = obj2.date_available,
                    
                    furnished = obj2.furnished,
                    bathroom = obj2.bathroom,
                    bedroom = obj2.bedroom,
                    total_room = obj2.total_room,
                    land_size = obj2.land_size,
                    living_area = obj2.living_area
                    )
                DBSession.add(house)
                if category:
                    category.listings.append(house)
                if subcategory:
                    subcategory.listings.append(house)
                if state:
                    state.listings.append(house)
                
                if 'for_rent' in session:
                    house.for_sale=False
                DBSession.flush()
                for filename in files:
                    
                    pix = Photos(
                            filename = filename
                        )
                    DBSession.add(pix)
                    house.photos.append(pix)
                    DBSession.flush()
                del session['obj1']
                del session['obj2']
                del session['obj4']
                del session['files']
                request.session.flash("success; Success")
                return HTTPFound(location=request.route_url('property_view', listing_id=house.id, slug=house.slug))
            else:
                category = Category.get_by_id(5)
                state=State.get_by_id(obj1.state_id)
                subcategory = Subcategory.get_by_id(obj1.subcategory_id)
                
                dev = House(
                    title = obj1.title,
                    user_id =request.user.id,
                    city = obj1.city,
                    area = obj1.area,
                    category_id=5,
                    subcategory_id =obj1.subcategory_id,
                    state_id = obj1.state_id,
                    city_id = obj1.city_id,
                    area_id = obj1.area_id,
                    address = obj1.address,
                    price=obj2.price,
                    currency = obj2.currency,
                    price_available=obj2.price_available,
                    price_condition = obj2.price_condition,
                    deposit = obj2.deposit,
                    agent_commission = obj2.agent_commission,
                    car_spaces = obj2.car_spaces,
                    year_built = obj2.year_built,
                    description = obj2.description,
                    display_fullname = obj4.display_fullname,
                    display_email = obj4.display_email,
                    home_phone= obj4.home_phone,
                    mobile = obj4.mobile,
                    office_phone = obj4.office_phone,
                    date_available = obj2.date_available,
                    
                    furnished = obj2.furnished,
                    bathroom = obj2.bathroom,
                    bedroom = obj2.bedroom,
                    total_room = obj2.total_room,
                    land_size = obj2.land_size,
                    living_area = obj2.living_area
                    )
                DBSession.add(dev)
                if category:
                    category.listings.append(dev)
                if subcategory:
                    subcategory.listings.append(dev)
                if state:
                    state.listings.append(dev)
                
                if 'for_rent' in session:
                    dev.for_sale=False
                DBSession.flush()
                for filename in files:
                    
                    pix = Photos(
                            filename = filename
                        )
                    DBSession.add(pix)
                    dev.photos.append(pix)
                    DBSession.flush()
                del session['obj1']
                del session['obj2']
                del session['obj4']
                del session['files']
                request.session.flash("success; Success")
                return HTTPFound(location=request.route_url('property_view', listing_id=dev.id, slug=dev.slug))
        elif 'step_back' in request.POST:
            return HTTPFound(location = request.route_url('step4'))
            
        
    else:
        return HTTPFound(location=request.route_url('step2'))
    return dict(title=title,obj1=obj1,obj2=obj2,obj4=obj4,
                form=FormRenderer(form),files=files)

@view_config(route_name="delete_listing",permission="admin")
def deletelisting(request):
    listing_id = request.matchdict['listing_id']
    dbsession=DBSession()
    listing = Listings.get_by_id(listing_id)
    if listing is None:
        request.session.flash("info; No such listing")
        return HTTPFound(location = request.route_url("browse_property"))
    try:
        transaction.begin()
        dbsession.delete(listing);
        transaction.commit()
        request.session.flash("success; Listing deleted")
        return HTTPFound(location=request.route_url("browse_property"))
    except IntegrityError:
        transaction.abort()
        request.session.flash("info; Operation failed")
        return HTTPFound(location=request.route_url("browse_property"))
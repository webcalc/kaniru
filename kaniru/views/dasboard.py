import transaction
from pyramid.view import view_config
from pyramid_mailer import get_mailer
from pyramid_mailer.message import Message
import transaction
from webhelpers.paginate import PageURL_WebOb, Page 
from sqlalchemy import or_
from pyramid.renderers import render_to_response
from kaniru.utils.schemas import NewsLetterSchema, StateSchema, Upload
from kaniru.models import (State,
                           City,
                           Area,
                           Category,
                           Subcategory,
                           Users,
                           Listings,
                           Feature_types,
                           Features,
                           DBSession,
                           Groups,
                           Adverts,
                           EmailBank)
from pyramid_simpleform import Form
from pyramid_simpleform.renderers import FormRenderer
from pyramid.httpexceptions import HTTPFound, HTTPNotFound
from sqlalchemy.exc import IntegrityError




@view_config(route_name='admin_search_listing',permission='admin',
             renderer='kaniru:templates/component/listing/listsearch.mako')
def adminlistsearch(request):
    dbsession=DBSession()
    
    title = 'Admin search listing'
    
    term = request.params.get('search','').strip()
    
    listing = dbsession.query(Listings).filter(Listings.serial==term).first()
    
    return dict(
        listing=listing,
        title=title
        )
    
@view_config(route_name="pushfront",permission='admin')
def pushf(request):
    dbsession=DBSession()
    listing_id = request.matchdict['listing_id']
    listing = Listings.get_by_id(listing_id)
    if listing is None:
        request.session.flash("info; No such listing")
        return HTTPFound(location = request.route_url("browse_property"))

    listing.pushfront = True
    dbsession.flush()
    request.session.flash("success; Listing Pushed to front")
    return HTTPFound(location=request.route_url("home"))
    
    
@view_config(route_name="dashboard",permission='admin',renderer="kaniru:templates/component/dashboard.mako")
def dash(request):
    title="Dashboard"
    return dict(title=title)



@view_config(route_name="send_news",permission='admin', renderer= "kaniru:templates/component/news/send.mako")
def send(request):
    title = "Send News letter"
    form = Form(request, schema=NewsLetterSchema)
    if 'form_submitted' in request.POST and form.validate():
        sender = 'info@kaniru.com.ng'
        subject = form.data['subject']
        body = form.data['body']
        report_recipients = [sender]
        emails = DBSession.query(EmailBank).all()
        
        send_to = [e.address for e in emails]+report_recipients
        
        
        mailer = get_mailer(request)
        message = Message(subject=subject,
                      sender=sender,
                      recipients=send_to,
                      body=body)
        mailer.send(message)
        transaction.commit()
        request.session.flash("success; message sent")
        return HTTPFound(location=request.route_url('dashboard'))
    return dict(
        form = FormRenderer(form),
        title=title)




@view_config(route_name="ad_list",permission='admin',renderer="kaniru:templates/component/ads/ads.mako")
def page_ad_views(request):
    ads = DBSession.query(Adverts).all()
    
    title = "Adverts"
    
    return dict(
        ads=ads,
        title=title)

#ads 
@view_config(route_name="create_ads",permission='admin',renderer="kaniru:templates/component/ads/createads.mako")
def create_ads(request):
    title = "Create Ads"
    form = Form(request)
    if 'url_submitted' in request.POST:
        ad = Adverts(
            url = request.params.get('url')
            )
        DBSession.add(ad)
        DBSession.flush()
        request.session.flash('success; Success')
        return HTTPFound(location=request.route_url('ad_list'))
    return dict(
        title=title,
        form=FormRenderer(form)
        )
@view_config(route_name="post_ad", permission="admin", request_method="POST")
def postads(request):
    title="Post ads"
    form = Form(request)
    if 'form_submitted' in request.POST:
        filen=request.storage.save(request.POST['pics'], randomize=True)
        link = request.params.get('link')
        ad=Adverts(
            filename=filen,
            link=link
        )
        DBSession.add(ad)
        DBSession.flush()
    
        request.session.flash('success; Success')
        return HTTPFound(location=request.route_url('ad_list'))

@view_config(route_name="front_ad",permission="admin")
def frontad(request):
    ad_id = request.matchdict['ad_id']
    advert = DBSession.query(Adverts).get(ad_id)
    if advert is None:
        request.session.flash('warning; No such advert')
        return HTTPFound(location=request.route_url('ad_list'))
    #run check for ad on front page
    f_ad = DBSession.query(Adverts).filter(Adverts.frontpage==True).first()
    if f_ad:
        f_ad.frontpage=False
    advert.frontpage=True
    DBSession.flush()
    request.session.flash('success; ad pushed to front page')
    return HTTPFound(location=request.route_url('ad_list'))

@view_config(route_name="first_on_second", permission="admin")
def firstad(request):
    ad_id = request.matchdict['ad_id']
    advert = DBSession.query(Adverts).get(ad_id)
    if advert is None:
        request.session.flash('warning; No such advert')
        return HTTPFound(location=request.route_url('ad_list'))
    #run check for ad on front page
    f_ad = DBSession.query(Adverts).filter(Adverts.first==True).first()
    if f_ad:
        f_ad.first=False
    advert.first=True
    DBSession.flush()
    request.session.flash('success; ad pushed to first position on second page')
    return HTTPFound(location=request.route_url('ad_list'))

@view_config(route_name="ad_on_viewpage", permission="admin")
def onviewpage(request):
    ad_id = request.matchdict['ad_id']
    advert = DBSession.query(Adverts).get(ad_id)
    if advert is None:
        request.session.flash('warning; No such advert')
        return HTTPFound(location=request.route_url('ad_list'))
    #run check for ad on front page
    f_ad = DBSession.query(Adverts).filter(Adverts.viewpage==True).first()
    if f_ad:
        f_ad.viewpage=False
    advert.viewpage=True
    DBSession.flush()
    request.session.flash('success; Ad pushed to property view page')
    return HTTPFound(location=request.route_url('ad_list'))

@view_config(route_name="second_on_second", permission="admin")
def secondad(request):
    ad_id = request.matchdict['ad_id']
    advert = DBSession.query(Adverts).get(ad_id)
    if advert is None:
        request.session.flash('warning; No such advert')
        return HTTPFound(location=request.route_url('ad_list'))
    #run check for ad on front page
    f_ad = DBSession.query(Adverts).filter(Adverts.second==True).first()
    if f_ad:
        f_ad.second=False
    advert.second=True
    DBSession.flush()
    request.session.flash('success; ad pushed to second position on second page')
    return HTTPFound(location=request.route_url('ad_list'))
    

@view_config(route_name="third_on_second", permission="admin")
def thirdad(request):
    ad_id = request.matchdict['ad_id']
    advert = DBSession.query(Adverts).get(ad_id)
    if advert is None:
        request.session.flash('warning; No such advert')
        return HTTPFound(location=request.route_url('ad_list'))
    #run check for ad on front page
    f_ad = DBSession.query(Adverts).filter(Adverts.third==True).first()
    if f_ad:
        f_ad.third=False
    advert.third=True
    DBSession.flush()
    request.session.flash('success; ad pushed to third position on second page')
    return HTTPFound(location=request.route_url('ad_list'))

@view_config(route_name="delete_ad", permission="admin")
def deletead(request):
    ad_id = request.matchdict['ad_id']
    advert = DBSession.query(Adverts).get(ad_id)
    if advert is None:
        request.session.flash('warning; No such advert')
        return HTTPFound(location=request.route_url('ad_list'))
    
    try:
        transaction.begin()
        DBSession.delete(advert);
        transaction.commit()
        request.session.flash('success; ad deleted')
        return HTTPFound(location = request.route_url("ad_list"))
    except IntegrityError:
        transaction.abort()
        request.session.flash('danger; unable to delete ad')
        return HTTPFound(location=request.route_url('ad_list'))

    

@view_config(route_name='list_users',permission='admin')
def listusers(request):
    dbsession=DBSession()
    
    title = 'User list'
    
    search_term = request.params.get('search','')
    if search_term=='':
        search = dbsession.query(Users).filter(Users.id!=2).\
               order_by(Users.first_name).all()
    else:
        terms = search_term.split()
        search = dbsession.query(Users).\
               filter(or_(Users.first_name.in_(terms),
                    Users.last_name.in_(terms)
                        )).order_by(Users.first_name)\
               .filter(Users.id!=2).all()
    page_url = PageURL_WebOb(request)
    paginator= Page(search, 
                     page=int(request.params.get("page", 1)), 
                     items_per_page=10, 
                     url=page_url)
    return render_to_response('kaniru:templates/component/users/userlist.mako',
                                      dict(paginator=paginator,title=title),
                                      request=request)

@view_config(route_name="add_to_groups",permission='admin',)
def addg(request):
    user_id = request.matchdict['user_id']
    user = Users.get_by_id(user_id)
    if 'add_group' in request.POST and form.validate():
        for i, grp in enumerate(user.mygroups):
            if grp.id not in form.data['groups']:
                del user.mygroups[i]
        grpids = [g.id for g in user.mygroups]
        for grp in form.data['groups']:
            if grp not in grpids:
                g = DBSession.query(Groups).get(grp)
                user.mygroups.append(g)
        transaction.commit()
        request.session.flash('Success; Groups added to user')
        return HTTPFound(location=request.route_url('list_users'))
    return HTTPFound(location=request.route_url('list_users'))



@view_config(route_name="add_city",permission='admin', renderer="kaniru:templates/component/location/addcity.mako")
def addcity(request):
    title="Add cities"
    state_id = request.matchdict['state_id']
    state = State.get_by_id(state_id)
    form = Form(request, schema=StateSchema)
    if 'form_submitted' in request.POST and form.validate():
        city_string = form.data['name']
        city=City.create_city(state_id,city_string)
        request.session.flash("success; City(ies) added")
        return HTTPFound(location=request.route_url('view_state', state_id=state_id))
    action_url = request.route_url('add_city', state_id=state_id)
    return dict(
        title=title,
        state=state,
        form=FormRenderer(form),
        action_url=action_url)

@view_config(route_name ="view_city", permission='admin',renderer="kaniru:templates/component/location/viewcity.mako")
def viewcity(request):
    
    city_id = request.matchdict['city_id']
    city = City.get_by_id(city_id)
    title="Viewing %s" %(city.name)
    return dict(
        title=title,
        city=city,
        )

@view_config(route_name="delete_city",permission='admin',)
def delete_city(request):
    dbsession=DBSession()
    city_id = request.matchdict['city_id']
    state_id = request.matchdict['state_id']
    city = City.get_by_id(city_id)
    if city is None:
        request.session.flash('info; No such city')
        return HTTPFound(location = request.route_url("view_state",state_id=state_id))
    try:
        transaction.begin()
        dbsession.delete(city);
        transaction.commit()
        request.session.flash('success; city deleted')
        return HTTPFound(location = request.route_url("view_state",state_id=state_id))
    except IntegrityError:
        transaction.abort()
        request.session.flash('info; operation failed')
        return HTTPFound(location = request.route_url("view_state",state_id=state_id))

@view_config(route_name ="edit_city",permission='admin', renderer="kaniru:templates/component/location/editcity.mako")
def editcity(request):
    city_id = request.matchdict['city_id']
    state_id = request.matchdict['state_id']
    state=State.get_by_id(state_id)
    city =City.get_by_id(city_id)
    title = "Editing %s" %(city.name)
    form = Form(request, schema=StateSchema)
    if 'form_submitted' in request.POST and form.validate():
        s_string = form.data['name']
        city.name = s_string
        city.state_id = state_id
        transaction.commit()
        request.session.flash("success; city updated")
        return HTTPFound(location=request.route_url('view_state', state_id=state_id))
    action_url = request.route_url('edit_city',city_id=city_id, state_id=state_id)
    return dict(
        title=title,
        state=state,
        form=FormRenderer(form),
        city=city,
        action_url=action_url)

@view_config(route_name="add_area", permission='admin',renderer="kaniru:templates/component/location/addarea.mako")
def addarea(request):
    title="Add area to city"
    city_id = request.matchdict['city_id']
    city = City.get_by_id(city_id)
    form = Form(request, schema=StateSchema)
    if 'form_submitted' in request.POST and form.validate():
        area_string = form.data['name']
        area=Area.create_area(city_id,area_string)
        request.session.flash("success; Area(s) added")
        return HTTPFound(location=request.route_url('view_city', city_id=city_id))
    action_url = request.route_url('add_area',city_id=city_id)
    return dict(
        title=title,
        city=city,
        form=FormRenderer(form),
        action_url=action_url)

@view_config(route_name ="edit_area",permission='admin', renderer="kaniru:templates/component/location/editarea.mako")
def editarea(request):
    area_id = request.matchdict['area_id']
    city_id = request.matchdict['city_id']
    city = City.get_by_id(city_id)
    area =Area.get_by_id(area_id)
    title = "Editing %s" %(area.name)
    form = Form(request, schema=StateSchema)
    if 'form_submitted' in request.POST and form.validate():
        s_string = form.data['name']
        area.name = s_string
        area.city_id = city_id
        transaction.commit()
        request.session.flash("success; Area updated")
        return HTTPFound(location=request.route_url('view_city',city_id=city_id))
    action_url = request.route_url('edit_area',area_id=area_id,city_id=city_id)
    return dict(
        form=FormRenderer(form),
        area=area,
        city=city,
        title=title,
        action_url=action_url)


@view_config(route_name="delete_area",permission='admin')
def delete_area(request):
    dbsession=DBSession()
    area_id = request.matchdict['area_id']
    city_id = request.matchdict['city_id']
    area = Area.get_by_id(area_id)
    if area is None:
        request.session.flash('info; No such area')
        return HTTPFound(location = request.route_url("view_city",city_id=city_id))
    try:
        transaction.begin()
        dbsession.delete(area);
        transaction.commit()
        request.session.flash('success; area deleted')
        return HTTPFound(location = request.route_url("view_city",city_id=city_id))
    except IntegrityError:
        transaction.abort()
        request.session.flash('info; operation failed')
        return HTTPFound(location = request.route_url("view_city",city_id=city_id))
    
@view_config(route_name ="view_state",permission='admin', renderer="kaniru:templates/component/location/viewstate.mako")
def viewstate(request):
    state_id = request.matchdict['state_id']
    state = State.get_by_id(state_id)
    title = "Viewing %s" %(state.name)
    cities = DBSession.query(City).filter(City.state_id ==state_id).order_by(City.name).all()
    page_url = PageURL_WebOb(request)
    paginator = Page(cities, 
                     page=int(request.params.get("page", 1)), 
                     items_per_page=20, 
                     url=page_url)
    return dict(
        state=state,
        paginator = paginator,
        title=title
        )

@view_config(route_name ="add_state", permission='admin',renderer="kaniru:templates/component/location/addstate.mako")
def addstate(request):
    title = "Add State"
    form = Form(request, schema=StateSchema)
    if 'form_submitted' in request.POST and form.validate():
        
        s_string = form.data['name']
        state=State.create_state(s_string)
        request.session.flash("success; State(s) added")
        return HTTPFound(location=request.route_url('list_state'))
    action_url = request.route_url('add_state')
    return dict(
        title=title,
        form=FormRenderer(form),
        action_url=action_url)

@view_config(route_name ="edit_state",permission='admin', renderer="kaniru:templates/component/location/editstate.mako")
def editstate(request):
    state_id = request.matchdict['state_id']
    state =State.get_by_id(state_id)
    title = "Editing %s" %(state.name)
    form = Form(request, schema=StateSchema)
    if 'form_submitted' in request.POST and form.validate():
        s_string = form.data['name']
        state.name = s_string
        transaction.commit()
        request.session.flash('success; State updated')
        return HTTPFound(location=request.route_url('list_state'))
    action_url = request.route_url('edit_state', state_id=state_id)
    return dict(
        title=title,
        form=FormRenderer(form),
        state=state,
        action_url=action_url)


@view_config(route_name="delete_state",permission='admin')
def delete_state(request):
    dbsession = DBSession()
    state_id = request.matchdict['state_id']
    state = State.get_by_id(state_id)
    if state is None:
        request.session.flash("info; No such state")
        return HTTPFound(location = request.route_url("list_state"))
    try:
        transaction.begin()
        dbsession.delete(state);
        transaction.commit()
        request.session.flash("success; State deleted")
        return HTTPFound(location=request.route_url("list_state"))
    except IntegrityError:
        transaction.abort()
        request.session.flash("info; Operation failed")
        return HTTPFound(location=request.route_url("list_state"))



@view_config(route_name ="list_state",permission='admin', renderer="kaniru:templates/component/location/states.mako")
def liststate(request):
    title = "State List"
    page = int(request.params.get('page', 1))
    paginator = State.get_paginator(request, page)
    total = DBSession.query(State).count()
    return dict(
        title=title,
        total=total,
        paginator=paginator,
        )


@view_config(route_name ="add_category",permission='admin', renderer="kaniru:templates/component/category/addcategory.mako")
def addcat(request):
    title="Add category"
    
    form = Form(request, schema=StateSchema)
    if 'form_submitted' in request.POST and form.validate():
        s_string = form.data['name']
        category=Category.create_categories(s_string)
        request.session.flash('success; category(ies) added')
        return HTTPFound(location=request.route_url('list_categories'))
    action_url = request.route_url('add_category')
    return dict(
        title=title,
        form=FormRenderer(form),
        action_url=action_url)

@view_config(route_name ="view_category", permission='admin',renderer="kaniru:templates/component/category/viewcategory.mako")
def viewcate(request):
    cate_id = request.matchdict['category_id']
    category = Category.get_by_id(cate_id)
    title = "Viewing %s" %(category.name)
    return dict(
        category=category,
        title=title
        )

@view_config(route_name ="list_categories", permission='admin',renderer="kaniru:templates/component/category/list.mako")
def listcat(request):
    title = "All categories"
    category = Category.all()
    return dict(
        title = title,
        category=category
        )

@view_config(route_name="delete_category",permission='admin')
def delete_cate(request):
    #dbsession=DBSession()
    category_id = request.matchdict['category_id']
    category = Category.get_by_id(category_id)
    if category is None:
        request.session.flash('success; Category not found')
        return HTTPFound(location = request.route_url("list_categories"))
    try:
        transaction.begin()
        dbsession.delete(category);
        transaction.commit()
        request.session.flash('success; Category deleted')
        return HTTPFound(location = request.route_url("list_categories"))
    except IntegrityError:
        transaction.abort()
        request.session.flash('success; Operation failed')
        return HTTPFound(location=request.route_url("list_category"))

@view_config(route_name ="edit_category",permission='admin', renderer="kaniru:templates/component/category/editcategory.mako")
def editcate(request):
    c_id = request.matchdict['category_id']
    category =Category.get_by_id(c_id)
    title = "Editing %s" %(category.name)
    form = Form(request, schema=StateSchema)
    if 'form_submitted' in request.POST and form.validate():
        s_string = form.data['name']
        category.name = s_string
        transaction.commit()
        request.session.flash('success; Category updated')
        return HTTPFound(location=request.route_url('list_categories'))
    action_url = request.route_url('edit_category', category_id=category.id)
    return dict(
        form=FormRenderer(form),
        category=category,
        title=title,
        action_url=action_url)

@view_config(route_name="delete_subcategory",permission='admin')
def delete_subcat(request):
    dbsession = DBSession()
    sub_id = request.matchdict['sub_id']
    category_id = request.matchdict['category_id']
    subcategory = Subcategory.get_by_id(sub_id)
    if subcategory is None:
        request.session.flash('success; Sub-Category not found')
        return HTTPFound(location = request.route_url("view_category", category_id=category_id))
    try:
        transaction.begin()
        dbsession.delete(subcategory);
        transaction.commit()
        request.session.flash('success; Sub-Category deleted')
        return HTTPFound(location = request.route_url("view_category", category_id=category_id))
    except IntegrityError:
        transaction.abort()
        request.session.flash('success; Operation failed')
        return HTTPFound(location = request.route_url("view_category", category_id=category_id))

@view_config(route_name ="add_subcategory", permission='admin',renderer="kaniru:templates/component/category/addsubcategory.mako")
def addsub(request):
    title="Add Subcategory to category"
    category_id = request.matchdict['category_id']
    category=Category.get_by_id(category_id)
    form = Form(request, schema=StateSchema)
    if 'form_submitted' in request.POST and form.validate():
        s_string = form.data['name']
        category=Subcategory.create_subcategories(category_id,s_string)
        request.session.flash('success; Sub-Category added')
        return HTTPFound(location=request.route_url('view_category', category_id=category_id))
    action_url = request.route_url('add_subcategory', category_id=category_id)
    return dict(
        form=FormRenderer(form),
        title=title,
        category=category,
        action_url=action_url)

@view_config(route_name ="edit_subcategory",permission='admin', renderer="kaniru:templates/component/category/editsubcategory.mako")
def editsub(request):
    sub_id = request.matchdict['sub_id']
    c_id = request.matchdict['category_id']
    subcategory =Subcategory.get_by_id(sub_id)
    title = "Editing %s" %(subcategory.name)
    form = Form(request, schema=StateSchema)
    if 'form_submitted' in request.POST and form.validate():
        s_string = form.data['name']
        subcategory.name = s_string
        transaction.commit()
        request.session.flash('success; Sub-Category updated')
        return HTTPFound(location=request.route_url('view_category',category_id=c_id))
    action_url = request.route_url('edit_subcategory',sub_id=sub_id, category_id=c_id)
    return dict(
        title=title,
        subcategory=subcategory,
        form=FormRenderer(form),
        action_url=action_url)

@view_config(route_name ="feature_type", permission='admin',renderer="kaniru:templates/component/feature/addftype.mako")
def addf_type(request):
    title = "Add Feature Type"
    form = Form(request, schema=StateSchema)
    if 'form_submitted' in request.POST and form.validate():
        
        s_string = form.data['name']
        feature_type=Feature_types.create_ftypes(s_string)
        request.session.flash("success; feature types added")
        return HTTPFound(location=request.route_url('list_featuretypes'))
    action_url = request.route_url('feature_type')
    return dict(
        title=title,
        form=FormRenderer(form),
        action_url=action_url)


@view_config(route_name ="edit_featuretype",permission='admin', renderer="kaniru:templates/component/feature/editftype.mako")
def editf_type(request):
    type_id = request.matchdict['featuretype_id']
    feature_type =Feature_types.get_by_id(type_id)
    title = "Editing %s" %(feature_type.name)
    form = Form(request, schema=StateSchema)
    if 'form_submitted' in request.POST and form.validate():
        s_string = form.data['name']
        feature_type.name = s_string
        transaction.commit()
        request.session.flash('success; Feature type updated')
        return HTTPFound(location=request.route_url('list_featuretypes'))
    action_url = request.route_url('edit_featuretype',featuretype_id=type_id)
    return dict(
        title=title,
        feature_type=feature_type,
        form=FormRenderer(form),
        action_url=action_url)

@view_config(route_name ="list_featuretypes", renderer="kaniru:templates/component/feature/list.mako")
def listf_types(request):
    title = "All Feature Types"
    typeq = Feature_types.all()
    return dict(
        title = title,
        feature_type = typeq
        )

@view_config(route_name="delete_featuretypes",permission='admin',)
def delete_f_type(request):
    f_id = request.matchdict['featuretype_id']
    typeq = Feature_types.get_by_id(f_id)
    if typeq is None:
        request.session.flash('info; Feature type not found')
        return HTTPFound(location = request.route_url("list_featuretypes"))
    try:
        transaction.begin()
        DBSession.delete(typeq);
        transaction.commit()
        request.session.flash('success; Feature type deleted')
        return HTTPFound(location = request.route_url("list_featuretypes"))
    except IntegrityError:
        transaction.abort()
        request.session.flash('success; Operation failed')
        return HTTPFound(location=request.route_url("list_featuretypes"))
    

@view_config(route_name ="view_featuretypes", permission='admin',renderer="kaniru:templates/component/feature/viewftype.mako")
def viewf_type(request):
    f_id = request.matchdict['featuretype_id']
    feature_type = Feature_types.get_by_id(f_id)
    title = "Viewing %s" %(feature_type.name)
    return dict(
        feature_type=feature_type,
        title=title
        )


@view_config(route_name ="add_features", permission='admin',renderer="kaniru:templates/component/feature/addfeatures.mako")
def addfeature(request):
    title="Add feature to selected feature type"
    featuretype_id = request.matchdict['featuretype_id']
    ftype = Feature_types.get_by_id(featuretype_id)
    form = Form(request, schema=StateSchema)
    if 'form_submitted' in request.POST and form.validate():
    
        s_string = form.data['name']
        feature=Features.create_features(featuretype_id,s_string)
        request.session.flash('success; feature added')
        return HTTPFound(location=request.route_url('view_featuretypes',featuretype_id=featuretype_id))
    action_url = request.route_url('add_features', featuretype_id=featuretype_id)
    return dict(
        form=FormRenderer(form),
        title=title,
        feature_type=ftype,
        action_url=action_url)

@view_config(route_name ="edit_feature",permission='admin', renderer="kaniru:templates/component/feature/editfeature.mako")
def editfeature(request):
    feature_id = request.matchdict['feature_id']
    ftype_id = request.matchdict['featuretype_id']
    feature =Features.get_by_id(feature_id)
    title = "Editing %s" %(feature.name)
    form = Form(request, schema=StateSchema)
    if 'form_submitted' in request.POST and form.validate():
        s_string = form.data['name']
        feature.name = s_string
        feature.type_id = ftype_id
        transaction.commit()
        request.session.flash('success; Feature updated')
        return HTTPFound(location=request.route_url('view_featuretypes', featuretype_id=ftype_id))
    action_url = request.route_url('edit_feature', feature_id=feature_id,featuretype_id=ftype_id)
    return dict(
        title=title,
        feature=feature,
        ftype_id=ftype_id,
        form=FormRenderer(form),
        action_url=action_url)


@view_config(route_name="delete_feature",permission='admin')
def delete_feature(request):
    dbsession=DBSession()
    f_id = request.matchdict['feature_id']
    featuretype_id = request.matchdict['featuretype_id']
    feature = Features.get_by_id(f_id)
    if feature is None:
        request.session.flash('info; Feature not found')
        return HTTPFound(location = request.route_url("view_featuretypes", featuretype_id=featuretype_id))
    try:
        transaction.begin()
        dbsession.delete(feature);
        transaction.commit()
        request.session.flash('success; Feature deleted')
        return HTTPFound(location = request.route_url("view_featuretypes", featuretype_id=featuretype_id))
    except IntegrityError:
        transaction.abort()
        request.session.flash('success; Operation failed')
        return HTTPFound(location = request.route_url("view_featuretypes", featuretype_id=featuretype_id))
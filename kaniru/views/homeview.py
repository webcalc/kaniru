from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound, HTTPNotFound, HTTPForbidden

import json
from kaniru.models import (Users,
                           Adverts,
                           EmailBank,
                           State,
                           DBSession,
                           Category,
                           Listings)
from pyramid.security import authenticated_userid
from pyramid.renderers import render, render_to_response
from kaniru.models import DBSession as dbsession

@view_config(route_name='home',renderer="kaniru:templates/derived/home/home.mako")
def home_view(request):
    
    ad = DBSession.query(Adverts).filter(Adverts.frontpage==True).first()
    
    return {'title':'Home',
            'ad':ad,
            'topproducts':adminproduct(request),
            'featuredlistings':featuredview(request)}

@view_config(route_name='terms',renderer="kaniru:templates/terms.mako")
def terms(request):
    title = "Terms and conditions"
    return dict(title=title)

@view_config(route_name='contact',renderer="kaniru:templates/contact.mako")
def contactus(request):
    title = "Contact"
    return dict(title=title)

@view_config(route_name='about',renderer="kaniru:templates/about.mako")
def cus(request):
    title = "About Us"
    return dict(title=title)

def adminproduct(request):
    listing = dbsession.query(Listings).filter(Listings.pushfront==True).all()
    return render('kaniru:templates/derived/home/adminproducts.mako',
                  dict(listings=listing),request=request)

def featuredview(request):
    listing = Listings.listing_bunch(Listings.created.desc(),20)
    return render('kaniru:templates/derived/home/featured.mako',
                  dict(listings=listing),request=request)

@view_config(route_name="subscribemail")
def subscribmail(request):
    email = request.params.get("email")
    
    addr = EmailBank(
        address=email)
    dbsession.add(addr)
    dbsession.flush()
    request.session.flash("success; Thanks for subscribing for our newsletter")
    return HTTPFound(location = request.route_url('home'))
    
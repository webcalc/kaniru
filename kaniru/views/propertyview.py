from sqlalchemy import or_
from sqlalchemy.orm import with_polymorphic
from pyramid.response import Response
from pyramid.view import view_config, view_defaults
from pyramid_mailer import get_mailer
from pyramid_mailer.message import Message
from kaniru.models import DBSession
from kaniru.models import State,City,Area,Category, Listings, Adverts,Users,House,Apartments
from webhelpers.paginate import PageURL_WebOb, Page

from sqlalchemy.orm import joinedload
from pyramid.httpexceptions import HTTPFound, HTTPNotFound
from kaniru.utils.schemas import MailAgent
from pyramid_simpleform import Form
from pyramid_simpleform.renderers import FormRenderer
import transaction
from pyramid.renderers import render_to_response
from sqlalchemy import or_
from pyramid.security import (
    authenticated_userid,
    remember,
    forget,
    )

@view_config(route_name="prop_search")
def prop_searchin(request):
    title='Property search'
    type_name=request.params.get('type_id').strip()
    max_price = request.params.get('price').strip()
    beds = request.params.get('bed').strip()

    fulltext = request.params.get('fulltext')
    searchstring = u'%%%s%%' %fulltext
    
    
    if not beds:
        listings = DBSession.query(Listings).\
             join(Category).filter(
            or_(Listings.price<=max_price,
                Category.name==type_name.title())).all()
    if beds:
        listings = DBSession.query(Listings).with_polymorphic([House,Apartments]).\
             join(Category).filter(
            or_(Listings.price<=max_price,
                Category.name==type_name.title(),
                House.bedroom>=beds,
                Apartments.bedroom>=beds)).all()
    if fulltext !='':
        listings = DBSession.query(Listings).with_polymorphic([House,Apartments]).\
             join(Category).filter(
            or_(Listings.price<=max_price,
                Category.name==type_name.title(),
                House.bedroom>=beds,
                Apartments.bedroom>=beds,
                Listings.address.like(searchstring),
                Listings.title.like(searchstring),
                Listings.description.like(searchstring))).all()
   
    page_url = PageURL_WebOb(request)
    paginator = Page(listings, 
                     page=int(request.params.get("page", 1)), 
                     items_per_page=5, 
                     url=page_url)
    first = DBSession.query(Adverts).filter(Adverts.first==True).first()
    second = DBSession.query(Adverts).filter(Adverts.second==True).first()
    third = DBSession.query(Adverts).filter(Adverts.third==True).first()
    return render_to_response("kaniru:templates/derived/property/browse.mako",
                                     dict(title=title, paginator=paginator, 
                                          first=first, second=second, third=third),
                                      request=request)


@view_config(route_name="property_view", renderer="kaniru:templates/derived/property/view.mako")
def view_p(request):
    form = Form(request, schema=MailAgent, obj=request.user)
    prop_id = request.matchdict['listing_id']
    prop = Listings.get_by_id(prop_id)
    
    photo_ids = []
    for pix in prop.photos:
        id = pix.id
        photo_ids.append(id)
    state = DBSession.query(State).filter(State.id==prop.state_id).first()
    action_url=request.route_url('property_view',listing_id=prop_id,slug=prop.title)
    return dict(listing=prop,title=prop.title,ids = photo_ids,
                action_url=action_url, form=FormRenderer(form))



    
@view_config(route_name="browse_property", renderer="kaniru:templates/derived/property/browse.mako")
def list_all_p(request):
    title="Featured properties"
    page = int(request.params.get('page', 1))
    paginator = Listings.get_paginator(request,page)
    first = DBSession.query(Adverts).filter(Adverts.first==True).first()
    second = DBSession.query(Adverts).filter(Adverts.second==True).first()
    third = DBSession.query(Adverts).filter(Adverts.third==True).first()
    return dict(title=title, paginator=paginator, first=first, second=second, third=third)

@view_config(route_name="browse_location", renderer=
             "kaniru:templates/derived/property/location.mako")
def from_local(request):
    location_name = request.matchdict['location_name']
    listings=DBSession.query(Listings).join(State).filter(State.name==location_name).all()
    page_url = PageURL_WebOb(request)
    paginator = Page(listings, 
                     page=int(request.params.get("page", 1)), 
                     items_per_page=5, 
                     url=page_url)
    title = "Properties in %s"%(location_name)
    first = DBSession.query(Adverts).filter(Adverts.first==True).first()
    second = DBSession.query(Adverts).filter(Adverts.second==True).first()
    third = DBSession.query(Adverts).filter(Adverts.third==True).first()
    return dict(title=title, paginator=paginator,location_name=location_name, first=first, second=second, third=third)

@view_config(route_name="browse_city", renderer=
             "kaniru:templates/derived/property/city.mako")
def from_city(request):
    location_name = request.matchdict['location_name']
    listings=DBSession.query(Listings).filter(Listings.city==location_name).all()
    state=listings[0].state.name
    page_url = PageURL_WebOb(request)
    paginator = Page(listings, 
                     page=int(request.params.get("page", 1)), 
                     items_per_page=5, 
                     url=page_url)
    title = "Properties in %s"%(location_name)
    first = DBSession.query(Adverts).filter(Adverts.first==True).first()
    second = DBSession.query(Adverts).filter(Adverts.second==True).first()
    third = DBSession.query(Adverts).filter(Adverts.third==True).first()
    return dict(title=title,state=state, paginator=paginator,location_name=location_name, first=first, second=second, third=third)


@view_config(route_name="browse_area", renderer=
             "kaniru:templates/derived/property/area.mako")
def from_area(request):
    location_name = request.matchdict['location_name']
    listings=DBSession.query(Listings).filter(Listings.area==location_name).all()
    city = listings[0].city
    state = listings[0].state.name

    page_url = PageURL_WebOb(request)
    paginator = Page(listings, 
                     page=int(request.params.get("page", 1)), 
                     items_per_page=5, 
                     url=page_url)
    title = "Properties in %s"%(location_name)
    first = DBSession.query(Adverts).filter(Adverts.first==True).first()
    second = DBSession.query(Adverts).filter(Adverts.second==True).first()
    third = DBSession.query(Adverts).filter(Adverts.third==True).first()
    return dict(title=title,state=state, city=city,paginator=paginator,location_name=location_name, first=first, second=second, third=third)


@view_config(route_name="browse_category", renderer=
             "kaniru:templates/derived/property/category.mako")
def from_category(request):
    category_name = request.matchdict['category_name']
    listings=DBSession.query(Listings).join(Category).filter(Category.name==category_name).all()
    area = listings[0].area
    city = listings[0].city
    state = listings[0].state.name

    page_url = PageURL_WebOb(request)
    paginator = Page(listings, 
                     page=int(request.params.get("page", 1)), 
                     items_per_page=5, 
                     url=page_url)
    title = "Properties tagged %s"%(category_name)
    first = DBSession.query(Adverts).filter(Adverts.first==True).first()
    second = DBSession.query(Adverts).filter(Adverts.second==True).first()
    third = DBSession.query(Adverts).filter(Adverts.third==True).first()
    return dict(title=title,state=state,area=area, city=city,paginator=paginator,category_name=category_name, first=first, second=second, third=third)



@view_config(route_name="add_to_favourites")
def makefavourites(request):
    listing_id = request.matchdict['listing_id']
    listing = Listings.get_by_id(listing_id)
    user = request.user
    user.favourites.append(listing)
    DBSession.flush()
    return HTTPFound(location=request.route_url('browse_property'))

@view_config(route_name="for_sale",renderer="kaniru:templates/derived/property/browse.mako")
def forSale(request):
    listings = DBSession.query(Listings).filter(Listings.for_sale==True).all()
    page_url = PageURL_WebOb(request)
    paginator = Page(listings, 
                     page=int(request.params.get("page", 1)), 
                     items_per_page=5, 
                     url=page_url)
    title = "Properties for sale"
    first = DBSession.query(Adverts).filter(Adverts.first==True).first()
    second = DBSession.query(Adverts).filter(Adverts.second==True).first()
    third = DBSession.query(Adverts).filter(Adverts.third==True).first()
    return dict(title=title, paginator=paginator, first=first, second=second, third=third)

@view_config(route_name="for_rent",renderer="kaniru:templates/derived/property/browse.mako")
def forRent(request):
    listings = DBSession.query(Listings).filter(Listings.for_sale==False).all()
    page_url = PageURL_WebOb(request)
    paginator = Page(listings, 
                     page=int(request.params.get("page", 1)), 
                     items_per_page=5, 
                     url=page_url)
    title = "Properties for rent"
    first = DBSession.query(Adverts).filter(Adverts.first==True).first()
    second = DBSession.query(Adverts).filter(Adverts.second==True).first()
    third = DBSession.query(Adverts).filter(Adverts.third==True).first()
    return dict(title=title, paginator=paginator, first=first, second=second, third=third)

@view_config(route_name="dev",renderer="kaniru:templates/derived/property/browse.mako")
def devm(request):
    listings = DBSession.query(Listings).join(Category).filter(Category.name==u'Development').all()
    page_url = PageURL_WebOb(request)
    paginator = Page(listings, 
                     page=int(request.params.get("page", 1)), 
                     items_per_page=5, 
                     url=page_url)
    title = "Properties for rent"
    first = DBSession.query(Adverts).filter(Adverts.first==True).first()
    second = DBSession.query(Adverts).filter(Adverts.second==True).first()
    third = DBSession.query(Adverts).filter(Adverts.third==True).first()
    return dict(title=title, paginator=paginator, first=first, second=second, third=third)

@view_config(route_name="emailagent")
def emailage(request):
    camefrom = request.params.get('camefrom')
    listing = Listings.get_by_id(camefrom)
    form=Form(request, schema=MailAgent)
    if 'submit' in request.POST and form.validate():
        sender = "info@kaniru.com.ng"
        receiver = [request.params.get('mail')]
        fullname=form.data['fullname']
        phone=form.data['mobile']
        email = form.data['email']
        body = form.data['body']
        footer=""
        if phone:
            footer = "Here is my phone number %s." %phone
        msg_body = body+"."+ "\r\n" +footer +"\r\n"+\
                 "Email: %s"%email+"\r\n"\
                 "Name: %s"%fullname
        
        mailer = get_mailer(request)
        subject = "Kaniru.com.ng - Hi, "+fullname+" wants to contact you for your listing at Kaniru.com.ng"
        message = Message(subject=subject,
                      sender=sender,
                      recipients=receiver,
                      body=msg_body)
        mailer.send(message)
        transaction.commit()
        request.session.flash("success; Email sent")
        return HTTPFound(location=request.route_url('property_view',listing_id=camefrom,
                                                    slug =listing.slug))
    
        
@view_config(route_name="view_agent",renderer="kaniru:templates/derived/property/viewagent.mako")
def dagent(request):
    
    id = request.matchdict['user_id']
    user = Users.get_by_id(id)
    if not user:
        request.session.flash('warning; unknown error')
        return HTTPFound(location=request.route_url('home'))
    title = user.agency_name
    return dict(title=title,user=user)
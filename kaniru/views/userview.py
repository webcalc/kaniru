import base64
import hmac
import time

from pyramid.response import Response
from pyramid.renderers import render
from pyramid.view import view_config, view_defaults, forbidden_view_config

from kaniru.models import DBSession
from kaniru.models import Users, State,EmailBank
from kaniru.utils.messages import kaniru_email_forgot,kaniru_email
from kaniru.utils.schemas import ( UserAccountForm,
                                   ChangePasswordForm,
                                   ResetPasswordForm,
                                   ForgotPasswordForm,
                                   LoginSchema,
                                   UserProfile,
                                   AgencyProfile,
                                   Upload
                                   )
from pyramid_storage.exceptions import FileNotAllowed
from sqlalchemy.orm import joinedload
from pyramid.httpexceptions import HTTPFound, HTTPNotFound, HTTPForbidden
from pyramid.security import (
    authenticated_userid,
    remember,
    forget,
    )
from pyramid_simpleform import Form
from pyramid_simpleform.renderers import FormRenderer
from webhelpers.paginate import PageURL_WebOb, Page

def get_states():
    query = DBSession.query(State.id, State.name).all()
    return query



@view_config(route_name="registration", renderer="kaniru:templates/derived/reg.mako")
def register(request):
    title = 'Register'
    states=get_states()
    form  = Form(request, schema = UserAccountForm)
    dbsession =DBSession()
    
    if "form_submitted" in request.POST and form.validate():
        email = form.data['email']
        if not request.params.get('agency_name'):
            userObj = form.bind(Users())
            dbsession.add(userObj)
        else:
            state_id = request.params.get('state_id')
            state = State.get_by_id(state_id)
            userObj = Users(
                first_name=form.data['first_name'],
                last_name = form.data['last_name'],
                password = form.data['password'],
                email = form.data['email'],
                agency_name=request.params.get('agency_name'),
                agency_address = request.params.get('agency_address'),
                agency_website = request.params.get('agency_website')
                )
            dbsession.add(userObj)
            userObj.state=state
        if request.params.get('newsletter'):
            ebank = EmailBank(address=email)
            dbsession.add(ebank)
        dbsession.flush()
        headers = remember(request, email)
        request.session.flash("success; success")
        return HTTPFound(location = request.route_url('profile'), headers=headers)
    
    action_url = request.route_url("registration")
    return {'form':FormRenderer(form),'title':title,'states':states,
            'action_url':action_url}

@view_config(route_name="login", renderer="kaniru:templates/login.mako")
@forbidden_view_config(renderer="kaniru:templates/login.mako")
def userlogin(request):
    
    session = request.session
    title = "Login"
    login_url = request.route_url('login')
    referrer = request.url
    if referrer == login_url:
        referrer = '/' # never use the login form itself as came_from
    came_from = request.params.get('came_from', referrer)
    message = ''
    error_cls=''
    
    
    form = Form(request, schema=LoginSchema)
    if 'form_submitted' in request.POST and form.validate():
        email = form.data['email']
        password = form.data['password']
        if Users.check_password(email, password):
            
            headers = remember(request, email)
            request.session.flash("success;Logged in successfully")
            return HTTPFound(location=came_from, headers=headers)
        
        message = 'Failed login, Please try again'
        error_cls = 'has-error'
    elif 'continue' in request.POST:
        email = request.params.get('email','')
        session['email']=email
        return HTTPFound(location=request.route_url('registration'))

    return dict(
        title = title,
        message = message,
        error_cls = error_cls,
        url = request.application_url + '/login',
        came_from = came_from,
        form=FormRenderer(form)
        )



@view_config(route_name='logout')
def logout_view(request):
    headers = forget(request)
    request.session.invalidate()
    request.session.flash("success;Logged out successfully")
    return HTTPFound(location=request.route_url('home'), headers=headers)


@view_config(route_name = "forgot_password")
def passforgot(request):
    
    form = Form(request, schema=ForgotPasswordForm)
    if 'form_submitted' in request.POST and form.validate():
        user = Users.get_by_email(form.data['email'])
        if user:
            timestamp = time.time()+3600
            hmac_key = hmac.new('%s:%s:%d' % (str(user.id), \
                                'r5$55g35%4#$:l3#24&', timestamp), \
                                user.email).hexdigest()[0:10]
            time_key = base64.urlsafe_b64encode('%d' % timestamp)
            email_hash = '%s%s' % (hmac_key, time_key)
            kaniru_email_forgot(request, user.id, user.email, email_hash)
            request.session.flash('success; Password reset email sent')
            return HTTPFound(location=request.route_url('login'))
        request.session.flash('danger; No such user')
        return HTTPFound(location=request.route_url('login'))
    request.session.flash('danger; No such user')
    return HTTPFound(location=request.route_url('login'))

@view_config(route_name="change_password", renderer="kaniru:templates/changepassword.mako")
def change_pass(request):
    title = "Change your password"
    
    changepass_url = request.route_url('change_password')
    referrer = request.url
    if referrer == changepass_url:
        referrer = '/' # never use the change_pass form itself as came_from
    came_from = request.params.get('came_from', referrer)
    form = Form(request, schema=ChangePasswordForm)
    
    if 'form_submitted' in request.POST and form.validate():
        user.password = form.data['password']
        DBSession.merge(user)
        DBSession.flush()
        return HTTPFound(location=came_from)
    action_url=request.route_url('change_password')
    return {'title':title, 'form':FormRenderer(form),'username':username,'user':user,
            'action_url':action_url}

@view_config(route_name="reset_password",renderer="kaniru:templates/resetpassword.mako")
def restpass(request):
    title="Reset password"
    submitted_hmac = request.matchdict.get('hmac')
    user_id = request.matchdict.get('user_id')
    form = Form(request, schema = ResetPasswordForm)
    if 'form_submitted' in request.POST and form.validate():
        user = Users.get_by_id(user_id)
        current_time = time.time()
        time_key = int(base64.b64decode(submitted_hmac[10:]))
        if current_time < time_key:
            hmac_key = hmac.new('%s:%s:%d' % (str(user.id), \
                                'r5$55g35%4#$:l3#24&', time_key), \
                                user.email).hexdigest()[0:10]
            if hmac_key == submitted_hmac[0:10]:
                #FIXME reset email, no such attribute email
                user.password = form.data['password']
                DBSession.merge(user)
                DBSession.flush()
                request.session.flash('success; Password Changed. Please log in')
                return HTTPFound(location=request.route_url('login'))
            else:
                request.session.flash('Invalid request, please try again')
                return HTTPFound(location=request.route_url('forgot_password'))
    action_url = request.route_url("reset_password",user_id=user_id,hmac=submitted_hmac)
    return {'title': title,
            'form': FormRenderer(form), 'action_url': action_url}
        


@view_config(route_name="profile", permission="post", renderer="kaniru:templates/derived/account/myaccount.mako")
def myaccount(request):
    form=Form(request)
    title = "My Account Overview"
    
    action_url = request.route_url("agupload")
    return dict(
        form=FormRenderer(form),
        title=title,
        action_url=action_url,
   )


@view_config(route_name="details", permission="post", renderer="kaniru:templates/derived/account/details.mako")
def details(request):
    
    form1 = Form(request, schema=UserProfile,obj=request.user)
    form2 = Form(request, schema=AgencyProfile, obj=request.user)
    form = Form(request, schema=Upload)
    title = "My Account Details"
    states = get_states()
    if 'userdetails' in request.POST and form1.validate():
        useremail = form1.data['email']
        user_ = Users.get_by_email(useremail)
        if user_ and user_==request.user:
            user = form1.bind(request.user)
            DBSession.add(user)
            DBSession.flush()
            headers = remember(request,user.email)
            request.session.flash('success; details updated')
            return HTTPFound(location=request.route_url('details'),headers=headers)
        elif user_ and user_ != request.user:
            request.session.flash('danger; Something went wrong with the email you entered, '
                                  'please enter another one')
            return HTTPFound(location=request.route_url('details'))
        else:
            user = form1.bind(request.user)
            DBSession.add(user)
            DBSession.flush()
            headers = remember(request,user.email)
            request.session.flash('success; details updated')
            return HTTPFound(location=request.route_url('details'),headers=headers)
    if 'prodetails' in request.POST and form2.validate():
        
        user = form2.bind(request.user)
        DBSession.add(user)
        user.is_agent=True
        DBSession.flush()
        request.session.flash('success; details updated')
        return HTTPFound(location=request.route_url('details'))
            
    action_url= request.route_url('details')
    return dict(
        form1=FormRenderer(form1),
        form2=FormRenderer(form2),
        form=FormRenderer(form),
        states=states,
        action_url=action_url,
        title=title,
   )



@view_config(route_name="agupload", request_method="POST")
def agupload(request):
    form = Form(request, schema=Upload)
    if 'submit' in request.POST and form.validate():
        if request.user.agency_logo:
            request.storage.delete(request.user.agency_logo)
        filename=request.storage.save(request.POST['pics'], randomize=True)
        request.user.agency_logo=filename
        DBSession.flush()
        return HTTPFound(location=request.route_url('profile'))
        


@view_config(route_name="messages", permission="post", renderer="kaniru:templates/derived/account/msg.mako")
def msg(request):
    
    title = "My message box"
    
    return dict(
        
    title=title,
    
   )


@view_config(route_name="favourites", permission="post", renderer="kaniru:templates/derived/account/favourites.mako")
def favourites(request):
    
    title = "Favourite properties"
    listings = request.user.favourites
    page_url = PageURL_WebOb(request)
    paginator = Page(listings, 
                     page=int(request.params.get("page", 1)), 
                     items_per_page=5, 
                     url=page_url)
    return dict(
        paginator=paginator,
        title=title,
   )


@view_config(route_name="user_listings", permission="post", renderer="kaniru:templates/derived/account/userlistings.mako")
def my_listings(request):
    title = "My Listings"
    listings = request.user.listings
    page_url = PageURL_WebOb(request)
    paginator = Page(listings, 
                     page=int(request.params.get("page", 1)), 
                     items_per_page=5, 
                     url=page_url)
    return dict(
        paginator=paginator,
        title=title,
   )
    








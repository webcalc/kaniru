from fnmatch import fnmatch
import random
import string


from webhelpers.paginate import PageURL_WebOb, Page #<-
from webhelpers.date import time_ago_in_words 
from webhelpers.text import urlify, truncate 
from kaniru.utils.url_normalizer import url_normalizer


from sqlalchemy.orm.collections import attribute_mapped_collection

from sqlalchemy import Table
from sqlalchemy import LargeBinary
from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy.dialects.mysql import BIGINT,LONGBLOB
from sqlalchemy.orm import backref
from sqlalchemy.orm import relationship
from sqlalchemy.orm import column_property
from sqlalchemy.orm import synonym
from sqlalchemy.types import Integer
from sqlalchemy.types import String
from sqlalchemy.types import Boolean
from sqlalchemy.types import Float
from sqlalchemy.types import Unicode
from sqlalchemy.types import UnicodeText
from sqlalchemy.types import DateTime
from sqlalchemy.types import Date
from sqlalchemy.sql import func
from sqlalchemy.ext.hybrid import hybrid_property
"""SQLAlchemy Metadata and DBSession object"""
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker,joinedload
from zope.sqlalchemy import ZopeTransactionExtension 
from pyramid.security import (
    Everyone,
    Authenticated,
    unauthenticated_userid,
    authenticated_userid,
    ALL_PERMISSIONS,
    Allow,
    )


import transaction
from sqlalchemy import String
import cryptacular.bcrypt
crypt = cryptacular.bcrypt.BCRYPTPasswordManager()


DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()

alphabet = u'K-NG-'
NUMBER = u'123456789'


def make_random_number(length):
    'Return a random number(number) of a given length'
    return ''.join(random.choice(NUMBER) for n in xrange(length))

def make_random_unique_number(length, is_unique):
    'Return a random number given a function that checks for uniqueness'
    # Initialize
    iterationCount = 0
    permutationCount = len(NUMBER) ** length
    while iterationCount < permutationCount:
        # Make randomID
        randomID = alphabet + make_random_number(length)
        iterationCount += 1
        # If our randomID is unique, return it
        if is_unique(randomID):
            return randomID
    # Raise exception if we have no more permutations left
    raise RuntimeError('Could not create a unique string')

        
def is_unique(serial):
    reg = DBSession.query(Listings).filter(Listings.serial==serial).first()
    if reg:
        return False
    return True

def hash_password(password):
    return unicode(crypt.encode(password))


        
class State(Base):
    """State Model """
    __tablename__="state"
    id = Column(Integer, primary_key=True)
    
    name = Column(Unicode(100))
    #city = relationship('City',backref='state', cascade="all,delete-orphan")
    
    def __init__(self, name):
        self.name = name
        
    @staticmethod
    def extract_states(s_string):
        
        states = [s.title() for s in s_string.split(',')]
        
        states = set(states)

        return states

    @classmethod
    def get_by_name(cls, s_name):
        s= DBSession.query(cls).filter(cls.name == s_name)
        return s.first()
    
    @classmethod
    def get_by_id(cls, s_id):
        s = DBSession.query(cls).filter(cls.id == s_id)
        return s.first()

    @classmethod
    def create_state(cls, s_string):
        state_list = cls.extract_states(s_string)
        states = []

        for s_name in state_list:
            state = cls.get_by_name(s_name)
            if not state:
                state = State(name=s_name)
                DBSession.add(state)
            states.append(state)

        return states
    
    @classmethod
    def all(cls):
        return DBSession.query(cls).\
    order_by(cls.name)
    
        
    @classmethod
    def get_paginator(cls, request,page=1):
        page_url = PageURL_WebOb(request)
        return Page(cls.all(), page, url=page_url,
                    items_per_page=10)

    
class City(Base):
    """City Model"""
    __tablename__="city"
    id = Column(Integer, primary_key=True)
    
    name = Column(Unicode(100))
    #state_id = Column(Integer, ForeignKey('state.id'))
    #area = relationship("Area", cascade="all,delete-orphan", backref='city')
    def __init__(self, name, state_id):
        self.name = name
        self.state_id=state_id
        
    @staticmethod
    def extract_cities(city_string):
        
        cities = [city.title() for city in city_string.split(',')]
        
        cities = set(cities)

        return cities
    @classmethod
    def get_by_name(cls, city_name):
        city = DBSession.query(cls).filter(cls.name == city_name)
        return city.first()
    
    @classmethod
    def get_by_id(cls, city_id):
        city = DBSession.query(cls).filter(cls.id == city_id)
        return city.first()

    @classmethod
    def create_city(cls, state_id,city_string):
        city_list = cls.extract_cities(city_string)
        cities = []

        for city_name in city_list:
            city = cls.get_by_name(city_name)
            if not city:
                city = City(name=city_name, state_id=state_id)
                DBSession.add(city)
                DBSession.flush()
                area = Area.get_by_name(city_name)
                if not area:
                    area = Area(name=city_name,city_id=city.id)
                    DBSession.add(area)
                    DBSession.flush()
            area = Area.get_by_name(city_name)
            if not area:
                area = Area(name=city_name,city_id=city.id)
                DBSession.add(area)
                DBSession.flush()
            cities.append(city)

        return cities
    
    @classmethod
    def all(cls, state_id):
        return DBSession.query(City).filter_by(state_id=state_id).\
    order_by(City.name)
    
        
    @classmethod
    def get_paginator(cls, request,state_id,page=1):
        page_url = PageURL_WebOb(request)
        return Page(City.all(state_id), page, url=page_url,
                    items_per_page=10)

class Area(Base):
    """Area Model"""
    __tablename__="area"
    id = Column(Integer, primary_key=True)
    
    name = Column(Unicode(100))
    #city_id = Column(Integer, ForeignKey('city.id'))
    
    def __init__(self, name, city_id):
        self.name = name
        self.city_id = city_id
        
    @staticmethod
    def extract_area(area_string):
        
        areas = [area.title() for area in area_string.split(',')]
        
        areas = set(areas)

        return areas
    
    @classmethod
    def get_by_name(cls, area_name):
        area = DBSession.query(cls).filter(cls.name == area_name)
        return area.first()
    
    @classmethod
    def get_by_id(cls, area_id):
        area = DBSession.query(cls).filter(cls.id == area_id)
        return area.first()

    @classmethod
    def create_area(cls, city_id,area_string):
        area_list = cls.extract_area(area_string)
        areas = []

        for area_name in area_list:
            area = cls.get_by_name(area_name)
            if not area:
                area = Area(name=area_name, city_id=city_id)
                DBSession.add(area)
            areas.append(area)

        return areas
    
    @classmethod
    def all(cls, city_id):
        return DBSession.query(Area).filter_by(city_id=city_id).\
    order_by(Area.name)
    
        
    @classmethod
    def get_paginator(cls, request,city_id,page=1):
        page_url = PageURL_WebOb(request)
        return Page(City.all(city_id), page, url=page_url,
                    items_per_page=10)

class Category(Base):
    """Listing Category Entity Model
    
    """
    __tablename__="category"
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(100))
    subcategory = relationship("Subcategory", cascade="all,delete-orphan",backref='category')
    
    
    def __init__(self, name):
        self.name = name
    
        
    @staticmethod
    def extract_category(c_string):
        
        categories = [c.title() for c in c_string.split(',')]
        
        categories = set(categories)

        return categories

    @classmethod
    def get_by_name(cls, c_name):
        category = DBSession.query(cls).filter(cls.name == c_name)
        return category.first()
    
    @classmethod
    def get_by_id(cls,id):
        category = DBSession.query(cls).filter(cls.id == id)
        return category.first()

    @classmethod
    def create_categories(cls, c_string):
        categories_list = cls.extract_category(c_string)
        categories = []
        for c_name in categories_list:
            category = cls.get_by_name(c_name)
            if not category:
                category = Category(name=c_name)
                DBSession.add(category)
            categories.append(category)

        return categories
    
    @classmethod
    def all(cls):
        return DBSession.query(Category).\
    order_by(Category.name)
    
        
    @classmethod
    def get_paginator(cls, request,page=1):
        page_url = PageURL_WebOb(request)
        return Page(Category.all(), page, url=page_url,
                    items_per_page=10)
    

class Subcategory(Base):
    __tablename__ = "subcategory"
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(80))
    category_id = Column(Integer, ForeignKey("category.id"))
    
    
    
    def __init__(self, name,category_id):
        self.name = name
        self.category_id = category_id
    
    @staticmethod
    def extract_subcategory(sub_string):
        
        subcategories = [sub.title() for sub in sub_string.split(',')]
        
        subcategories = set(subcategories)

        return subcategories
    
    @classmethod
    def get_by_name(cls, sub_name):
        sub= DBSession.query(cls).filter(cls.name == sub_name)
        return sub.first()
    
    @classmethod
    def get_by_id(cls, sub_id):
        sub = DBSession.query(cls).filter(cls.id == sub_id)
        return sub.first()

    @classmethod
    def create_subcategories(cls, category_id,sub_string):
        sub_list = cls.extract_subcategory(sub_string)
        subs = []

        for sub_name in sub_list:
            sub = cls.get_by_name(sub_name)
            if not sub:
                sub= Subcategory(name=sub_name, category_id=category_id)
                DBSession.add(sub)
            subs.append(sub)

        return subs
    
    @classmethod
    def all(cls, category_id):
        return DBSession.query(Subcategory).filter_by(category_id=category_id).\
    order_by(Subcategory.name)
    
        
    @classmethod
    def get_paginator(cls, request,category_id,page=1):
        page_url = PageURL_WebOb(request)
        return Page(Subcategory.all(category_id), page, url=page_url,
                    items_per_page=10)

    
    
class Listings(Base):
    """ Listing Entity Table """
    __tablename__= "listings"
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'),index=True)
    serial = Column(Unicode(80), default=lambda:make_random_unique_number(9,is_unique))
    for_sale = Column(Boolean, default=True)
    category_id = Column(Integer, 
                         ForeignKey("category.id"),
                         index=True)
    category = relationship('Category',backref=backref('listings',cascade="all,delete-orphan"))
    
    subcategory_id = Column(Integer,ForeignKey("subcategory.id"),
                         index=True)
    subcategory = relationship('Subcategory',backref=backref('listings',cascade="all,delete-orphan"))
    
    state_id = Column(Integer, ForeignKey("state.id"), index= True)
    state = relationship('State',backref=backref('listings',cascade="all,delete-orphan"))
    
    #city_id = Column(Integer, ForeignKey("city.id"),index=True)
    #city = relationship('City',backref=backref('listings',cascade="all,delete-orphan"))
    city = Column(Unicode(80))
    #area_id = Column(Integer, ForeignKey("area.id"),index=True)
    #area = relationship('Area',backref=backref('listings',cascade="all,delete-orphan"))
    area = Column(Unicode(80))
    pushfront = Column(Boolean,default=False)
    address = Column(Unicode(80))
    show_address = Column(Boolean,default=True)
    price = Column(Integer)
    currency = Column(Unicode(80))
    price_available = Column(Boolean, default=True)
    price_condition = Column(Unicode(80))
    deposit = Column(Integer)
    agent_commission = Column(Unicode(80))
    date_available = Column(Unicode(80))
    title = Column(Unicode(255))
    description = Column(UnicodeText)
    car_spaces = Column(Integer)
    year_built = Column(Integer)
    active = Column(Boolean, default=True)
    display_fullname = Column(Unicode(80))
    display_email = Column(Unicode(80))
    home_phone= Column(Unicode(80))
    mobile = Column(Unicode(80))
    office_phone = Column(Unicode(80))
    created = Column(DateTime,default=func.now())
    modified = Column(DateTime)
    type = Column(String(50))

    __mapper_args__ = {
        'polymorphic_identity':'listings',
        'polymorphic_on':type,
        'with_polymorphic':'*'
    }
    
    
    @classmethod
    def get_query(cls,with_joinedload=True):
        query = DBSession.query(cls)
        if with_joinedload:
            query = query.join(Users)
        return query
    
    @classmethod
    def get_by_id(cls, listing_id, with_joinedload=True):
        query = cls.get_query(with_joinedload)
        return query.filter(cls.id == listing_id).first()
    
    @classmethod
    def listing_bunch(cls, order_by, how_many=50, with_joinedload=True):
        query = cls.get_query(with_joinedload)
        query = query.order_by(order_by)
        return query.limit(how_many).all()
    
    @property
    def slug(self):
        return url_normalizer(self.title)
    @property
    def featured_title(self):
        result= truncate(self.title,length=25, whole_word=True)
        if len(self.title)<25:
            return self.title
        return "%s" %(result)
    @property
    def naira_price(self):
        stringNum = str(self.price)
        length = len(stringNum)
        price=stringNum
        if length==4:
            price = stringNum[0]+','+stringNum[1:]
        if length==5:
            price = stringNum[0:2]+','+stringNum[2:]
        if length==6:
            price = stringNum[0:3]+','+stringNum[3:]
        if length==7:
            price = stringNum[0]+','+stringNum[1:4]+','+stringNum[4:]
        if length==8:
            price = stringNum[0:2]+','+stringNum[2:5]+','+stringNum[5:]
        if length==9:
            price = stringNum[0:3]+','+stringNum[3:6]+','+stringNum[6:]
        if length==10:
            price = stringNum[0]+','+stringNum[1:4]+','+stringNum[4:7]+','\
                +stringNum[7:]
        return u'\u20a6 ' +price
    
    @property
    def naira_deposit(self):
        stringNum = str(self.deposit)
        length = len(stringNum)
        price=stringNum
        if length==4:
            price = stringNum[0]+','+stringNum[1:]
        if length==5:
            price = stringNum[0:2]+','+stringNum[2:]
        if length==6:
            price = stringNum[0:3]+','+stringNum[3:]
        if length==7:
            price = stringNum[0]+','+stringNum[1:4]+','+stringNum[4:]
        if length==8:
            price = stringNum[0:2]+','+stringNum[2:5]+','+stringNum[5:]
        if length==9:
            price = stringNum[0:3]+','+stringNum[3:6]+','+stringNum[6:]
        if length==10:
            price = stringNum[0]+','+stringNum[1:4]+','+stringNum[4:7]+','\
                +stringNum[7:]
        return u'\u20a6 ' +price
    @property
    def browse_title(self):
        result= truncate(self.title,length=50, whole_word=True)
        if len(self.title)<50:
            return self.title
        return "%s" %(result)
    @classmethod
    def all(request):
        return DBSession.query(Listings).order_by(Listings.created.desc()).all()
    
    @classmethod
    def get_paginator(cls, request,page=1):
        page_url = PageURL_WebOb(request)
        return Page(cls.all(), page, url=page_url,
                    items_per_page=5)
    
uploadable_mimetypes = ['image/*', ]
def is_uploadable_mimetypes(mimetype):
    match_score = 0

    for mt in uploadable_mimetypes:
        if fnmatch(mimetype, mt):
            if len(mt) > match_score:
                match_score = len(mt)

    return match_score



class Photos(Base):
    """Photo Entity Model"""
    __tablename__="photos"
    id = Column(Integer, primary_key=True)
    filename = Column(Unicode(80))
    listing_id = Column(Integer, ForeignKey("listings.id"))
    listings = relationship("Listings", backref=backref("photos",cascade="all, delete-orphan"))
    
        
    
class House(Listings):
    """ Residential homes Entity Model """
    __tablename__="house"
    id = Column(Integer, ForeignKey("listings.id"),
                        primary_key=True)
    bedroom = Column(Integer)
    bathroom = Column(Integer)
    total_room = Column(Integer)
    living_area = Column(Unicode(24))
    land_size = Column(Unicode(24))
    features = relationship("Features",secondary='house_features',
                            backref="house")
    
    
    furnished = Column(Unicode(80))
    
    __mapper_args__ = {
        'polymorphic_identity':'house'
        }
    
    
class Apartments(Listings):
    """ Agricultural homes Entity Models"""
    __tablename__="apartments"
    id = Column(Integer, ForeignKey("listings.id"),
                        primary_key=True)
    bedroom = Column(Integer)
    bathroom = Column(Integer)
    total_room = Column(Integer)
    living_area = Column(Unicode(24))
    floor = Column(Integer)
    total_floor = Column(Integer)
    features = relationship("Features",secondary='apartment_features',
                            backref="apartments")
   
    furnished = Column(Unicode(24))
    
    
    __mapper_args__ = {
        'polymorphic_identity':'agricultural'
        }
    
    
    
class Commercials(Listings):
    """ Commercial Building Entity Model"""
    __tablename__="commercials"
    id = Column(Integer, ForeignKey("listings.id"),
                        primary_key=True)
    total_room = Column(Integer)
    
    building_size = Column(Unicode(24))
    features = relationship("Features",secondary='commercial_features',
                            backref="commercials")
   
    furnished = Column(Unicode(80))
    
    __mapper_args__ = {
        'polymorphic_identity':'commercials'
        }
    
    
class Lands(Listings):
    """ Land Entity Model """
    __tablename__="lands"
    id = Column(Integer, ForeignKey("listings.id"),
                        primary_key=True)
    plot_size = Column(Unicode(24))
    
    __mapper_args__ = {
        'polymorphic_identity':'lands'
        }
    
    
house_feature = Table('house_features', Base.metadata,
    Column('house_id', Integer, ForeignKey('house.id')),
    Column('feature_id', Integer, ForeignKey('features.id'))
)

apartment_feature = Table('apartment_features', Base.metadata,
    Column('apartments_id', Integer, ForeignKey('apartments.id')),
    Column('feature_id', Integer, ForeignKey('features.id'))
)

commercial_feature = Table('commercial_features', Base.metadata,
    Column('commercial_id', Integer, ForeignKey('commercials.id')),
    Column('feature_id', Integer, ForeignKey('features.id'))
)



class Feature_types(Base):
    """Property Feature Types"""
    __tablename__="feature_types"
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(80))
    features = relationship("Features",cascade="all, delete-orphan", backref="feature_types")
    
    def __init__(self, name):
        self.name=name
    
    @staticmethod
    def extract_ftype(f_string):
        
        types = [t.title() for t in f_string.split(',')]
        
        features = set(types)

        return features
    
    @classmethod
    def get_by_name(cls, f_name):
        feature = DBSession.query(cls).filter(cls.name == f_name)
        return feature.first()
    
    @classmethod
    def get_by_id(cls, feature_id):
        feature = DBSession.query(cls).filter(cls.id == feature_id)
        return feature.first()

    @classmethod
    def create_ftypes(cls, f_string):
        f_list = cls.extract_ftype(f_string)
        types = []

        for f_name in f_list:
            typeq = cls.get_by_name(f_name)
            if not typeq:
                typeq = Feature_types(name=f_name)
                DBSession.add(typeq)
            types.append(typeq)

        return types
    
    @classmethod
    def all(cls):
        return DBSession.query(Feature_types).\
    order_by(Feature_types.name)
    
    
class Features(Base):
    """ Property Features Models """
    __tablename__='features'
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(80))
    
    type_id = Column(Integer, ForeignKey("feature_types.id"))
    
    
    
    def __init__(self, name, type_id):
        self.name=name
        self.type_id = type_id
    
    
    @staticmethod
    def extract_features(f_string):
        
        features = [feature.title() for feature in f_string.split(',')]
        
        features = set(features)

        return features
    
    @classmethod
    def get_by_name(cls, f_name):
        feature = DBSession.query(cls).filter(cls.name == f_name)
        return feature.first()
    
    @classmethod
    def get_by_id(cls, feature_id):
        feature = DBSession.query(cls).filter(cls.id == feature_id)
        return feature.first()

    @classmethod
    def create_features(cls, type_id,f_string):
        f_list = cls.extract_features(f_string)
        features = []

        for f_name in f_list:
            feature = cls.get_by_name(f_name)
            if not feature:
                feature = Features(name=f_name, type_id=type_id)
                DBSession.add(feature)
            features.append(feature)

        return features
    
    @classmethod
    def all(cls, type_id):
        return DBSession.query(Features).filter_by(type_id=type_id).\
    order_by(Features.name)
    
        
    @classmethod
    def get_paginator(cls, request,type_id,page=1):
        page_url = PageURL_WebOb(request)
        return Page(Features.all(type_id), page, url=page_url,
                    items_per_page=10)

user_favourites = Table('user_favourites', Base.metadata,
    Column('user_id', Integer, ForeignKey("users.id")),
    Column('listing_id',Integer, ForeignKey("listings.id"))
    )

    
    
    

class Users(Base):
    """ We're extending AuthUser by adding a Foreign Key to Profile. 
    Make sure you set index=True on user_id.
    """
    __tablename__ = 'users'
    
    id = Column(Integer, primary_key=True)
    
    first_name = Column(Unicode(80))
    last_name = Column(Unicode(80))
    email = Column(Unicode(80))
    _password = Column(Unicode(80))
    mygroups = relationship("Groups", secondary='user_group')
    state_id = Column(Integer,ForeignKey("state.id",ondelete="cascade", onupdate="cascade"))
    state = relationship('State',backref=backref('users',
                                                        cascade="all, delete-orphan",
                                                        passive_deletes=True))
    listings = relationship("Listings", backref="users")
    favourites = relationship("Listings",secondary='user_favourites',
                              backref='my_favourites')
    
    newsletter = Column(Boolean, default = 0)
    
    agency_note=Column(Unicode(255))
    agency_name = Column(Unicode(80))
    agency_logo = Column(Unicode(80))
    agency_website = Column(Unicode(80))
    agency_address = Column(Unicode(80))
    mobile = Column(Unicode(80))
    home_phone = Column(Unicode(80))
    office_phone = Column(Unicode(80))
    
    is_agent = Column(Boolean, default=0)
    created = Column(DateTime, default=func.now())
    
    @hybrid_property
    def fullname(self):
        return self.last_name.title() +" "+ self.first_name.title()
    def _get_password(self):
        return self._password

    def _set_password(self, password):
        self._password = hash_password(password)

    password = property(_get_password, _set_password)
    password = synonym('_password', descriptor=password)
    
    @property
    def slug(self):
        return url_normalizer(self.agency_name)
    
    @classmethod
    def get_by_id(cls, user_id):
        return DBSession.query(Users).filter(Users.id==user_id).first()
    
    @classmethod
    def get_by_email(cls, email):
        return DBSession.query(cls).filter(cls.email==email).first()

    @classmethod
    def check_password(cls, email, password):
        user= cls.get_by_email(email)
        if not user:
            return False
        return crypt.check(user.password, password)
    
    @classmethod
    def all(cls):
        return DBSession.query(cls).\
    order_by(cls.first_name)
    
        
    @classmethod
    def get_paginator(cls, request,page=1):
        page_url = PageURL_WebOb(request)
        return Page(cls.all(), page, url=page_url,
                    items_per_page=10)
class Groups(Base):
    __tablename__='groups'
    id = Column(Integer, primary_key=True)
    groupname = Column(Unicode(255), unique=True)

    def __init__(self, groupname):
        self.groupname = groupname
    
    @classmethod
    def get_by_name(cls, groupname):
        return DBSession.query(Groups).\
               filter(Groups.groupname==groupname).first()

user_group = Table('user_group', Base.metadata,
    Column('user_id', Integer, ForeignKey("users.id")),
    Column('groups_id',Integer, ForeignKey("groups.id")),
                         )


def groupfinder(userid,request):
    user = request.user
    if user:
        return [g.groupname for g in request.user.mygroups]
    return None



def get_user(request):
    userid = unauthenticated_userid(request)
    if userid is not None:
        return Users.get_by_email(userid)
    


class EmailBank(Base):
    __tablename__ = 'emailbank'
    id = Column(Integer, primary_key=True)
    address = Column(Unicode(80))
    


    
class Adverts(Base):
    __tablename__='ads'
    id = Column(Integer, primary_key=True)
    url = Column(UnicodeText)
    filename = Column(Unicode(80))
    link=Column(Unicode(80))
    frontpage = Column(Boolean, default=False)
    first = Column(Boolean,default=False)
    second = Column(Boolean, default=False)
    third = Column(Boolean, default=False)
    viewpage = Column(Boolean, default=False)
    created = Column(DateTime, default=func.now())

    

class RootFactory(Base):
    """ Defines the default ACLs, groups populated from SQLAlchemy.
    """
    __tablename__='root'
    id = Column(Integer, primary_key=True)
    def __init__(self, request):
        if request.matchdict:
            self.__dict__.update(request.matchdict)

    @property
    def __acl__(self):
        defaultlist = [ (Allow, Everyone, 'view'),
                (Allow, Authenticated, 'post'),
                (Allow, 'editor',('view','post','edit')),
                (Allow,'admin',ALL_PERMISSIONS)]
        
        return defaultlist


def populate_groups():
    g1 = Groups(u'admin')
    g2 = Groups(u'editor')
    g3 = Groups(u'viewer')
    adminuser = Users(
        first_name=u"admin",
        last_name = u"admin",
        email = u"info@kaniru.com.ng",
        password = u"admin321"
        )
    adminuser2 = Users(
        first_name=u"Ephraim",
        last_name = u"Anierobi",
        email = u"splendidzigy24@gmail.com",
        password = u"unitedkins"
        )
    with transaction.manager:
        DBSession.add_all([g1,g2,g3])
        adminuser.mygroups.append(g1)
        adminuser2.mygroups.append(g1)
        DBSession.add(adminuser)
        DBSession.add(adminuser2)
        transaction.commit()
    

def populate_category():
        root_category_1 = Category(name=u'House')
        
        
         
        root_category_2 = Category(name=u"Apartments")
        
        
        
        root_category_3 = Category(name=u"Land")
        
        root_category_4 =Category(name=u"Commercial")
        
        
        root_category_5 = Category(name=u"Development")
        
        with transaction.manager:
                DBSession.add_all([root_category_1, root_category_2, root_category_3,root_category_4, root_category_5])
        

def populate_location():
        
        abuja=State(name=u"Abuja")
        abia=State(name=u"Abia")
        anambra= State(name=u"Anambra")
        adamawa=State(name=u"Adamawa")
        akwaibom=State(name=u"Akwa Ibom")
        
        bauchi=State(name=u"Bauchi")
        benue=State(name=u"Benue")
        bayelsa=State(name=u"Bayelsa")
        borno=State(name=u"Borno")
        
        crossriver=State(name=u"Cross River")
        
        enugu=State(name=u"Enugu")
        ebonyi=State(name=u"Ebonyi")
        edo=State(name=u"Edo")
        ekiti=State(name=u"Ekiti")
        
        
        delta=State(name=u"Delta")
        
        gombe=State(name=u"Gombe")
        imo=State(name=u"Imo")
        jigawa=State(name=u"Jigawa")
        
        kebbi=State(name=u"Kebbi")
        kogi=State(name=u"Kogi")
        kwara=State(name=u"Kwara")
        kano=State(name=u"Kano")
        kaduna=State(name=u"Kaduna")
        katsina=State(name=u"Katsina")
        
        lagos=State(name=u"Lagos")
        nasarawa=State(name=u"Nasarawa")
        niger=State(name=u"Niger")
        
        osun=State(name=u"Osun")
        ogun=State(name=u"Ogun")
        oyo=State(name=u"Oyo")
        ondo=State(name=u"Ondo")
        
        rivers=State(name=u"Rivers")
        plateau=State(name=u"Plateau")
        taraba=State(name=u"Taraba")
        sokoto=State(name=u"Sokoto")
        yobe=State(name=u"Yobe")
        zamfara=State(name=u"Zamfara")
        with transaction.manager:
                DBSession.add_all([abuja,abia,anambra, adamawa,akwaibom,bauchi,benue,bayelsa,borno, crossriver,ebonyi,enugu,
                                   edo,ekiti,delta,gombe,imo,jigawa,kebbi,kogi,kwara,kano,kaduna,katsina,lagos,nasarawa,
                                   niger,osun,ogun,oyo,ondo,rivers,plateau,taraba,sokoto,yobe,zamfara])








def init_model(engine):
        """Call me before using any of the tables or classes in the model"""
        DBSession.configure(bind=engine)
        Base.metadata.bind = engine
        Base.metadata.drop_all(engine)
        Base.metadata.create_all(engine)
        if DBSession.query(State).count()==0:
            populate_location()
            populate_groups()
            populate_category()
            
            
                
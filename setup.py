import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.txt')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()

requires = [
    'pyramid==1.4',
    'pyramid_mailer==0.13',
    'SQLAlchemy==0.8.2',
    'transaction==1.4.3',
    'pyramid_storage==0.0.6',
    'pyramid_tm==0.7',
    'pyramid_debugtoolbar==2.0.2',
    'zope.sqlalchemy==0.7.2',
    'waitress==0.8.8',
    'pyramid_beaker==0.8',
    'WebHelpers==1.3',
    'pyramid_simpleform==0.6.1',
    'mysql-python',
    'cryptacular==1.4.1',
    'mako==0.9.1',
    'alembic==0.5.0',
    'unidecode==0.04.14'
    ]

setup(name='kaniru',
      version='0.0',
      description='kaniru',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='',
      author_email='',
      url='',
      keywords='web wsgi bfg pylons pyramid',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='kaniru',
      install_requires=requires,
      entry_points="""\
      [paste.app_factory]
      main = kaniru:main
      [console_scripts]
      initialize_kaniru_db = kaniru.scripts.initializedb:main
      """,
      )
